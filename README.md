## Installation

- git reset --hard
- git pull
- php artisan optimize:clear
- php artisan migrate:fresh --database=mysql --seed
- php artisan import:party --force
- php artisan import:stock --force
- php artisan import:stock-upload
- php artisan storage:link

## ENV Changes

- APP_ENV = production
