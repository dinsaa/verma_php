@extends('layouts.app')

@push('title', 'Dashboard - Bajaj Portal')

@section('content')
    <main class="container">
        <section class="row pt-5">
            <div class="col text-end">
                <form action="{{ route('auth.logout-post') }}" method="post">
                    @csrf
                    <a href="javascript:;" onclick="parentNode.submit();">Logout</a>
                </form>
            </div>
        </section>

        <section class="row">
            <div class="col-md-3">
                <div class="card border-primary text-center">
                    <div class="card-header bg-gradient text-bg-primary">
                        <h5>Invoice</h5>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('invoice.index') }}" class="btn btn-primary">View</a>
                    </div>
                </div>
            </div>
            @if(auth()->user()->role_id == 1)
                <div class="col-md-3">
                    <div class="card border-info text-center">
                        <div class="card-header bg-gradient text-bg-info">
                            <h5>User Manage</h5>
                        </div>
                        <div class="card-body">
                            <a href="{{ route('user.index') }}" class="btn btn-info">View</a>
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </main>
@endsection
