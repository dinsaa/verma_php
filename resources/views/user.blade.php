@extends('layouts.app')

@push('title', 'User Manage - Bajaj Portal')

@section('content')
    <main class="container">
        <section class="row pt-5">
            <div class="col d-flex">
                <h5>USER MANAGE</h5>
            </div>
            <div class="col text-end">
                <a href="{{ route('dashboard.index') }}">Back to Dashboard</a>
            </div>
        </section>

        <section class="row pt-4">
            <div class="col">
                <form action="{{ route('user.index') }}" method="get" autocomplete="off">
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <td><label for="username">Name / Designation</label></td>
                            <td><label for="status">Status</label></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" name="username" id="username" class="form-control"
                                       value="{{ request()->input('username') }}">
                            </td>
                            <td>
                                <select name="status" id="status" class="form-select">
                                    <option value="">All</option>
                                    <option value="active" @if(request()->input('status') == 'active')
                                        {{ 'selected' }}
                                        @endif>Active
                                    </option>
                                    <option value="inactive" @if(request()->input('status') == 'inactive')
                                        {{ 'selected' }}
                                        @endif>In Active
                                    </option>
                                </select>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-success">Search</button>
                                <button type="button" class="btn btn-danger"
                                        onclick="window.location = window.location.pathname;">Clear
                                </button>
                            </td>
                            <td class="text-end">
                                <button type="button" class="btn btn-secondary" id="addUser">Add User</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </section>

        <section class="row">
            <div class="col">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Sr No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Designation</th>
                        <th scope="col">Voucher Series</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td scope="row">{{ $row->name ?? "" }}</td>
                            <td>{{ $row->designation ?? "-" }}</td>
                            <td>{{ $row->voucher_series ? implode(", ", $row->voucher_series) : "-" }}</td>
                            <td>{{ $row->status == 1 ? "Active" : "In Active" }}</td>
                            <td>
                                <a href='javascript:void(0)'
                                   class="text-decoration-none link-info editUser @if($row->status == 0) d-none @endif"
                                   data-id="{{ $row->id }}">Edit</a>
                                <a href='javascript:void(0)'
                                   class="text-decoration-none link-danger deleteUser @if($row->role_id == 1) d-none @endif"
                                   data-id="{{ $row->id }}">{{ $row->status == 1 ? "Delete" : "Restore" }}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $data->withQueryString()->links() }}
            </div>
        </section>

        <section id="modal-section"></section>
    </main>
@endsection

@push('js')
    <script>
        const addUserBtn = document.querySelector('#addUser')
        const editUserBtn = document.querySelectorAll('.editUser')
        const deleteUserBtn = document.querySelectorAll('.deleteUser')
        let modalForm = null;

        const getModalAPI = (url) => {
            fetch(url)
                .then(res => res.text())
                .then(html => document.querySelector('#modal-section').innerHTML = html)
                .then(() => (new bootstrap.Modal('#modal')).show())
                .then(() => {
                    modalForm = document.querySelector('#modalForm')
                    document.querySelector('#modal').addEventListener('hidden.bs.modal', () => modalForm = null)
                })
                .then(() => {
                    modalForm.addEventListener('submit', event => {
                        event.preventDefault()

                        fetch(modalForm.action, {method: modalForm.method, body: new FormData(modalForm)})
                            .then(res => res.json())
                            .then(json => {
                                alert(json.message)
                                if (json.status === true) {
                                    window.location.reload()
                                }
                            })
                    })
                })
        }

        addUserBtn.addEventListener('click', () => {
            getModalAPI('{{ route('user.create') }}')
        })

        for (let i = 0; i < editUserBtn.length; i++) {
            editUserBtn[i].addEventListener('click', e => {
                getModalAPI('{{ route('user.edit') }}?id=' + e.target.getAttribute('data-id'))
            })
        }

        for (let i = 0; i < deleteUserBtn.length; i++) {
            deleteUserBtn[i].addEventListener('click', e => {
                if (confirm('Are you sure?')) {
                    fetch('{{ route('user.destroy') }}?id=' + e.target.getAttribute('data-id'))
                        .then(res => res.json())
                        .then(json => {
                            alert(json.message)
                            if (json.status === true) {
                                window.location.reload()
                            }
                        })
                }
            })
        }
    </script>
@endpush
