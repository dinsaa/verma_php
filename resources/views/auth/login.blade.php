@extends('layouts.app')

@push('title', 'Login - Bajaj Portal')

@section('content')
    <main class="container">
        <section class="row pt-5">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        <h5>Login Page</h5>
                        <hr>
                        <form autocomplete="off" method="post" action="{{ route('auth.login-post') }}">
                            @csrf
                            <div class="mb-3">
                                <label for="username" class="form-label">Username</label>
                                <input type="text" name="email" class="form-control" id="username">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" id="password">
                            </div>
                            @if($errors->any())
                                <div class="mb-3 text-danger">
                                    {{ $errors->first() }}
                                </div>
                            @endif
                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
