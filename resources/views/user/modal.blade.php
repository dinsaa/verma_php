@php
    $modalType = $type ?? "add";
    $modalTitle = $title ?? "Title";
    $data = $data ?? null;
    $branchList = $branch ?? [];
@endphp

<div class="modal" id="modal" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $modalTitle }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="post" action="{{ $modalType == 'add' ? route('user.store') : route('user.update') }}"
                  id="modalForm">
                @csrf
                @if($modalType != 'add')
                    <input type="hidden" name="id" value="{{ isset($data['id']) ? $data['id'] : '' }}">
                @endif
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       value="{{ isset($data['name']) ? $data['name'] : '' }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="name" class="form-label">Username</label>
                                <input type="text" class="form-control" id="email" name="email"
                                       value="{{ isset($data['email']) ? $data['email'] : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       value="{{ isset($data['password']) ? $data['password'] : '' }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="password_confirmation" class="form-label">Confirm Password</label>
                                <input type="password" class="form-control" id="password_confirmation"
                                       name="password_confirmation"
                                       value="{{ isset($data['password']) ? $data['password'] : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label for="designation" class="form-label">Designation</label>
                                <input type="text" class="form-control" id="designation" name="designation"
                                       value="{{ isset($data['designation']) ? $data['designation'] : '' }}">
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="voucher_series" class="form-label">Voucher Series</label>
                                <select class="form-select" id="voucher_series" name="voucher_series[]" multiple>
                                    @foreach($branchList as $branch)
                                        <option value="{{ $branch['inv_prefix'] }}"
                                                @if(isset($data['voucher_series']) && in_array($branch['inv_prefix'], $data['voucher_series'])) selected @endif>{{ $branch['inv_prefix'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit"
                            class="btn btn-primary">{{ $modalType == 'add' ? 'Save' : 'Save Changes' }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
