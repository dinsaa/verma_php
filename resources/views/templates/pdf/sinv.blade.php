@php
    $buyerData = null;
    if ($data['buyerDetails']['PARTYNAME']) {
        $buyerData = \App\Models\MasterParty::where('ID', $data['buyerDetails']['PARTYNAME'])->first();
    }

    $adoc = explode("/", $data['sellerDetails']['referenceNo']);
    $docty = $adoc[0];
    $doclc = substr($adoc[1], 0, 3);
    if ($data['extra']['iVoucherType'] == "Credit Note") {
        $adoc = "LUG";
    }
    $location = "";
    $warehouse = "";
    $hAddress = "";
    $hContact = "";
    if ($doclc == "NDW") {
        $location = "Ndeeba WH";
        $warehouse = "Ndeeba WH";
        $hAddress = "Plot No. 889-890, Masaka Road,Ndeeba";
        $hContact = "Tel: +256 706 031 012 Email: info@bajajverma.com";
    } else if ($doclc == "NDB") {
        $location = "Ndeeba Branch";
        $warehouse = "Ndeeba Branch";
        $hAddress = "Plot No. 889-890, Masaka Road,Ndeeba";
        $hContact = "Tel: +256 706 031 008 Email: info@bajajverma.com";
    } else if ($doclc == "BMB") {
        $location = "Bombo Road";
        $warehouse = "Bombo Road";
        $hAddress = "Bombo Rd, Kampala";
        $hContact = "Tel: +256 706 031 064 Email: info@bajajverma.com;";
    } else if ($doclc == "JNJ") {
        $location = "Jinja";
        $warehouse = "Jinja";
        $hAddress = "Plot 42, Iganga Road, Jinja";
        $hContact = "Tel: +256 706 031 009 Email: info@bajajverma.com";
    } else if ($doclc == "RTS") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "RSP") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "RSS") {
        $location = "Sowedi Mudehere";
        $warehouse = "Sowedi Mudehere";
        $hAddress = "Sowedi Mudehere, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "LUG") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "MSK") {
        $location = "Masaka";
        $warehouse = "Masaka";
        $hAddress = "Plot 824, Kampala Road, Nyendo, Masaka";
        $hContact = "Tel: +256 706 031 017 Email: info@bajajverma.com";
    } else if ($doclc == "MBL") {
        $location = "Mbale";
        $warehouse = "Mbale";
        $hAddress = "Plot 16/16A, Bunyoli Road, Mbale";
        $hContact = "Tel: +256 706 031 027 Email: info@bajajverma.com";
    }
@endphp

    <!DOCTYPE HTML>
<html>

<head>
    <style>
        body {
            width: 100%;
            margin: 0;
            padding: 0;
            font-size: 0.9rem;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        td {
            padding: 3px 2px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }
    </style>
</head>
<body>
<div>
    <table style="border: none !important;">
        <tr>
            <td style="border: none !important;" width="300px">
                <img src="{{ public_path('img/logo.jpg') }}" alt="" width="230px">
            </td>
            <td style="border: none !important;">
                <img src="{{ $qrcode }}" alt="" height="90px">
            </td>
            <td align="center" width="150px" style="font-size: 1.1rem">
                <b>CASH SALES <br> TAX INVOICE- <br> BIKE</b>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="border: none !important;">
                {{ $hAddress }}
            </td>
            <td style="border: none !important;">
                TIN:1000041335
            </td>
        </tr>
    </table>

    <div style="width: 100%; height:auto;">
        <table>
            <tr>
                <td style="border: none !important; padding: 2px 0px; font-weight: bold;">&nbsp;Fiscal Document Number
                </td>
                <td style="border: none !important; padding: 2px 0px; font-weight: bold; width: 250px;">
                    &nbsp;: {{ $result['invoiceNo'] ?? "" }}</td>

                <td style="border: none !important; padding: 2px 0px; font-weight: bold;">&nbsp;Verification Code</td>
                <td style="border: none !important; padding: 2px 0px; font-weight: bold;">
                    &nbsp;: {{ $result['antifakeCode'] ?? "" }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Customer Tin</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['buyerDetails']['buyerTin'] ?: $data['buyerDetails']['PARTYNAME'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Verma Doc No</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['sellerDetails']['referenceNo'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Customer Name</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['buyerDetails']['buyerLegalName'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Document Date</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['basicInformation']['issuedDate'] ? date("d-m-Y", strtotime($data['basicInformation']['issuedDate'])) : "" }}</td>
            </tr>
            <tr>
                <td rowspan="2" style="border: none !important; padding: 2px 0px;">&nbsp;Address</td>
                <td rowspan="2" style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $taxpayer ? $taxpayer['address'] : $data['buyerDetails']['buyerAddress'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;PO No. and Date</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['basicInformation']['issuedDate'] ? date("d-m-Y", strtotime($data['basicInformation']['issuedDate'])) : "" }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Currency</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: Ugandan Shilling</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Contact Person</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $buyerData ? $buyerData->LEDGERCONTACT : ''  }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Cost Center</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['CostCenter'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Tel No</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $buyerData ? $buyerData->LEDGERPHONE : ''  }} Fax No
                    : {{ $buyerData ? $buyerData->LEDGERFAX : ''  }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Sales Order No</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['SalesOrderNo'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;WareHouse</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $warehouse }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Payment Terms</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['Payment_Terms'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;SalesMan</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['SalesMan'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Verma Member</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['VermaMember'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Sales Reference</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['SalesRefernce'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Location</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $location }}</td>
            </tr>
        </table>
    </div>

    <div style="width: 100%; height:auto;">
        <table>
            <tr>
                <td align="center">No</td>
                <td align="center">Pr.Code</td>
                <td align="center">Item Description</td>
                <td align="center">Unit</td>
                <td align="center">Qty</td>
                <td align="center">Rate</td>
                <td align="center">Gross</td>
            </tr>
            @php
                $netTotalAmt = 0;
            @endphp
            @foreach($data['goodsDetails'] as $goodsDetails)
                @if(in_array($goodsDetails['itemCode'], ['RegistrationFees', 'NP TFR Fees', 'StampDuty', 'RiderAdvTax']))
                    @continue
                @endif
                @php
                    $unitQty = number_format($goodsDetails['qty'], 2, '.', '');
                    try {
                        if (isset($data['extra']['discount']) && $data['extra']['discount'] > 0 && is_numeric($goodsDetails['taxRate']) && $goodsDetails['taxRate'] > 0) {
                            $unitDiscount = $data['extra']['discount'] / $goodsDetails['qty'];
                            $unitDiscountBeforeTax = ($unitDiscount * (100 + $goodsDetails['taxRate'])) / 100;
                        }
                        $unitPriceBeforeTax = number_format((($goodsDetails['unitPrice'] + ($unitDiscountBeforeTax ?? 0)) * 100) / (100 + $goodsDetails['taxRate']), 2, '.', '');
                    } catch (\Throwable $th) {
                        $unitPriceBeforeTax = number_format($goodsDetails['unitPrice'], 2, '.', '');
                    }
                    $netTotalAmt += number_format($unitPriceBeforeTax * $unitQty, 2, '.', '');
                @endphp
                <tr>
                    <td>&nbsp;{{ $loop->iteration }}</td>
                    <td>&nbsp;{{ $goodsDetails['itemCode'] }}</td>
                    <td>&nbsp;{{ $goodsDetails['item'] }}</td>
                    <td align="center"><b>{{ $goodsDetails["STOCKITEMUOM"] ?? $goodsDetails['unitOfMeasure'] }}</b></td>
                    <td align="center"><b>{{ number_format($unitQty, 2, '.', ',') }}</b></td>
                    <td style="text-align: right;">{{ number_format($unitPriceBeforeTax, 2, '.', ',') }}&nbsp;
                    </td>
                    <td style="text-align: right;">{{ number_format($unitQty * $unitPriceBeforeTax, 2, '.', ',') }}
                        &nbsp;
                    </td>
                </tr>
            @endforeach
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div style="width: 100%; height:auto;">
        <table style="border: none !important;">
            <tr>
                <td width="50%" style="border: none !important;">
                    <table style="border: none !important;">
                        @php
                            $fmt = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        @endphp
                        <tr>
                            <td style="border: none !important; font-size: 0.8rem; vertical-align: top">Amount in
                                words
                            </td>
                            <td style="border: none !important; font-size: 0.8rem;">:
                                UGX {{ ucwords(str_replace('-', ' ', $fmt->format($data['summary']['grossAmount']))) }}
                                Only
                            </td>
                        </tr>
                        <tr>
                            <td style="border: none !important;">Narration</td>
                            <td style="border: none !important;">: {{ $data['extra']['sNarration'] }}</td>
                        </tr>
                        @php
                            $goodsDetails = array_values(array_filter($data['goodsDetails'], function($goods) {
                                return $goods['itemCode'] == 'NP TFR Fees';
                            }));
                            if (!empty($goodsDetails) && $goodsDetails[0]) {
                                $netTotalAmt += number_format($goodsDetails[0]['total'], 2, '.', '');
                            }
                        @endphp
                        <tr>
                            <td style="border: none !important;">NP Transfer</td>
                            <td style="border: none !important;">
                                : {{ (!empty($goodsDetails) && $goodsDetails[0]) ? number_format($goodsDetails[0]['total'], 2, '.', ',') : "" }}</td>
                        </tr>
                        @php
                            $goodsDetails = array_values(array_filter($data['goodsDetails'], function($goods) {
                                return $goods['itemCode'] == 'RegistrationFees';
                            }));
                            if (!empty($goodsDetails) && $goodsDetails[0]) {
                                $netTotalAmt += number_format($goodsDetails[0]['total'], 2, '.', '');
                            }
                        @endphp
                        <tr>
                            <td style="border: none !important;">Registration Fee</td>
                            <td style="border: none !important;">
                                : {{ (!empty($goodsDetails) && $goodsDetails[0]) ? number_format($goodsDetails[0]['total'], 2, '.', ',') : "" }}</td>
                        </tr>
                        @php
                            $goodsDetails = array_values(array_filter($data['goodsDetails'], function($goods) {
                                return $goods['itemCode'] == 'RiderAdvTax';
                            }));
                            if (!empty($goodsDetails) && $goodsDetails[0]) {
                                $netTotalAmt += number_format($goodsDetails[0]['total'], 2, '.', '');
                            }
                        @endphp
                        <tr>
                            <td style="border: none !important;">RiderAdvTax</td>
                            <td style="border: none !important;">
                                : {{ (!empty($goodsDetails) && $goodsDetails[0]) ? number_format($goodsDetails[0]['total'], 2, '.', ',') : "" }}</td>
                        </tr>
                    </table>
                </td>
                <td width="50%" style="border: none !important;">
                    <table style="border: none !important;">
                        <tr>
                            <td style="border: none !important;" width="40%" align="center">
                                <div style="border: 1px solid black !important; margin-right: 1px">
                                    <b>
                                        VAT-WHT <br>
                                        EXEMPTED WHT <br>
                                        EXEMPTED
                                    </b>
                                </div>
                            </td>
                            <td style="border: none !important;">
                                <table>
                                    @php
                                        $goodsDetails = array_values(array_filter($data['goodsDetails'], function($goods) {
                                            return $goods['itemCode'] == 'StampDuty';
                                        }));
                                        $stampDuty = 0;
                                        $discount = 0;
                                        if (!empty($goodsDetails) && $goodsDetails[0]) {
                                            $stampDuty = $goodsDetails[0]['total'];
                                        }
                                        if (!empty($data['extra']) && $data['extra']['discount']) {
                                            $discount = $data['extra']['discount'];
                                            $netTotalAmt = $netTotalAmt - $discount;
                                        }
                                    @endphp
                                    <tr>
                                        <td style="border: none !important; width: 40%">&nbsp;Net + (Incl. Charges)</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ number_format($netTotalAmt, 2, '.', ',') }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none !important;">&nbsp;Stamp Duty</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ $stampDuty ? number_format($stampDuty, 2, '.', ',') : "" }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none !important;">&nbsp;Discount</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ $discount ? '-' . number_format($discount, 2, '.', ',') : '' }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none !important;">&nbsp;Vat Tax</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ number_format($data['summary']['taxAmount'], 2, '.', ',') }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    @php
                                        $grossAmount = $netTotalAmt + $stampDuty - $discount + $data['summary']['taxAmount'];
                                        $roundedGrossAmount = round($grossAmount);
                                        $roundAmount = $roundedGrossAmount - $grossAmount;
                                    @endphp
                                    <tr>
                                        <td style="border: none !important;">&nbsp;RoundOff</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ isset($roundAmount) && $roundAmount != "0" ? round($roundAmount, 2) : "" }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none !important; font-weight: bold">&nbsp;Gross</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">
                                            {{ number_format($roundedGrossAmount, 2, '.', ',') }}&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <p style="font-size: 0.8rem">
        We Declare that this invoice shows the actual price of the goods described and that all particulars are true
        and Correct. <br>
        1.Though we shall try and respect our delivery schedule, delivery location, but in case of any change due to
        transit delay or any other
        delay beyond our control, the company shall not be responsible for any liability. <br>
        2.All Models are inclusive of Registration, Logbook, Number Plate, Transfer of Name, Helmet, Toolkits, FSC &
        Warranty Booklet. <br>
        3.Warranty of 6 Months, 1st service - 500kms/7-10 Days, 2nd service - 2500kms/15-20Days, 3rd 5000kms/30-35
        Days. (Whichever Comes Earlier).
    </p>

    <h4>GOODS ONCE SOLD ARE NOT RETURNED</h4>
    <span style="font-style: italic">This is a Computer generated document</span>
    <span style="float: right; font-weight: bold">for Verma Co Ltd.</span>

    <br><br>

    <table style="border: none !important;">
        <tr>
            <td style="border: none !important;"><b>Prepared by:</b></td>
            <td style="border: none !important;"><b>Printed
                    On:<br>{{ $data['basicInformation']['issuedDate'] ? date("d-m-Y H:i:s", strtotime($data['basicInformation']['issuedDate'])) : "" }}
                </b>
            </td>
            <td style="border: none !important;"><b>Customer Sign:</b></td>
            <td style="border: none !important;text-align: right;"><b><br>Billing Clerk</b></td>
        </tr>
        <tr>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;">&nbsp;</td>
        </tr>
        <tr>
            <td style="border: none !important;" colspan="2"><b>{{ $data['extra']['sUserName'] }}</b></td>
            <td style="border: none !important;"><b>{{ $data['buyerDetails']['buyerLegalName'] }}</b></td>
            <td style="border: none !important;text-align: right;"><b>Page number: 1 \ 1</b></td>
        </tr>
        <tr>
            <td style="border: none !important;" colspan="4">This is EFRIS integrated software generated invoice
                powered by Cabbagesoft Technologies
            </td>
        </tr>
    </table>

</div>
</body>

</html>
