@php
    $buyerData = null;
    if ($data['buyerDetails']['PARTYNAME']) {
        $buyerData = \App\Models\MasterParty::where('ID', $data['buyerDetails']['PARTYNAME'])->first();
    }

    $adoc = explode("/", $data['sellerDetails']['referenceNo']);
    $docty = $adoc[0];
    $doclc = substr($adoc[1], 0, 3);
    if ($data['extra']['iVoucherType'] == "Credit Note") {
        $adoc = "LUG";
    }
    $location = "";
    $warehouse = "";
    $hAddress = "";
    $hContact = "";
    if ($doclc == "NDW") {
        $location = "Ndeeba WH";
        $warehouse = "Ndeeba WH";
        $hAddress = "Plot No. 889-890, Masaka Road,Ndeeba";
        $hContact = "Tel: +256 706 031 012 Email: info@bajajverma.com";
    } else if ($doclc == "NDB") {
        $location = "Ndeeba Branch";
        $warehouse = "Ndeeba Branch";
        $hAddress = "Plot No. 889-890, Masaka Road,Ndeeba";
        $hContact = "Tel: +256 706 031 008 Email: info@bajajverma.com";
    } else if ($doclc == "BMB") {
        $location = "Bombo Road";
        $warehouse = "Bombo Road";
        $hAddress = "Bombo Rd, Kampala";
        $hContact = "Tel: +256 706 031 064 Email: info@bajajverma.com;";
    } else if ($doclc == "JNJ") {
        $location = "Jinja";
        $warehouse = "Jinja";
        $hAddress = "Plot 42, Iganga Road, Jinja";
        $hContact = "Tel: +256 706 031 009 Email: info@bajajverma.com";
    } else if ($doclc == "RTS") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "RSP") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "RSS") {
        $location = "Sowedi Mudehere";
        $warehouse = "Sowedi Mudehere";
        $hAddress = "Sowedi Mudehere, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "LUG") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "MSK") {
        $location = "Masaka";
        $warehouse = "Masaka";
        $hAddress = "Plot 824, Kampala Road, Nyendo, Masaka";
        $hContact = "Tel: +256 706 031 017 Email: info@bajajverma.com";
    } else if ($doclc == "MBL") {
        $location = "Mbale";
        $warehouse = "Mbale";
        $hAddress = "Plot 16/16A, Bunyoli Road, Mbale";
        $hContact = "Tel: +256 706 031 027 Email: info@bajajverma.com";
    }
@endphp

    <!DOCTYPE HTML>
<html>

<head>
    <style>
        body {
            width: 230px;
            margin: 0;
            padding: 0;
            font-size: 0.75rem;
        }

        table,
        th,
        td {
            border: 1px solid lightgray;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }
    </style>
</head>
<body>
<div>
    <table style="border: none !important;">
        <tr>
            <td style="text-align: center;"><b>Verma Co Ltd - 2023</b></td>
        </tr>
        <tr>
            <td style="text-align: center;">Plot 46, Mirembe Business Centre, Lugogo Bypass,P.O Box 33733,
                Kampala-Uganda
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;">Toll Free No - 0800 340 777</td>
        </tr>
        <tr>
            <td style="text-align: center;"><b>TAX INVOICE/CASH SALE</b></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                Fiscal Document Number: {{ $result['invoiceNo'] ?? "" }} <br>
                Verification Code: {{ $result['antifakeCode'] ?? "" }}
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <span>Scan this QR Code for URA verification.</span> <br>
                <img src="{{ $qrcode }}" alt="" height="80px">
            </td>
        </tr>
        <tr>
            <td>
                Name: {{ $data['buyerDetails']['buyerLegalName'] }}
            </td>
        </tr>
        <tr>
            <td>Inv.NO : {{ $data['sellerDetails']['referenceNo'] }}
                TIN: {{ $data['buyerDetails']['buyerTin'] ?: $data['buyerDetails']['PARTYNAME'] }}</td>
        </tr>
        <tr>
            <td>Date
                : {{ $data['basicInformation']['issuedDate'] ? date("d-m-Y", strtotime($data['basicInformation']['issuedDate'])) : "" }}</td>
        </tr>
        <tr>
            <td>Location : {{ $location }}</td>
        </tr>
        <tr>
            <td>Served By : {{ $data['extra']['sUserName'] }}</td>
        </tr>
    </table>

    <div style="width: 100%; height:auto;">
        <table>
            <tr>
                <td>Pr. Code</td>
                <td style="text-align: right;">Quantity</td>
                <td style="text-align: right;">Rate</td>
                <td style="text-align: right;">Gross</td>
            </tr>
            @foreach($data['goodsDetails'] as $goodsDetails)
                @php
                    $unitQty = number_format($goodsDetails['qty'], 2, '.', '');
                    try {
                        $unitPriceBeforeTax = number_format(($goodsDetails['unitPrice'] * 100) / (100 + $goodsDetails['taxRate']), 2, '.', '');
                    } catch (\Throwable $th) {
                        $unitPriceBeforeTax = number_format($goodsDetails['unitPrice'], 2, '.', '');
                    }
                @endphp
                <tr>
                    <td>{{ $goodsDetails['itemCode'] }} {{ $goodsDetails['item'] }}</td>
                    <td style="text-align: right;">{{ number_format($unitQty, 2, '.', ',') }}</td>
                    <td style="text-align: right;">{{ number_format($unitPriceBeforeTax, 2, '.', ',') }}</td>
                    <td style="text-align: right;">{{ number_format($unitQty * $unitPriceBeforeTax, 2, '.', ',') }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3">Net + (Incl. Charges)</td>
                <td style="text-align: right;">{{ number_format($data['summary']['netAmount'], 2, '.', ',') }}</td>
            </tr>
            <tr>
                <td colspan="3">Vat Tax</td>
                <td style="text-align: right;">{{ number_format($data['summary']['taxAmount'], 2, '.', ',') }}</td>
            </tr>
            @php
                $grossAmount = $data['summary']['grossAmount'];
                $roundedGrossAmount = round($grossAmount);
                $roundAmount = $roundedGrossAmount - $grossAmount;
            @endphp
            @if(isset($roundAmount) && $roundAmount != "0")
                <tr>
                    <td colspan="3">Round Off</td>
                    <td style="text-align: right;">{{ round($roundAmount, 2) }}</td>
                </tr>
            @endif
            <tr>
                <td colspan="3">Gross</td>
                <td style="text-align: right;">{{ number_format($roundedGrossAmount, 2, '.', ',') }}</td>
            </tr>
        </table>
    </div>

    <div style="width: 100%; height:auto;">
        <table>
            <tr>
                <td>
                    <b>GOODS ONCE SOLD ARE NOT RETURNABLE</b> <br><br>
                    We Declare that this invoice shows the acutalprice of the goods described and thatall partuculars
                    are true and correct
                </td>
            </tr>
            <tr>
                <td>SalesManName: {{ $data['extra']['SalesMan'] }}</td>
            </tr>
            <tr>
                <td>Narration: {{ $data['extra']['sNarration'] }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <b>For Verma Co LTD</b> <br>
                    Authorized signatory.
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>This is EFRIS integrated software generated invoice powered by Cabbagesoft Technologies.</td>
            </tr>
        </table>
    </div>

</div>
</body>

</html>
