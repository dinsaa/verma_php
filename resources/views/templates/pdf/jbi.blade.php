@php
    $buyerData = null;
    if ($data['buyerDetails']['PARTYNAME']) {
        $buyerData = \App\Models\MasterParty::where('ID', $data['buyerDetails']['PARTYNAME'])->first();
    }

    $adoc = explode("/", $data['sellerDetails']['referenceNo']);
    $docty = $adoc[0];
    $doclc = substr($adoc[1], 0, 3);
    if ($data['extra']['iVoucherType'] == "Credit Note") {
        $adoc = "LUG";
    }
    $location = "";
    $warehouse = "";
    $hAddress = "";
    $hContact = "";
    if ($doclc == "NDW") {
        $location = "Ndeeba WH";
        $warehouse = "Ndeeba WH";
        $hAddress = "Plot No. 889-890, Masaka Road,Ndeeba";
        $hContact = "Tel: +256 706 031 012 Email: info@bajajverma.com";
    } else if ($doclc == "NDB") {
        $location = "Ndeeba Branch";
        $warehouse = "Ndeeba Branch";
        $hAddress = "Plot No. 889-890, Masaka Road,Ndeeba";
        $hContact = "Tel: +256 706 031 008 Email: info@bajajverma.com";
    } else if ($doclc == "BMB") {
        $location = "Bombo Road";
        $warehouse = "Bombo Road";
        $hAddress = "Bombo Rd, Kampala";
        $hContact = "Tel: +256 706 031 064 Email: info@bajajverma.com;";
    } else if ($doclc == "JNJ") {
        $location = "Jinja";
        $warehouse = "Jinja";
        $hAddress = "Plot 42, Iganga Road, Jinja";
        $hContact = "Tel: +256 706 031 009 Email: info@bajajverma.com";
    } else if ($doclc == "RTS") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "RSP") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "RSS") {
        $location = "Sowedi Mudehere";
        $warehouse = "Sowedi Mudehere";
        $hAddress = "Sowedi Mudehere, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "LUG") {
        $location = "Lugogo - HO";
        $warehouse = "Lugogo";
        $hAddress = "Plot -46, Mirembe Business Center, Lugogo Bypass, Kampala";
        $hContact = "+256 414 340 777 Email: info@bajajverma.com";
    } else if ($doclc == "MSK") {
        $location = "Masaka";
        $warehouse = "Masaka";
        $hAddress = "Plot 824, Kampala Road, Nyendo, Masaka";
        $hContact = "Tel: +256 706 031 017 Email: info@bajajverma.com";
    } else if ($doclc == "MBL") {
        $location = "Mbale";
        $warehouse = "Mbale";
        $hAddress = "Plot 16/16A, Bunyoli Road, Mbale";
        $hContact = "Tel: +256 706 031 027 Email: info@bajajverma.com";
    }
@endphp

    <!DOCTYPE HTML>
<html>

<head>
    <style>
        body {
            width: 100%;
            margin: 0;
            padding: 0;
            font-size: 0.9rem;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        td {
            padding: 3px 2px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }
    </style>
</head>
<body>
<div>
    <table style="border: none !important;">
        <tr>
            <td style="border: none !important;" width="300px">
                <img src="{{ public_path('img/logo.jpg') }}" alt="" width="230px">
            </td>
            <td style="border: none !important;">
                <img src="{{ $qrcode }}" alt="" height="90px">
            </td>
            <td align="center" width="150px" style="font-size: 1.1rem">
                <b>TAX INVOICE <br>/ CASH SALES</b>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="border: none !important;">
                {{ $hAddress }}
            </td>
            <td style="border: none !important;">
                TIN:1000041335
            </td>
        </tr>
    </table>

    <div style="width: 100%; height:auto;">
        <table>
            <tr>
                <td style="border: none !important; padding: 2px 0px; font-weight: bold;">&nbsp;Fiscal Document Number
                </td>
                <td style="border: none !important; padding: 2px 0px; font-weight: bold; width: 250px;">
                    &nbsp;: {{ $result['invoiceNo'] ?? "" }}</td>

                <td style="border: none !important; padding: 2px 0px; font-weight: bold;">&nbsp;Verification Code</td>
                <td style="border: none !important; padding: 2px 0px; font-weight: bold;">
                    &nbsp;: {{ $result['antifakeCode'] ?? "" }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Customer Name</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['buyerDetails']['buyerLegalName'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Verma Doc No</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['sellerDetails']['referenceNo'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Mobile No</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $buyerData ? $buyerData->LEDGERMOBILE : ''  }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Document Date</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['basicInformation']['issuedDate'] ? date("d-m-Y", strtotime($data['basicInformation']['issuedDate'])) : "" }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Location</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: Workshop - Lugogo</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Chassis No</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['ChassisNo'] }}
                </td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Mechanic Name</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['MechanicName'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Registration No</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['RegistrationNo'] }}
                </td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Service Type</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['ServiceType'] }}
                </td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Product Code</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;:</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Time In</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['TimeIn'] }}
                </td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Sold To</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['Soldto'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Est Delivery Date</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['EstDeliveryDate'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Created By</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['sUserName'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;Est Delivery Time</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;: {{ $data['extra']['EstDeliveryTime'] }}</td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Current KM</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['CurrentKm'] }}</td>
            </tr>
            <tr>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;</td>
                <td style="border: none !important; padding: 2px 0px;">
                    &nbsp;
                </td>

                <td style="border: none !important; padding: 2px 0px;">&nbsp;Job Card No</td>
                <td style="border: none !important; padding: 2px 0px;">&nbsp;: {{ $data['extra']['JobCardNo'] }}</td>
            </tr>
        </table>
    </div>

    <div style="width: 100%; height:auto;">
        <table>
            <tr>
                <td align="center">No</td>
                <td align="center">Job Detail</td>
                <td align="center">Pr. Code</td>
                <td align="center">Item Description</td>
                <td align="center">Unit</td>
                <td align="center">Qty</td>
                <td align="center">Rate</td>
                <td align="center">Gross</td>
            </tr>
            @foreach($data['goodsDetails'] as $goodsDetails)
                @php
                    $unitQty = number_format($goodsDetails['qty'], 2, '.', '');
                    $unitPriceBeforeTax = number_format(($goodsDetails['unitPrice'] * 100) / (100 + $goodsDetails['taxRate']), 2, '.', '');
                @endphp
                <tr>
                    <td>&nbsp;{{ $loop->iteration }}</td>
                    <td>&nbsp;{{ $goodsDetails['item'] }}</td>
                    <td>&nbsp;{{ $goodsDetails['itemCode'] }}</td>
                    <td>&nbsp;{{ $goodsDetails['item'] }}</td>
                    <td align="center"><b>{{ $goodsDetails["STOCKITEMUOM"] ?? $goodsDetails['unitOfMeasure'] }}</b></td>
                    <td align="center"><b>{{ number_format($unitQty, 2, '.', ',') }}</b></td>
                    <td style="text-align: right;">{{ number_format($unitPriceBeforeTax, 2, '.', ',') }}&nbsp;
                    </td>
                    <td style="text-align: right;">{{ number_format($unitQty * $unitPriceBeforeTax, 2, '.', ',') }}
                        &nbsp;
                    </td>
                </tr>
            @endforeach
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

    <div style="width: 100%; height:auto;">
        <table style="border: none !important;">
            <tr>
                <td width="50%" style="border: none !important;">
                    <table style="border: none !important;">
                        @php
                            $fmt = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                        @endphp
                        <tr>
                            <td style="border: none !important; font-size: 0.8rem; vertical-align: top">Amount in
                                words
                            </td>
                            <td style="border: none !important; font-size: 0.8rem;">:
                                UGX {{ ucwords(str_replace('-', ' ', $fmt->format($data['summary']['grossAmount']))) }}
                                Only
                            </td>
                        </tr>
                        <tr>
                            <td style="border: none !important;">Narration</td>
                            <td style="border: none !important;">: {{ $data['extra']['sNarration'] }}</td>
                        </tr>
                    </table>
                </td>
                <td width="50%" style="border: none !important;">
                    <table style="border: none !important;">
                        <tr>
                            <td style="border: none !important;" width="40%" align="center">
                                <div style="border: 1px solid black !important; margin-right: 1px">
                                    <b>
                                        VAT-WHT <br>
                                        EXEMPTED <br>
                                        WHT EXEMPTED
                                    </b>
                                </div>
                            </td>
                            <td style="border: none !important;">
                                <table>
                                    <tr>
                                        <td style="border: none !important; width: 40%">&nbsp;Net + (Incl. Charges)</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ number_format($data['summary']['netAmount'], 2, '.', ',') }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none !important;">&nbsp;Vat Tax</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ number_format($data['summary']['taxAmount'], 2, '.', ',') }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    @php
                                        $grossAmount = $data['summary']['grossAmount'];
                                        $roundedGrossAmount = round($grossAmount);
                                        $roundAmount = $roundedGrossAmount - $grossAmount;
                                    @endphp
                                    <tr>
                                        <td style="border: none !important;">&nbsp;RoundOff</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">{{ isset($roundAmount) && $roundAmount != "0" ? round($roundAmount, 2) : "" }}
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border: none !important; font-weight: bold">&nbsp;Gross</td>
                                        <td style="border: none !important;">&nbsp;:</td>
                                        <td style="text-align: right;border: none !important;">
                                            {{ number_format($roundedGrossAmount, 2, '.', ',') }}&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <p style="font-size: 0.8rem">
        We Declare that this invoice shows the actual price of the goods described and that all particulars are true
        and Correct.
    </p>

    <h4>GOODS ONCE SOLD ARE NOT RETURNED</h4>
    <span style="font-style: italic">This is a Computer generated document</span>
    <span style="float: right; font-weight: bold">for Verma Co Ltd.</span>

    <br><br>

    <table style="border: none !important;">
        <tr>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;"><b>Printed
                    On:<br>{{ $data['basicInformation']['issuedDate'] ? date("d-m-Y H:i:s", strtotime($data['basicInformation']['issuedDate'])) : "" }}
                </b>
            </td>
            <td style="border: none !important;"><b>Customer Sign:</b></td>
            <td style="border: none !important;text-align: right;"><b><br>Billing Clerk</b></td>
        </tr>
        <tr>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;">&nbsp;</td>
            <td style="border: none !important;">&nbsp;</td>
        </tr>
        <tr>
            <td style="border: none !important;" colspan="2">&nbsp;</td>
            <td style="border: none !important;"><b>{{ $data['buyerDetails']['buyerLegalName'] }}</b></td>
            <td style="border: none !important;text-align: right;"><b>Page number: 1 \ 1</b></td>
        </tr>
        <tr>
            <td style="border: none !important;" colspan="4">This is EFRIS integrated software generated invoice
                powered by Cabbagesoft Technologies
            </td>
        </tr>
    </table>

</div>
</body>

</html>
