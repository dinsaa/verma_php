@extends('layouts.app')

@push('title', 'Invoice - Bajaj Portal')

@section('content')
    <main class="container">
        <section class="row pt-5">
            <div class="col">
                <h5>INVOICE REPORT</h5>
            </div>
            <div class="col text-end">
                <a href="{{ route('dashboard.index') }}">Back to Dashboard</a>
            </div>
        </section>

        <section class="row pt-4">
            <div class="col">
                <form action="{{ route('invoice.index') }}" method="get" autocomplete="off">
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <td><label for="customer_name">Customer Name</label></td>
                            <td><label for="unique_no">Ref / FDN No</label></td>
                            <td><label for="from_date">From Date</label></td>
                            <td><label for="to_date">To Date</label></td>
                            <td><label for="status">Status</label></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="text" name="customer_name" id="customer_name" class="form-control"
                                       value="{{ request()->input('customer_name') }}">
                            </td>
                            <td>
                                <input type="text" name="unique_no" id="unique_no" class="form-control"
                                       value="{{ request()->input('unique_no') }}">
                            </td>
                            <td>
                                <input type="date" name="from_date" id="from_date" class="form-control"
                                       value="{{ request()->input('from_date') }}" max="{{ date('Y-m-d') }}">
                            </td>
                            <td>
                                <input type="date" name="to_date" id="to_date" class="form-control"
                                       value="{{ request()->input('to_date') }}" max="{{ date('Y-m-d') }}">
                            </td>
                            <td>
                                <select name="status" id="status" class="form-select">
                                    <option value="">All</option>
                                    <option value="success" @if(request()->input('status') == 'success')
                                        {{ 'selected' }}
                                        @endif>Success
                                    </option>
                                    <option value="failed" @if(request()->input('status') == 'failed')
                                        {{ 'selected' }}
                                        @endif>Failed
                                    </option>
                                </select>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-success">Search</button>
                                <button type="button" class="btn btn-danger"
                                        onclick="window.location = window.location.pathname;">Clear
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </section>

        <section class="row">
            <div class="col">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Sr No</th>
                        <th scope="col">Date</th>
                        <th scope="col">Ref No</th>
                        <th scope="col">FDN No</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $row)
                        @php
                            $reqBody = json_decode($row->req_body, true);
                            $resBody = json_decode($row->res_body, true);
                        @endphp
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td scope="row">{{ date('d-m-Y', strtotime($reqBody['basicInformation']['issuedDate'])) }}</td>
                            <td>{{ $reqBody['sellerDetails']['referenceNo'] }}</td>
                            <td>{{ $resBody['invoiceNo'] ?? "" }}</td>
                            @if($row->status == 1)
                                <td>Success</td>
                                <td>
                                    {{-- <a href="{{ $resBody['pdf'] ?? "" }}" target="_blank"
                                       class="text-decoration-none link-primary">PDF-1</a> --}}
                                    <a href='{{ Storage::url($reqBody["sellerDetails"]["REFERENCE"] . '.pdf') ?? "" }}'
                                       target="_blank" class="text-decoration-none link-info">PDF</a>
                                </td>
                            @elseif($row->status == 2)
                                <td>
                                    Failed
                                    <span data-bs-toggle="popover" data-bs-placement="bottom"
                                          data-bs-content="{{ $resBody['error_message'] ?? '' }}" role="button">&#9432;</span>
                                </td>
                                <td>
                                    <a href="{{ route('invoice.upload', ['vch_no' => $reqBody['sellerDetails']['referenceNo']]) }}"
                                       target="_blank" class="text-decoration-none link-danger">Re-Upload</a>
                                </td>
                            @elseif($row->status == 3)
                                <td>
                                    Failed
                                    <span data-bs-toggle="popover" data-bs-placement="bottom"
                                          data-bs-content="{{ $resBody['error_message'] ?? '' }}" role="button">&#9432;</span>
                                </td>
                                <td></td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $data->withQueryString()->links() }}
            </div>
        </section>
    </main>
@endsection
