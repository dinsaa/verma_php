<?php
//echo "========".$fromdate."<bR>";
$dt = explode("/", $fromdate);
$sDate = $dt[2] . "-" . $dt[1] . "-" . $dt[0];
$tDate = $dt[2] . $dt[1] . $dt[0];
echo "Fetching Invoice For (" . $sDate . ")<bR>";
$sql = "SELECT d.[iTransactionId]
				,Convert(date, convert(varchar, (iDate % 256)) + '/' +
                     convert(varchar, (iDate / 256 % 256)) + '/' +
                     convert(varchar, (iDate / 65536)), 103) as dDate
				,h.[sVoucherNo],h.[iVoucherType],d.[iHeaderId],d.[iBodyId]
				,u.[sUserName],d.[iBookNo],cs.[sName]
				,i.[iProduct],i.[iUnit],i.[fQuantity],i.[mRate],d.[mAmount2]
				,i.[mGross],d.[mAmount1],d.[mAmount3],d.[mOriginalAmount]
				,s.[mInput0],s.[mVal0],s.[mInput1],s.[mVal1]
				,s.[mInput2],s.[mVal2],s.[mInput3],s.[mVal3]
				,s.[mInput4],s.[mVal4],s.[mInput5],s.[mVal5]
				,s.[mInput6],s.[mVal6],s.[mInput7],s.[mVal7]
				,s.[mInput8],s.[mVal8],s.[mInput9],s.[mVal9]
				,CASE h.iVoucherType
					WHEN 3332 THEN D3332.[Description]
					WHEN 3333 THEN D3333.[Description]
					WHEN 3334 THEN D3334.[Description]
					WHEN 3335 THEN D3335.[Description]
					WHEN 3337 THEN D3337.[Description]
					WHEN 3338 THEN D3338.[Description]
					WHEN 3339 THEN D3339.[Description]
					WHEN 3340 THEN D3340.[Description]
				END as Description
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[sNarration]
					WHEN 3333 THEN H3333.[sNarration]
					WHEN 3334 THEN H3334.[sNarration]
					WHEN 3335 THEN H3335.[sNarration]
					WHEN 3337 THEN H3337.[sNarration]
					WHEN 3338 THEN H3338.[sNarration]
					WHEN 3339 THEN H3339.[sNarration]
					WHEN 3340 THEN H3340.[sNarration]
				END as sNarration
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Delivery_Terms]
					WHEN 3333 THEN H3333.[Delivery_Terms]
					WHEN 3334 THEN H3334.[Delivery_Terms]
					WHEN 3335 THEN H3335.[Delivery_Terms]
					WHEN 3337 THEN H3337.[Delivery_Terms]
					WHEN 3338 THEN H3338.[Delivery_Terms]
					WHEN 3339 THEN H3339.[Delivery_Terms]
				END as Delivery_Terms
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Delivery_At]
					WHEN 3333 THEN H3333.[Delivery_At]
					WHEN 3334 THEN H3334.[Delivery_At]
					WHEN 3335 THEN H3335.[Delivery_At]
					WHEN 3337 THEN H3337.[Delivery_At]
					WHEN 3338 THEN H3338.[Delivery_At]
					WHEN 3339 THEN H3339.[Delivery_At]
				END as Delivery_At
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Payment_Terms]
					WHEN 3333 THEN H3333.[Payment_Terms]
					WHEN 3334 THEN H3334.[Payment_Terms]
					WHEN 3335 THEN H3335.[Payment_Terms]
					WHEN 3337 THEN H3337.[Payment_Terms]
					WHEN 3338 THEN H3338.[Payment_Terms]
					WHEN 3339 THEN H3339.[Payment_Terms]
				END as Payment_Terms
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[PO_No]
					WHEN 3333 THEN H3333.[PO_No]
					WHEN 3334 THEN H3334.[PO_No]
					WHEN 3335 THEN H3335.[PO_No]
					WHEN 3337 THEN H3337.[PO_No]
					WHEN 3338 THEN H3338.[PO_No]
					WHEN 3339 THEN H3339.[PO_No]
				END as PO_No
				,CASE h.iVoucherType
					WHEN 3332 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3332.[PO_Date]), 105)
					WHEN 3333 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3333.[PO_Date]), 105)
					WHEN 3334 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3334.[PO_Date]), 105)
					WHEN 3335 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3335.[PO_Date]), 105)
					WHEN 3337 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3337.[PO_Date]), 105)
					WHEN 3338 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3338.[PO_Date]), 105)
					WHEN 3339 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3339.[PO_Date]), 105)
				END as PO_Date
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Receiver_Name]
					WHEN 3333 THEN H3333.[Receiver_Name]
					WHEN 3334 THEN H3334.[Receiver_Name]
					WHEN 3335 THEN H3335.[Receiver_Name]
					WHEN 3337 THEN H3337.[Receiver_Name]
					WHEN 3338 THEN H3338.[Receiver_Name]
					WHEN 3339 THEN H3339.[Receiver_Name]
				END as Receiver_Name
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Receiver_Mobile_No]
					WHEN 3333 THEN H3333.[Receiver_Mobile_No]
					WHEN 3334 THEN H3334.[Receiver_Mobile_No]
					WHEN 3335 THEN H3335.[Receiver_Mobile_No]
					WHEN 3337 THEN H3337.[Receiver_Mobile_No]
					WHEN 3338 THEN H3338.[Receiver_Mobile_No]
					WHEN 3339 THEN H3339.[Receiver_Mobile_No]
				END as Receiver_Mobile_No
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Available_Points]
					WHEN 3333 THEN H3333.[Available_Points]
					WHEN 3334 THEN H3334.[Available_Points]
					WHEN 3335 THEN H3335.[Available_Points]
					WHEN 3337 THEN H3337.[Available_Points]
					WHEN 3339 THEN H3339.[Available_Points]
				END as Available_Points
				,f.[mInput1],f.[mVal1],
				cc.[sName] as CostCenter,
				CASE h.iVoucherType
					WHEN 3332 THEN H5635.[PO_No]
					WHEN 3333 THEN H5637.[PO_No]
					WHEN 3335 THEN H5639.[PO_No]
					WHEN 3337 THEN H5643.[PO_No]
				END as SalesOrderNo,
				vm.[sName] as VermaMember,sm.[sName] as SalesMan,
				sf.[sName] as SalesRefernce,mc.[sName] as MechanicName,
				st.[sCode] as ServiceType,
				CASE h.iVoucherType WHEN 3337 THEN H3337.[Time_In]
				END as TimeIn,
				CASE h.iVoucherType WHEN 3337 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H5643.[Est_Delivery_Date]), 105)
				END as EstDeliveryDate,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[Est_Delivery_Time]
				END as EstDeliveryTime,
				v.[sName] as ChassisNo,v.[sCode] as RegistrationNo,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[Sold_To]
				END as Soldto,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[Current_KM]
				END as CurrentKm,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[PO_No]
				END as JobCardNo,
				CONVERT(VARCHAR(10), [dbo].[IntToDate](iDate), 105) as Vdate
			FROM [Focus8040].[dbo].[tCore_Data_0] as d
				LEFT JOIN [Focus8040].[dbo].[tCore_Header_0] as h  on h.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3332_0] as H3332 on H3332.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3333_0] as H3333 on H3333.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3334_0] as H3334 on H3334.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3335_0] as H3335 on H3335.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3337_0] as H3337 on H3337.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3338_0] as H3338 on H3338.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3339_0] as H3339 on H3339.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3340_0] as H3340 on H3340.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Indta_0] as i  on i.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[vCore_Account] as cs  ON cs.iMasterId = d.iBookNo
				LEFT JOIN [Focus8040].[dbo].[mSec_Users] as u ON u.iUserId=h.iUserId
				LEFT JOIN [Focus8040].[dbo].[tCore_IndtaBodyScreenData_0] as s ON s.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3332_0] as D3332 ON D3332.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3333_0] as D3333 ON D3333.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3334_0] as D3334 ON D3334.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3335_0] as D3335 ON D3335.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3337_0] as D3337 ON D3337.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3338_0] as D3338 ON D3338.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3339_0] as D3339 ON D3339.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3340_0] as D3340 ON D3340.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_IndtaFooterScreenData_0] as f ON f.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data_Tags_0] as t  ON t.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[mCore_CostCenter] as cc on cc.[iMasterId] = t.[iTag5]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5635_0] as H5635 on H5635.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5637_0] as H5637 on H5637.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5639_0] as H5639 on H5639.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5643_0] as H5643 on H5643.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[mCore_VermaMember] as vm on vm.[iMasterId]=t.[iTag3019]
				LEFT JOIN [Focus8040].[dbo].[mCore_SalesMan] as sm on sm.[iMasterId]=t.[iTag3007]
				LEFT JOIN [Focus8040].[dbo].[mCore_SalesRefernce] as sf on sf.[iMasterId]=t.[iTag3021]
				LEFT JOIN [Focus8040].[dbo].[mCore_Mechanic] as mc on mc.[iMasterId]=t.[iTag3025]
				LEFT JOIN [Focus8040].[dbo].[mCore_ServiceType] as st on st.[iMasterId]=t.[iTag3026]
				LEFT JOIN [Focus8040].[dbo].[mCore_Vehicle] as v on v.[iMasterId] = t.[iTag3002]
			where i.[iBodyId] = d.[iBodyId]
				and h.iVoucherType in (3332,3333,3334,3335,3337,3339,3340)
				and Convert(date, convert(varchar, (iDate % 256)) + '/' +
                     convert(varchar, (iDate / 256 % 256)) + '/' +  convert(varchar, (iDate / 65536)), 103)='" . $sDate . "'
			order by cs.sName desc"; //,3338 //and i.[fQuantity]!=0
//			echo $sql."<br>"; exit;
//tCore_Data_0,tCore_Header_0,tCore_HeaderData3332_0,tCore_Data3332_0
mysqli_query($conni, 'DELETE FROM `invoice_details` where id in (select ID from transaction where DATE="' . $tDate . '")');
mysqli_query($conni, 'DELETE FROM transaction where DATE="' . $tDate . '"');
$sqltr = "Select ID FROM `transaction` where DATE=" . $tDate;
/* $transaction=array();
	$resulttr = mysqli_query($conni,$sqltr);
	while($rowtr =  mysqli_fetch_array($resulttr,MYSQLI_ASSOC)) {
		$transaction[$rowtr["ID"]]=$rowtr["ID"];
	} */
$cur = odbc_exec($sqlsrv_conn, $sql);
//$INV = getInvoiceList();
//echo "<pre>"; print_r($INV); echo "</pre>"; exit;

while (odbc_fetch_row($cur)) {
    $ID = odbc_result($cur, 1);
    $DATE = str_replace("-", "", odbc_result($cur, 2));
    //if (getInvoiceList(odbc_result( $cur, 3 )) ) {
    //$VOUCHERNUMBER= odbc_result( $cur, 3 );
    //$VOUCHERNUMBER= odbc_result( $cur, 3 )."-C";
    //} else {
    $VOUCHERNUMBER = odbc_result($cur, 3);
    //}
    //echo $VOUCHERNUMBER."<br>";

    $VOUCHERTYPENAME = odbc_result($cur, 4);
    $REFERENCE = odbc_result($cur, 5);
    $GUID = odbc_result($cur, 6);
    $ENTEREDBY = odbc_result($cur, 7);
    $PARTYNAME = odbc_result($cur, 8);
    $PARTYLEDGERNAME = odbc_result($cur, 9);
    $STOCKITEMNAME = odbc_result($cur, 10);
    $RATEVALUE = odbc_result($cur, 11);
    $QTY = round(odbc_result($cur, 12), 2);
    if ($QTY < 0) {
        $QTY = $QTY * -1;
    }
    if ($QTY == 0) {
        $QTY = 1;
    }
    //d.[iBodyId]
    $RATEVALUE = odbc_result($cur, 13);
    $AMOUNT = odbc_result($cur, 15);
    if ($AMOUNT == 0) {
        $AMOUNT = odbc_result($cur, 16);
    }
    if ($RATEVALUE == 0) {
        $RATEVALUE = odbc_result($cur, 16);
    }
    if ($AMOUNT < 0) {
        $AMOUNT = $AMOUNT * -1;
    }
    //if ($Amount==0 && $STOCKITEMNAME="2543") {$AMOUNT=odbc_result( $cur, 16);}
    //if ($STOCKITEMNAME=="20") { //$AMOUNT=round($AMOUNT/1.18);$RATEVALUE=round($RATEVALUE/1.18);
    //	$VAT=0;
    //} else { $VAT=round($AMOUNT*.18,2); }
    $AMOUNT = round($AMOUNT, 2);
    $RATEVALUE = round($RATEVALUE, 2);
    if ($VAT < 0) {
        $VAT = $VAT * -1;
    }
    $Sales = odbc_result($cur, 15);
    $DiscountPer = 0;
    $Discount = 0;
    if (odbc_result($cur, 19) > 0 && odbc_result($cur, 19) <= 100) {
        $DiscountPer = odbc_result($cur, 19);
        $Discount = $AMOUNT * ($DiscountPer / 100); //odbc_result( $cur, 20 );
        //echo $AMOUNT."==".$DiscountPer."==".($DiscountPer/100)."==".$Discount."<br>";
    } else if (odbc_result($cur, 19) > 100) {
        $Discount = odbc_result($cur, 19);
        $DiscountPer = round(($Discount / $AMOUNT) * 100, 2); //odbc_result( $cur, 20 );
        //echo $AMOUNT."==".$DiscountPer."==".$Discount."<br>";
    }

    $NetSales = odbc_result($cur, 22); //21,22

    $VATTax = odbc_result($cur, 24); //23,24
    //if (floatval($VATTax)==0) { $VAT=round($AMOUNT*.18,2); $AMOUNT= $AMOUNT-$VAT;}
    //else{
    $VAT = round(floatval($VATTax), 2); //}
    //echo $VAT."==================VAT===================".$AMOUNT."<br>";
    //echo $VATTax."==================VATTax===================<br>";
    $RegistrationFee = odbc_result($cur, 26); //25,26 --
    $NPTransferFee = odbc_result($cur, 28); //27,28 --
    $StampDuty = odbc_result($cur, 30); //29,30 --
    $LoyalityAddition = odbc_result($cur, 32); //31,32 --
    $NetCost = odbc_result($cur, 34); //33,34
    $CustNet = odbc_result($cur, 36); //35,36
    $RiderAdvTax = floatval(odbc_result($cur, 38)); //37,38 --

    $Description = odbc_result($cur, 39);
    $NARRATION = odbc_result($cur, 40);

    $Delivery_Terms = odbc_result($cur, 41);
    $Delivery_At = odbc_result($cur, 42);
    $Payment_Terms = odbc_result($cur, 43);
    $PO_No = odbc_result($cur, 44);
    $PO_Date = odbc_result($cur, 45);
    //echo "<br>".$VOUCHERNUMBER."======".odbc_result( $cur, 45 );
    $Receiver_Name = odbc_result($cur, 46);
    $Receiver_Mobile_No = odbc_result($cur, 47);
    $Available_Points = odbc_result($cur, 48);

    $CostCenter = odbc_result($cur, 51);  //100
    $SalesOrderNo = odbc_result($cur, 52); //30
    $VermaMember = odbc_result($cur, 53); //100
    $SalesMan = odbc_result($cur, 54); //100
    $SalesRefernce = odbc_result($cur, 55); //100
    $MechanicName = odbc_result($cur, 56); //100
    $ServiceType = odbc_result($cur, 57); //100
    $TimeIn = odbc_result($cur, 58); //int
    $EstDeliveryDate = str_replace("-", "", odbc_result($cur, 59));  //int
    $EstDeliveryTime = odbc_result($cur, 60); //int
    $ChassisNo = odbc_result($cur, 61); //100
    $RegistrationNo = odbc_result($cur, 62); //100
    $Soldto = odbc_result($cur, 63);  //int
    $CurrentKm = odbc_result($cur, 64); //int
    $JobCardNo = odbc_result($cur, 65); //30
    //exit;
    $QtyConv = 1;
    /* $sql = "Select ID,DATE,VOUCHERNUMBER,VOUCHERTYPENAME,REFERENCE,ENTEREDBY,PARTYNAME,
				PARTYLEDGERNAME,STOCKITEMNAME,RATEVALUE,QTY,UNIT,VAT,AMOUNT,Sales,GUID
				FROM `transaction` where id=".$ID;
		$result = mysqli_query($conni,$sql);
		$count =0;$count = mysqli_num_rows($result);
	//echo $sql."<br>";
		if ($count>0) {	*/
    //echo "-----".$transaction[$ID]."<==>".$ID."<br>";
    /* if ($transaction[$ID]==$ID) {
			$sql1 = 'update transaction set DATE="'.$DATE.'" ,VOUCHERNUMBER="'.$VOUCHERNUMBER.'" ,
						VOUCHERTYPENAME="'.$VOUCHERTYPENAME.'" ,REFERENCE="'.$REFERENCE.'" ,
						ENTEREDBY="'.$ENTEREDBY.'", PARTYNAME="'.$PARTYNAME.'",
						PARTYLEDGERNAME="'.$PARTYLEDGERNAME.'", STOCKITEMNAME="'.$STOCKITEMNAME.'",
						RATEVALUE='.$RATEVALUE.', QTY='.$QTY.',UNIT="'.$UNIT.'",
						VAT='.$VAT.', AMOUNT='.$AMOUNT.', Sales='.$RegistrationFee.',
						SalesTyres='.$NPTransferFee.',SalesExempted='.$StampDuty.',
						SalesZeroRate='.$LoyalityAddition.',SalesExport='.$RiderAdvTax.',
						DISCOUNT='.$Discount.',ADDLCOSTPERC='.$DiscountPer.',
						GUID="'.$GUID.'",PRICELEVEL="'.$Description.'",NARRATION="'.$NARRATION.'",
						BASICORDERREF="'.$Delivery_Terms.'",MFDON="'.$Delivery_At.'",
						BASICORDERTERMS="'.$Payment_Terms.'",
						BASICPURCHASEORDERNO="'.$PO_No.'",BASICORDERDATE="'.$PO_Date.'",
						EXPIRYPERIOD="'.$Receiver_Name.'",GODOWNNAME="'.$Receiver_Mobile_No.'",
						BATCHNAME="'.$Available_Points.'"
						WHERE id ='.$ID;
						//if ($VOUCHERNUMBER=="CS/NDB21/0250") {
			echo $sql1."<br>";
						//}
			//mysqli_query($conni,$sql1);
		} else { */
    $sql1 = 'insert into transaction (ID,DATE,VOUCHERNUMBER,VOUCHERTYPENAME,REFERENCE,
						ENTEREDBY,PARTYNAME,PARTYLEDGERNAME,STOCKITEMNAME,RATEVALUE,QTY,UNIT,
						VAT,AMOUNT,Sales,SalesTyres,SalesExempted,SalesZeroRate,SalesExport,
						DISCOUNT,ADDLCOSTPERC,GUID,PRICELEVEL,NARRATION,
						BASICORDERREF,MFDON,BASICORDERTERMS,BASICPURCHASEORDERNO,
						BASICORDERDATE,EXPIRYPERIOD,GODOWNNAME,BATCHNAME) VALUES (
						"' . $ID . '" ,"' . $DATE . '" ,
						"' . $VOUCHERNUMBER . '" ,"' . $VOUCHERTYPENAME . '","' . $REFERENCE . '",
						"' . $ENTEREDBY . '" ,"' . $PARTYNAME . '","' . $PARTYLEDGERNAME . '",
						"' . $STOCKITEMNAME . '" ,' . $RATEVALUE . ',' . $QTY . ',"' . $UNIT . '" ,' . $VAT . ',
						' . $AMOUNT . ',' . $RegistrationFee . ',' . $NPTransferFee . ',' . $StampDuty . ',
						' . Floatval($LoyalityAddition) . ',' . $RiderAdvTax . ',' . $Discount . ',' . $DiscountPer . ',"' . $GUID . '",
						"' . $Description . '","' . $NARRATION . '","' . $Delivery_Terms . '","' . $Delivery_At . '"
						,"' . $Payment_Terms . '","' . $PO_No . '","' . $PO_Date . '","' . $Receiver_Name . '"
						,"' . $Receiver_Mobile_No . '","' . $Available_Points . '")';
    //echo $sql1."<br>";
    mysqli_query($conni, $sql1);
    //}

    /* $sql2 = "Select * FROM `invoice_details` where id=".$ID;
		$result2 = mysqli_query($conni,$sql2);
		$count2 =0;$count2 = mysqli_num_rows($result2);
	//echo $sql."<br>";
		if ($count2>0) {	 */
    /* if ($transaction[$ID]==$ID) {
			$sql12 = 'update invoice_details set CostCenter="'.$CostCenter.'" ,SalesOrderNo="'.$SalesOrderNo.'" ,
						VermaMember="'.$VermaMember.'" ,SalesMan="'.$SalesMan.'" ,
						SalesRefernce="'.$SalesRefernce.'", MechanicName="'.$MechanicName.'",
						ServiceType="'.$ServiceType.'", TimeIn='.$TimeIn.',
						EstDeliveryDate="'.$EstDeliveryDate.'", EstDeliveryTime='.$EstDeliveryTime.',
						ChassisNo="'.$ChassisNo.'", RegistrationNo="'.$RegistrationNo.'",
						Soldto='.$Soldto.', CurrentKm='.$CurrentKm.', JobCardNo="'.$JobCardNo.'"
						WHERE id ='.$ID;
			echo $sql12."<br>";
			//mysqli_query($conni,$sql12);
		} else { */
    $sql12 = 'insert into invoice_details (ID,CostCenter,SalesOrderNo,VermaMember,SalesMan,
						SalesRefernce,MechanicName,ServiceType,TimeIn,EstDeliveryDate,EstDeliveryTime,
						ChassisNo,RegistrationNo,Soldto,CurrentKm,JobCardNo) VALUES (
						' . $ID . ' ,"' . $CostCenter . '" ,"' . $SalesOrderNo . '" ,"' . $VermaMember . '","' . $SalesMan . '",
						"' . $SalesRefernce . '" ,"' . $MechanicName . '","' . $ServiceType . '",' . intval($TimeIn) . ',
						"' . $EstDeliveryDate . '",' . intval($EstDeliveryTime) . ',"' . $ChassisNo . '","' . $RegistrationNo . '",
						' . intval($Soldto) . ',' . intval($CurrentKm) . ',"' . $JobCardNo . '")';
    //secho $sql12."<br>";
    mysqli_query($conni, $sql12);
    //}


}
//exit;

$sql = "Select DISTINCT t.VOUCHERNUMBER,t.DATE,t.VOUCHERTYPENAME,t.REFERENCE,t.ENTEREDBY,
				t.PARTYNAME,t.PARTYLEDGERNAME,p.VATTINNUMBER,p.Address1,
				p.Address2,p.Address3,p.Address4,p.LEDGERMOBILE,p.LEDGERPHONE,p.LEDGEREMAIL,
				e.invoiceno,
                (Select i3.STOCKITEMNAME from transaction as t2
                 	left join masteritem as i3 on i3.id=t2.STOCKITEMNAME
                 		where t2.VOUCHERNUMBER=t.VOUCHERNUMBER
                 		and (i3.ura_commodityCategoryId=''
                        or i3.ura_commodityCategoryId is null) limit 1)
						as STOCKNAME,
				(Select count(id) from transaction as t0
                 		where t0.VOUCHERNUMBER=t.VOUCHERNUMBER
                 		and (select count(id) from masteritem as i
						where i.id=t0.STOCKITEMNAME and (i.ura_commodityCategoryId=''
							or i.ura_commodityCategoryId is null)) > 0)
						as chkCommodityCode,
				(Select count(id) from transaction as t00
                 		where t00.VOUCHERNUMBER=t.VOUCHERNUMBER
                 		and (select count(id) from masteritem as i2
						where i2.id=t00.STOCKITEMNAME and
								(i2.ura_goodsCode='' or i2.ura_goodsCode is null)) > 0)
						as chkGoodsCode,
                 (Select sum(amount) from transaction as t1
						where t1.VOUCHERNUMBER=t.VOUCHERNUMBER ) as TAmount,
                  (Select sum(vat) from transaction as t1
						where t1.VOUCHERNUMBER=t.VOUCHERNUMBER ) as TVat
				FROM `transaction` as t
                left join masterparty as p on p.ID=t.PARTYNAME
				left join efris_invoice as e on e.VOUCHERNUMBER=t.VOUCHERNUMBER
				where t.date='" . str_replace("-", "", $sDate) . "'
                ORDER BY DATE,VOUCHERNUMBER";

//echo $sql."<br>"; exit;
$result = mysqli_query($conni, $sql);
$count = 0;
$count = mysqli_num_rows($result);

$h = "<table width='100%' >
			<thead><tr><td><b>DATE</b></td><td><b>VOUCHERNUMBER</b></td>
			<td><b>EFRISDocID</b></td>
			<td><b>PARTYLEDGERNAME</b></td><td><b>VAT</b></td><td><b>AMOUNT</b></td>
			<td><b>VATTINNUMBER</b></td></tr></thead><tbody>";
$h2 = "<table width='100%' >
			<thead><tr><td><b>DATE</b></td><td><b>VOUCHERNUMBER</b></td>
			<td><b>MissingItem</b></td>
			<td><b>PARTYLEDGERNAME</b></td><td><b>VAT</b></td><td><b>AMOUNT</b></td>
			<td><b>VATTINNUMBER</b></td></tr></thead><tbody>";
//<td><b>Address1</b></td><td><b>Address2</b></td>

//<td><b>Address3</b></td><td><b>Address4</b></td><td><b>LEDGERMOBILE</b></td>
//	<td><b>LEDGERPHONE</b></td><td><b>LEDGEREMAIL</b></td></tr>";
$l0 = "";
//exit;
if ($count > 0) {
    $v = "";
    $TAmount = 0;
    $TVat = 0;
    $bgcolor = "";
    $nrow = array();
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        //echo "<pre>"; print_r($row); echo "</pre>";
        $ID = $row["ID"];
        $DATE = intval($row["DATE"]);
        $invoiceno = $row["invoiceno"];
        $VOUCHERNUMBER = $row["VOUCHERNUMBER"];
        $VOUCHERTYPENAME = $row["VOUCHERTYPENAME"];
        $REFERENCE = $row["REFERENCE"];
        $ENTEREDBY = $row["ENTEREDBY"];
        $PARTYLEDGERNAME = $row["PARTYLEDGERNAME"];
        $chkCommodityCode = $row["chkCommodityCode"];
        $STOCKNAME = $row["STOCKNAME"];
        $chkGoodsCode = $row["chkGoodsCode"];
        //$STOCKITEMNAME=$row["STOCKITEMNAME"];
        //$ura_goodsCode=$row["ura_goodsCode"];$StockCode=$row["StockCode"];
        //$ura_commodityCategoryId=$row["ura_commodityCategoryId"];$RATEVALUE=$row["RATEVALUE"];
        //$QTY=$row["QTY"];$ura_measureUnit=$row["ura_measureUnit"];$VAT=$row["VAT"];
        //$AMOUNT=$row["AMOUNT"];$GUID=$row["GUID"];
        $TAmount = $row["TAmount"];
        $TVat = $row["TVat"];
        $VATTINNUMBER = $row["VATTINNUMBER"];
        $Address1 = $row["Address1"];
        $Address2 = $row["Address2"];
        $Address3 = $row["Address3"];
        $Address4 = $row["Address4"];
        $LEDGERMOBILE = $row["LEDGERMOBILE"];
        $LEDGERPHONE = $row["LEDGERPHONE"];
        $LEDGEREMAIL = $row["LEDGEREMAIL"];
        //$TAmount=$TAmount+$AMOUNT;$TVat=$TVat+$VAT;
        if ($chkCommodityCode > 0) {
            $bgcolor = "style='background: sienna;'";
        }
        if ($chkGoodsCode > 0) {
            $bgcolor = "style='background: darkgoldenrod;'";
        }
        if ($invoiceno != "") {
            $bgcolor = "style='background: darkgreen;'";
        }
        //<td>".$StockCode."</td><td>".$STOCKITEMNAME."</td><td>".$ura_goodsCode."</td>
        //<td>".$ura_commodityCategoryId."</td><td>".$RATEVALUE."</td>
        //<td>".$QTY."</td><td>".$ura_measureUnit."</td>
        //if ($v!=$VOUCHERNUMBER && $v!="") {
        if ($invoiceno != "") {
            $l0 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
						<td>" . $VOUCHERNUMBER . "</td><td>" . $invoiceno . "</td>
						<td>" . $PARTYLEDGERNAME . "</td>
						<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
						<td>" . $VATTINNUMBER . "</td></tr>";
            //<td>".$Address1."</td>
            //<td>".$Address2."</td><td>".$Address3."</td>
            //<td>".$Address4."</td><td>".$LEDGERMOBILE."</td>
            //<td>".$LEDGERPHONE."</td><td>".$LEDGEREMAIL."</td>
            //</tr>";
        } else if ($chkCommodityCode > 0) {
            $l1 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
						<td>" . $VOUCHERNUMBER . "</td><td>" . $STOCKNAME . "</td>
						<td>" . $PARTYLEDGERNAME . "</td>
						<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
						<td>" . $VATTINNUMBER . "</td></tr>";
            //<td>".$Address1."</td>
            //<td>".$Address2."</td><td>".$Address3."</td>
            //<td>".$Address4."</td><td>".$LEDGERMOBILE."</td>
            //<td>".$LEDGERPHONE."</td><td>".$LEDGEREMAIL."</td>
            //</tr>";
        } else if ($chkGoodsCode > 0) {
            $l2 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
						<td>" . $VOUCHERNUMBER . "</td><td>" . $invoiceno . "</td>
						<td>" . $PARTYLEDGERNAME . "</td>
						<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
						<td>" . $VATTINNUMBER . "</td></tr>";
            //<td>".$Address1."</td>
            //<td>".$Address2."</td><td>".$Address3."</td>
            //<td>".$Address4."</td><td>".$LEDGERMOBILE."</td>
            //<td>".$LEDGERPHONE."</td><td>".$LEDGEREMAIL."</td>
            //</tr>";
        }
        if ($VOUCHERNUMBER == "CS/LUG21/7586") {
            echo $bgcolor . "---" . $TAmount . "===<pre>";
            print_r($row);
            echo "</pre>";
        }
        if ($bgcolor == "" && $TAmount != 0) {

            $sql1 = "Select t.ID,p.Trans,t.DATE,t.VOUCHERNUMBER,t.VOUCHERTYPENAME,
						t.REFERENCE,t.ENTEREDBY,t.PARTYNAME,t.PARTYLEDGERNAME,
						t.STOCKITEMNAME as StockCode,t.RATEVALUE,t.DISCOUNT,t.ADDLCOSTPERC,t.QTY,
						t.VAT,t.AMOUNT,t.SalesExempted,t.SalesZeroRate, t.Sales,t.SalesTyres, t.SalesExport,
						t.GUID,
						i.STOCKITEMNAME,i.ura_goodsCode,i.ura_commodityCategoryId,i.ura_measureUnit,
						p.VATTINNUMBER,p.Address1,p.Address2,p.Address3,p.Address4,
						p.LEDGERMOBILE,p.LEDGERPHONE,p.LEDGEREMAIL,
						e.invoiceno
						FROM `transaction` as t
						left join masteritem as i on i.id=t.STOCKITEMNAME
						left join masterparty as p on p.ID=t.PARTYNAME
						left join efris_invoice as e on e.VOUCHERNUMBER=t.VOUCHERNUMBER
						where t.VOUCHERNUMBER='" . $VOUCHERNUMBER . "'
						ORDER BY DATE,VOUCHERNUMBER";
            //echo $sql1."<br>"; exit;
            $result1 = mysqli_query($conni, $sql1);
            $count1 = 0;
            $count1 = mysqli_num_rows($result1);
            if ($count1 > 0) {
                $v = "";
                $ps = -1;
                $nrow = array();
                $param = array();
                $isItemEmpty = false;
                while ($row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC)) {
                    $ps = $ps + 1;
                    //echo "<pre style='color:#fff;'>111111["; print_r( $row1); echo "]</pre>";
                    if ($row1["ura_goodsCode"] == "") {
                        $isItemEmpty = true;
                    }
                    $nrow[$ps] = $row1;
                }
                //echo "<pre style='color:#fff;'>22222222["; print_r( $nrow); echo "]</pre>";
                //if ( $isItemEmpty == false ) {
                $adoc = explode("/", $nrow["0"]["VOUCHERNUMBER"]);
                $docty = $adoc[0];
                $doclc = substr($adoc[1], 0, 3);
                //if ($docty=="CSIB" || $docty=="SINV") {
                //} else {
                $param = getParam($nrow);
                //echo "<pre style='color:#fff;'>33333333["; print_r( $param); echo "]</pre>";
                $retu = array();
                $retu = getSubmit($param); //exit;
                //$retu["returnStateInfo"],$retu["returnCode"],$retu["qrMessage"]
                //echo "<pre style='color:#fff;'>4444444["; print_r( $retu); echo "]</pre>";
                if ($retu["returnCode"] == "00") {
                    $l0 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
										<td>" . $VOUCHERNUMBER . "</td><td>" . $invoiceno . "</td>
										<td>" . $PARTYLEDGERNAME . "</td>
										<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
										<td>" . $VATTINNUMBER . "</td></tr>";
                    $l0 .= "<tr><td colspan='25'>" . $retu["qrMessage"] . "</td></tr>";
                } else if ($retu["returnCode"] == "2124") {
                    $l3 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
										<td>" . $VOUCHERNUMBER . "</td><td>" . $invoiceno . "</td>
										<td>" . $PARTYLEDGERNAME . "</td>
										<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
										<td>" . $VATTINNUMBER . "</td></tr>";
                    //<td>".$Address1."</td>
                    //<td>".$Address2."</td><td>".$Address3."</td>
                    //<td>".$Address4."</td><td>".$LEDGERMOBILE."</td>
                    //<td>".$LEDGERPHONE."</td><td>".$LEDGEREMAIL."</td>
                    //</tr>";
                    $l3 .= "<tr><td colspan='25'>" . $retu["qrMessage"] . "</td></tr>";
                    $d = explode("Collection index:", $retu["returnMessage"]);
                    $l3 .= "<tr><td colspan='25'>";
                    $l3 .= "<span style='margin-left:10px;'>(Iten Name : " . $param["goodsDetails"][$d[1]]["item"];
                    $l3 .= "),</span> <span style='margin-left:10px;'>(Iten Code : " . $param["goodsDetails"][$d[1]]["itemCode"];
                    $l3 .= "),</span> <span style='margin-left:10px;'> (Commodity Code : " . $param["goodsDetails"][$d[1]]["goodsCategoryId"];
                    //echo "<pre style='color:#fff;'>Param["; print_r( $param["goodsDetails"][$d[1]]["item"]); echo "]</pre>";
                    $l3 .= ")</span></td></tr>";
                } else if ($retu["returnCode"] == "1175") {
                    $l3 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
										<td>" . $VOUCHERNUMBER . "</td><td>" . $invoiceno . "</td>
										<td>" . $PARTYLEDGERNAME . "</td>
										<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
										<td>" . $VATTINNUMBER . "</td></tr>";
                    $l3 .= "<tr><td colspan='25'>" . $retu["qrMessage"] . "</td></tr>";
                    $d = explode("Collection index:", $retu["returnMessage"]);
                    $l3 .= "<tr><td colspan='25' style='background: orangered;'>";
                    $l3 .= "<span style='margin-left:10px;'>(Iten Name : " . $param["goodsDetails"][$d[1]]["item"];
                    $l3 .= "),</span> <span style='margin-left:10px;'>(Iten Code : " . $param["goodsDetails"][$d[1]]["itemCode"];
                    $l3 .= "),</span> <span style='margin-left:10px;'> (Commodity Code : " . $param["goodsDetails"][$d[1]]["goodsCategoryId"];
                    //echo "<pre style='color:#fff;'>Param["; print_r( $param["goodsDetails"][$d[1]]["item"]); echo "]</pre>";
                    $l3 .= ")</span></td></tr>";
                    //<td>".$Address1."</td>
                    //<td>".$Address2."</td><td>".$Address3."</td>
                    //<td>".$Address4."</td><td>".$LEDGERMOBILE."</td>
                    //<td>".$LEDGERPHONE."</td><td>".$LEDGEREMAIL."</td>
                    //</tr>";										$l0.= "<tr><td colspan='25'>";
                    //$l3.= "<pre style='color:#fff;'>SQL[".$sql1."]</pre>";
                    //echo "<pre style='color:#fff;'>Param["; print_r( $param); echo "]</pre>";
                    //echo "<pre style='color:#fff;'>Error["; print_r($retu["returnStateInfo"]); echo "]</pre>";
                    //$l3.= "</td></tr>";
                } else {
                    $l3 .= "<tr " . $bgcolor . "><td>" . $DATE . "</td>
										<td>" . $VOUCHERNUMBER . "</td><td>" . $invoiceno . "</td>
										<td>" . $PARTYLEDGERNAME . "</td>
										<td>" . $TVat . "</td><td>" . round($TAmount, 2) . "</td>
										<td>" . $VATTINNUMBER . "</td></tr>";
                    //<td>".$Address1."</td>
                    //<td>".$Address2."</td><td>".$Address3."</td>
                    //<td>".$Address4."</td><td>".$LEDGERMOBILE."</td>
                    //<td>".$LEDGERPHONE."</td><td>".$LEDGEREMAIL."</td>
                    //</tr>";										$l0.= "<tr><td colspan='25'>";
                    //$l3.= "<pre style='color:#fff;'>SQL[".$sql1."]</pre>";
                    $l3 .= "<tr><td colspan='25'>" . $retu["qrMessage"] . "</td></tr>";
                    $d = explode("Collection index:", $retu["returnMessage"]);
                    $l3 .= "<tr><td colspan='25' style='background: orangered;'>";
                    $l3 .= "<span style='margin-left:10px;'>(Iten Name : " . $param["goodsDetails"][$d[1]]["item"];
                    $l3 .= "),</span> <span style='margin-left:10px;'>(Iten Code : " . $param["goodsDetails"][$d[1]]["itemCode"];
                    $l3 .= "),</span> <span style='margin-left:10px;'> (Commodity Code : " . $param["goodsDetails"][$d[1]]["goodsCategoryId"];
                    //echo "<pre style='color:#fff;'>Param["; print_r( $param["goodsDetails"][$d[1]]["item"]); echo "]</pre>";
                    $l3 .= ")</span></td></tr>";
                    //echo "<pre style='color:#fff;'>Param["; print_r( $param); echo "]</pre>";
                    //echo "<pre style='color:#fff;'>Error["; print_r($retu["returnStateInfo"]); echo "]</pre>";
                    //$l3.= "</td></tr>";
                    //echo "<tr><td colspan='25'>";
                    //echo "<pre style='color:#fff;'>Param["; print_r( $param); echo "]</pre>";
                    //echo "<pre style='color:#fff;'>Error["; print_r($retu["returnStateInfo"]); echo "]</pre>";
                    //echo "</td></tr>";
                }
                //}
                /* } else {
							echo "<pre style='color:#fff;'>22222222["; print_r( $nrow); echo "]</pre>";
							$qrMessage='<div class="alert alert-danger" role="alert" style="padding-top: 0px;padding-bottom: 0px;text-align:center;">
									<span style=" font-weight: 900; vertical-align: middle; line-height: 32px;">
									Error found Item Code Empty,
									<img src="efris/ura_icon_grey_dark.jpg" alt="URA EFRIS" style="width:25px;margin: 0px 0px 0px 10px;" >
									</span>
								</div>';
							echo "<tr><td colspan='25'>".$qrMessage."</td></tr>"; //}
						} */
                //exit;
            }
        }
        $TAmount = 0;
        $TVat = 0;
        $bgcolor = "";
        $nrow = array();
        $ps = -1;
        //}
        //$v=$VOUCHERNUMBER;
    }

}
$f = '<tbody></table>';
$intab1 = " active";
$intab2 = "";
$intab3 = "";
$intab4 = "";
if ($isSchedule == "True") {
    echo "Schedule Invoice Fetching Completed!!";
} else {
    //if (isset($_POST['taskname'])) { $tab1=""; $tab2=" active";}
    echo '<ul class="nav nav-tabs" role="tablist">';
    echo '	<li class="nav-item"><a class="nav-link ' . $intab1 . '"
					style="background-color: #dc3545;color: white;"
					data-toggle="tab" href="#intab-first" role="tab" aria-controls="home">Error</a></li>';
    //echo '	<li class="nav-item"><a class="nav-link '.$intab2.'"
    //			style="background-color: #b8860b;color: white;"
    //			data-toggle="tab" href="#intab-second" role="tab" aria-controls="home">No Item Code</a></li>';
    echo '	<li class="nav-item"><a class="nav-link ' . $intab3 . '"
					style="background-color: #b8860b;color: white;"
					data-toggle="tab" href="#intab-third" role="tab" aria-controls="home">Missing Commodity Code</a></li>';
    echo '	<li class="nav-item"><a class="nav-link ' . $intab4 . '"
					style="background-color: #006400;color: white;"
					data-toggle="tab" href="#intab-fourth" role="tab" aria-controls="home">Submitted</a></li>
				</ul>';
    echo '	<div class="tab-content" style="padding: 0px;">';
    echo '		<div class="tab-pane ' . $intab1 . '" id="intab-first" role="tabpanel">';
    echo $h . $l3 . $f;
    echo '</div>';
    //echo '		<div class="tab-pane '.$intab2.'" id="intab-second" role="tabpanel">';
    //echo $h.$l2.$f;
    //echo 		'</div>';
    echo '		<div class="tab-pane ' . $intab3 . '" id="intab-third" role="tabpanel">';
    echo $h2 . $l1 . $f;
    echo '</div>';
    echo '		<div class="tab-pane ' . $intab4 . '" id="intab-fourth" role="tabpanel">';
    echo $h . $l0 . $f;
    echo '</div>';
    echo '	</div>';
}

function getParam($lrow)
{
    $param = array();
    $ncnt = -1;
    $TnetAmount = 0;
    $TVat = 0;
    $docty = "";
    $doclc = "";
    $StampDuty = 0;
    $LoyalityAddition = 0;
    $ServiceCharges = 0;
    $RegistrationFee = 0;
    $NPTransferFee = 0;
    $RiderAdvTax = 0;
    $tcnt = 0;
    $taxDetails_netAmount_ex = 0;
    $taxDetails_taxAmount_ex = 0;
    $taxDetails_grossAmount_ex = 0;
    $taxDetails_netAmount_std = 0;
    $taxDetails_taxAmount_std = 0;
    $taxDetails_grossAmount_std = 0;
    foreach ($lrow as $k => $row) {
//echo "<pre>"; 	print_r($lrow); echo "</pre>";
        //echo "<pre>0000----------</pre>";
        if ($k == 0) {
            //echo "<pre>00000----------11</pre>";
            $param["sellerDetails"]["REFERENCE"] = $row["REFERENCE"];
            $param["sellerDetails"]["DATE"] = $row["DATE"];
//sellerDetails----------------------------
            $param["sellerDetails"]["tin"] = "1000041335";
            $param["sellerDetails"]["ninBrn"] = "";
            $param["sellerDetails"]["legalName"] = "Verma Co Ltd";
            $param["sellerDetails"]["businessName"] = "Verma Co Ltd";
            $param["sellerDetails"]["address"] = "Plot -46, Mirembe Business Center, Lugogo Bypass P.O. Box 33733, Kampala-Uganda";
            $param["sellerDetails"]["mobilePhone"] = "";
            $param["sellerDetails"]["linePhone"] = "+256 414 340 7771";
            $param["sellerDetails"]["emailAddress"] = "info@bajajverma.com";
            $param["sellerDetails"]["placeOfBusiness"] = "http://bajajverma.com";
            $param["sellerDetails"]["referenceNo"] = $row["VOUCHERNUMBER"];
            $adoc = explode("/", $row["VOUCHERNUMBER"]);
            $docty = $adoc[0];
            $doclc = substr($adoc[1], 0, 3);
            $param["sellerDetails"]["branchCode"] = getBranchID($docty . "/" . $doclc, "Code");
            $param["sellerDetails"]["branchId"] = getBranchID($docty . "/" . $doclc);

//basicInformation---------------------------
            $param["basicInformation"]["invoiceNo"] = ""; //
            $param["basicInformation"]["antifakeCode"] = ""; //$row["REFERENCE"];
            $param["basicInformation"]["deviceNo"] = "1000029771_02"; //TCSd81fe12167509579 offline
            $yr2 = substr($row["DATE"], 0, 4);
            $mth2 = substr($row["DATE"], 4, 2);
            $dt2 = substr($row["DATE"], 6, 2);
            $param["basicInformation"]["issuedDate"] = $yr2 . "-" . $mth2 . "-" . $dt2 . " 00:00:00";
            if ($row["ENTEREDBY"] == "") {
                $param["basicInformation"]["operator"] = "Admin - sasbuz.com";
            } else {
                $param["basicInformation"]["operator"] = $row["ENTEREDBY"] . " - sasbuz.com";
            }
            $param["basicInformation"]["currency"] = "UGX";
            $param["basicInformation"]["oriInvoiceI"] = "";
            $param["basicInformation"]["invoiceType"] = "1";
            $param["basicInformation"]["invoiceKind"] = "1";
            $param["basicInformation"]["dataSource"] = "103";
            if ($docty == "CSIB" || $docty == "SINV") {
                $param["basicInformation"]["invoiceIndustryCode"] = "108";
                $param["basicInformation"]["isBatch"] = "0";
            } else {
                $param["basicInformation"]["invoiceIndustryCode"] = "";
                $param["basicInformation"]["isBatch"] = "0";
            }
            //$param["basicInformation"]["isBatch"]="0";
//buyerDetails---------------------------
            if (strlen($row["VATTINNUMBER"]) >= 10 && strlen($row["VATTINNUMBER"]) < 20) {
                $param["buyerDetails"]["buyerTin"] = $row["VATTINNUMBER"];
            } else {
                $param["buyerDetails"]["buyerTin"] = "";
            }
            $param["buyerDetails"]["buyerNinBrn"] = "";
            $param["buyerDetails"]["buyerPassportNum"] = "";
            $param["buyerDetails"]["PARTYNAME"] = $row["PARTYNAME"];
            $param["buyerDetails"]["buyerLegalName"] = $row["PARTYLEDGERNAME"];
            $param["buyerDetails"]["buyerBusinessName"] = $row["PARTYLEDGERNAME"];
            $Address = $row["Address1"];
            if ($row["Address2"] != "" && $row["Address2"] != "0") {
                if ($Address != "") {
                    $Address = ",";
                }
                $Address .= $row["Address2"];
            }
            if ($row["Address3"] != "") {
                if ($Address != "") {
                    $Address = ",";
                }
                $Address .= $row["Address2"];
            }
            if ($row["Address4"] != "") {
                if ($Address != "") {
                    $Address = ",";
                }
                $Address .= $row["Address4"];
            }
            $param["buyerDetails"]["buyerAddress"] = $Address;
            $param["buyerDetails"]["buyerEmail"] = $row["LEDGEREMAIL"];
            $param["buyerDetails"]["buyerMobilePhone"] = $row["LEDGERMOBILE"];
            $param["buyerDetails"]["buyerLinePhone"] = $row["LEDGERPHONE"];
            $param["buyerDetails"]["buyerPlaceOfBusi"] = $sPARTYNAME;
            $Trans = $row["Trans"];
//B2b/B2C---------------------------
            if ($Trans == 2 || $Trans == 3 || $Trans == 7 || $Trans == 8 || $Trans == 9) {
                //2 Corporate, 3	Govt. Body, 7	Stockist, 8	Dealer, 9	2S
                $param["buyerDetails"]["buyerType"] = "0";
            } else {
                //4	Association,6	Retailer,11	Boda Rider,12	Personal
                $param["buyerDetails"]["buyerType"] = "1";
            }

            $param["buyerDetails"]["buyerCitizenship"] = "";
            $param["buyerDetails"]["buyerSector"] = "";
            $param["buyerDetails"]["buyerReferenceNo"] = "";
        }
//BikeOtherCharges---------------------------
        if ($docty == "CSIB" || $docty == "SINV") {
            $tcnt = $row["QTY"];
            $StampDuty = $StampDuty + $row["SalesExempted"];
            $LoyalityAddition = $LoyalityAddition + $row["SalesZeroRate"];
            $RegistrationFee = $RegistrationFee + $row["Sales"];
            $NPTransferFee = $NPTransferFee + $row["SalesTyres"];
            $RiderAdvTax = $RiderAdvTax + $row["SalesExport"];
        }

        $maxCnt = 100;
        $DISCOUNT = 0;
        if ($ncnt <= $maxCnt && $row["AMOUNT"] != 0) {
            $ncnt = $ncnt + 1;
            $AMOUNT = 0;
            $VAT = 0;
            $RATEVALUE = 0;
            //echo "===========[".$ncnt."]=========================<br>";
            $AMOUNT = $row["AMOUNT"];
            //echo "1. AMOUNT = ".$AMOUNT."<br>";
            $VAT = $row["VAT"];
            //echo "2. VAT = ".$VAT."<br>";
            $RATEVALUE = $row["RATEVALUE"];
            //echo "3. RATEVALUE = ".$RATEVALUE."<br>";

            if ($row["DISCOUNT"] != 100) {
                $DISCOUNT = $row["DISCOUNT"];
            }
            //echo "4. DISCOUNT = ".$DISCOUNT."<br>";

            $qty = $row["QTY"];
            if ($qty < 0) {
                $qty = $qty * -1;
            }
            //echo "4. QTY = ".$qty."<br>";

            if ($row["RATEVALUE"] == 0) {
                $row["RATEVALUE"] = $AMOUNT;
            }
            //echo "5. row[RATEVALUE]  = ".$AMOUNT."<br>";

            $RATEQty = $row["RATEVALUE"] * $qty;
            //echo "6. RATEQty = ".$RATEQty."<br>";

            if ($row["STOCKITEMNAME"] == "Exempted Sales (Helmet)") {
                $VAT = 0;
            } else {
                $VAT = round(($RATEQty - $DISCOUNT) * .18, 2);
                $RATEVALUE = round(($row["RATEVALUE"] + ($row["RATEVALUE"] * .18)), 2);
            } //
            //echo "7. VAT = ".$VAT."<br>";
            //echo "8. RATEVALUE = ".$RATEVALUE."<br>";


            //$VAT = $row["VAT"];
            //echo  "==RATEVALUE=".number_format($RATEVALUE,2,".","")."====".$AMOUNT/$qty."====".$RATEVALUE*$qty."==".$AMOUNT."<br>";
            //if ($row["STOCKITEMNAME"]=="Exempted Sales (Helmet)") {{$RATEVALUE=round($RATEVALUE);}

            if ($DISCOUNT != 0) {
                if ($row["STOCKITEMNAME"] == "Exempted Sales (Helmet)") {
                    $AMOUNT = round($RATEQty - $DISCOUNT, 2);
                } else {
                    $AMOUNT = round($RATEQty - $DISCOUNT + (($RATEQty - $DISCOUNT) * .18), 2);
                }
                //echo "9. AMOUNT = ".$AMOUNT."<br>";
                $RATEVALUE = round(($AMOUNT / $qty), 2);
                $AMOUNT = round(($RATEVALUE * $qty), 2);
                //echo "10. RATEVALUE = ".$RATEVALUE."<br>";
                //echo "11. AMOUNT = ".$AMOUNT."<br>";
                //echo  "==RATEVALUE=".number_format($RATEVALUE,2,".","")."====".$AMOUNT/$qty."====".$RATEVALUE*$qty."<br>";
            } else {

                $AMOUNT = round((($RATEVALUE * $qty)), 2);
                //echo "12. AMOUNT = ".$AMOUNT."<br>";
            }
            if ($row["STOCKITEMNAME"] == "Exempted Sales (Helmet)") {
                $VAT = 0;
            } else {
                $VAT = round($AMOUNT - ($AMOUNT / 1.18), 2);
            }
            //echo "-------------------------------------<br>";
            //echo  "==RATEVALUE=".number_format($RATEVALUE,2,".","")."====".$AMOUNT/$qty."====".$RATEVALUE*$qty."<br>";
            //if ($row["StockCode"]=="20") { $AMOUNT=round($AMOUNT); }

//goodsDetails---------------------------
            $param["goodsDetails"][$ncnt]["item"] = $row["STOCKITEMNAME"];
            $param["goodsDetails"][$ncnt]["itemCode"] = $row["ura_goodsCode"];
            $param["goodsDetails"][$ncnt]["qty"] = $qty;
            $param["goodsDetails"][$ncnt]["unitOfMeasure"] = $row["ura_measureUnit"];
            if ($RATEVALUE == 0) {
                $param["goodsDetails"][$ncnt]["unitPrice"] = 1;
                $param["goodsDetails"][$ncnt]["total"] = $qty;
                $TnetAmount = $TnetAmount + $qty;
            } else {
                $param["goodsDetails"][$ncnt]["unitPrice"] = number_format($RATEVALUE, 2, ".", "");
                $param["goodsDetails"][$ncnt]["total"] = $AMOUNT;
                $TnetAmount = $TnetAmount + $AMOUNT;
            }

            $TVat = $TVat + $VAT;
            $param["goodsDetails"][$ncnt]["taxRate"] = "";
            if ($VAT == 0) {
                $param["goodsDetails"][$ncnt]["tax"] = "0";
            } else {
                $param["goodsDetails"][$ncnt]["tax"] = $VAT;
            }

//DISCOUNT---------------------------
            $discountFlag = "2"; //if ($DISCOUNT!=0) { $discountFlag="1"; }
            if ($discountFlag == "2") {
                $param["goodsDetails"][$ncnt]["discountTotal"] = ""; //$row["DISCOUNT"];
            } else {
                $param["goodsDetails"][$ncnt]["discountTotal"] = $DISCOUNT * -1;
            }
            $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0; //$row["ADDLCOSTPERC"];

            $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
            $param["goodsDetails"][$ncnt]["discountFlag"] = $discountFlag;
            $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
            $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
            $param["goodsDetails"][$ncnt]["categoryId"] = "";
            $param["goodsDetails"][$ncnt]["categoryName"] = "";
            $param["goodsDetails"][$ncnt]["goodsCategoryId"] = $row["ura_commodityCategoryId"];
            $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
            $param["goodsDetails"][$ncnt]["exciseRate"] = "";
            $param["goodsDetails"][$ncnt]["exciseRule"] = "";
            $param["goodsDetails"][$ncnt]["exciseTax"] = "";
            $param["goodsDetails"][$ncnt]["pack"] = "";
            $param["goodsDetails"][$ncnt]["stick"] = "";
            $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
            $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
            $param["goodsDetails"][$ncnt]["exciseRateName"] = "";

//taxDetails---------------------------
            if ($VAT == 0) {
                //echo $row["VOUCHERNUMBER"]."=====Exempted ===".$AMOUNT."===".$row["STOCKITEMNAME"]."==<br>";
                $param["goodsDetails"][$ncnt]["taxRate"] = "-";
                $taxDetails_netAmount_ex = $taxDetails_netAmount_ex + $AMOUNT;
                $taxDetails_taxAmount_ex = 0;
                $taxDetails_grossAmount_ex = $taxDetails_grossAmount_ex + $AMOUNT;
            } else {
                $param["goodsDetails"][$ncnt]["taxRate"] = "0.18";
                $taxDetails_netAmount_std = $taxDetails_netAmount_std + ($AMOUNT - $VAT);
                $taxDetails_taxAmount_std = $taxDetails_taxAmount_std + $VAT;
                $taxDetails_grossAmount_std = $taxDetails_grossAmount_std + $AMOUNT;
            }
        }
    }
    $ExemptCharges = 0;
    if ($docty == "CSIB" || $docty == "SINV") { //echo "<pre>2222----------</pre>";
        $ncnt2 = $ncnt;

        //96010101 - CHARGES.
        $ExemptCharges = $RegistrationFee + $NPTransferFee + $RiderAdvTax + $StampDuty;//-$LoyalityAddition
        if ($ExemptCharges > 0) {//$ncnt2=$ncnt2+1;
            $taxDetails_netAmount_ex = $taxDetails_netAmount_ex + $ExemptCharges;
            $taxDetails_taxAmount_ex = 0;
            $taxDetails_grossAmount_ex = $taxDetails_grossAmount_ex + $ExemptCharges;

            //$param["taxDetails"][$ncnt2]["taxCategory"]="C: Exempt";
            //$param["taxDetails"][$ncnt2]["netAmount"]=$ExemptCharges;
            //$param["taxDetails"][$ncnt2]["taxRate"]="0.00";
            //$param["taxDetails"][$ncnt2]["taxAmount"]="0";
            //$param["taxDetails"][$ncnt2]["grossAmount"]=$ExemptCharges;
            //$param["taxDetails"][$ncnt2]["taxRateName"]="-";
        }
        /* if ($LoyalityAddition>0) {//$ncnt2=$ncnt2+1;
		//LoyalityAddition - Loyality Addition
			//$param["taxDetails"][$ncnt2]["taxCategory"]="E: LoyalityAddition";
			//$param["taxDetails"][$ncnt2]["netAmount"]=$LoyalityAddition;
			//$param["taxDetails"][$ncnt2]["taxRate"]="0.00";
			//$param["taxDetails"][$ncnt2]["taxAmount"]="0";
			//$param["taxDetails"][$ncnt2]["grossAmount"]=$LoyalityAddition;
			//$param["taxDetails"][$ncnt2]["taxRateName"]="Fee";
			$ncnt=$ncnt+1;
			$param["goodsDetails"][$ncnt]["item"]="Loyality Addition";
			$param["goodsDetails"][$ncnt]["itemCode"]="LoyalityAddition";
			$param["goodsDetails"][$ncnt]["qty"]=$tcnt;
			$param["goodsDetails"][$ncnt]["unitOfMeasure"]="PP";
			$param["goodsDetails"][$ncnt]["unitPrice"]=$LoyalityAddition*-1;
			$param["goodsDetails"][$ncnt]["total"]=$LoyalityAddition*-1;
			$param["goodsDetails"][$ncnt]["taxRate"]="";
			$param["goodsDetails"][$ncnt]["tax"]="0";
			$param["goodsDetails"][$ncnt]["discountTotal"]=""; //$row["DISCOUNT"];
			$param["goodsDetails"][$ncnt]["discountTaxRate"]=0; //$row["ADDLCOSTPERC"];
			$param["goodsDetails"][$ncnt]["orderNumber"]=$ncnt;
			$param["goodsDetails"][$ncnt]["discountFlag"]="2";
			$param["goodsDetails"][$ncnt]["deemedFlag"]="2";
			$param["goodsDetails"][$ncnt]["exciseFlag"]="2";
			$param["goodsDetails"][$ncnt]["categoryId"]="";
			$param["goodsDetails"][$ncnt]["categoryName"]="";
			$param["goodsDetails"][$ncnt]["goodsCategoryId"]="96010101";
			$param["goodsDetails"][$ncnt]["goodsCategoryName"]="";
			$param["goodsDetails"][$ncnt]["exciseRate"]="";
			$param["goodsDetails"][$ncnt]["exciseRule"]="";
			$param["goodsDetails"][$ncnt]["exciseTax"]="";
			$param["goodsDetails"][$ncnt]["pack"]="";
			$param["goodsDetails"][$ncnt]["stick"]="";
			$param["goodsDetails"][$ncnt]["exciseUnit"]="";
			$param["goodsDetails"][$ncnt]["exciseCurrency"]="";
			$param["goodsDetails"][$ncnt]["exciseRateName"]="";
		} */
        //96010101
        //RegistrationFees -- Registration Fees
        if ($RegistrationFee > 0) {//$ncnt2=$ncnt2+1;
            //$param["taxDetails"][$ncnt2]["taxCategory"]="E: RegistrationFee";
            //$param["taxDetails"][$ncnt2]["netAmount"]=$RegistrationFee;
            //$param["taxDetails"][$ncnt2]["taxRate"]="0.00";
            //$param["taxDetails"][$ncnt2]["taxAmount"]="0";
            //$param["taxDetails"][$ncnt2]["grossAmount"]=$RegistrationFee;
            //$param["taxDetails"][$ncnt2]["taxRateName"]="Fee";
            $ncnt = $ncnt + 1;
            $param["goodsDetails"][$ncnt]["item"] = "Registration Fees";
            $param["goodsDetails"][$ncnt]["itemCode"] = "RegistrationFees";
            $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
            $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
            $param["goodsDetails"][$ncnt]["unitPrice"] = round($RegistrationFee / $tcnt, 2);
            $param["goodsDetails"][$ncnt]["total"] = $RegistrationFee;
            $param["goodsDetails"][$ncnt]["taxRate"] = "";
            $param["goodsDetails"][$ncnt]["tax"] = "0";
            $param["goodsDetails"][$ncnt]["discountTotal"] = ""; //$row["DISCOUNT"];
            $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0; //$row["ADDLCOSTPERC"];
            $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
            $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
            $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
            $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
            $param["goodsDetails"][$ncnt]["categoryId"] = "";
            $param["goodsDetails"][$ncnt]["categoryName"] = "";
            $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010101";
            $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
            $param["goodsDetails"][$ncnt]["exciseRate"] = "";
            $param["goodsDetails"][$ncnt]["exciseRule"] = "";
            $param["goodsDetails"][$ncnt]["exciseTax"] = "";
            $param["goodsDetails"][$ncnt]["pack"] = "";
            $param["goodsDetails"][$ncnt]["stick"] = "";
            $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
            $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
            $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
        }
        //NPTransferFees -- NP Transfer Fees
        if ($NPTransferFee > 0) {//$ncnt2=$ncnt2+1;
            //$param["taxDetails"][$ncnt2]["taxCategory"]="E: NPTransferFee";
            //$param["taxDetails"][$ncnt2]["netAmount"]=$NPTransferFee;
            //$param["taxDetails"][$ncnt2]["taxRate"]="0.00";
            //$param["taxDetails"][$ncnt2]["taxAmount"]="0";
            //$param["taxDetails"][$ncnt2]["grossAmount"]=$NPTransferFee;
            //$param["taxDetails"][$ncnt2]["taxRateName"]="Fee";
            $ncnt = $ncnt + 1;
            $param["goodsDetails"][$ncnt]["item"] = "NP Transfer Fees";
            $param["goodsDetails"][$ncnt]["itemCode"] = "NP TFR Fees";
            $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
            $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
            $param["goodsDetails"][$ncnt]["unitPrice"] = round($NPTransferFee / $tcnt, 2);
            $param["goodsDetails"][$ncnt]["total"] = $NPTransferFee;
            $param["goodsDetails"][$ncnt]["taxRate"] = "";
            $param["goodsDetails"][$ncnt]["tax"] = "0";
            $param["goodsDetails"][$ncnt]["discountTotal"] = ""; //$row["DISCOUNT"];
            $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0; //$row["ADDLCOSTPERC"];
            $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
            $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
            $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
            $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
            $param["goodsDetails"][$ncnt]["categoryId"] = "";
            $param["goodsDetails"][$ncnt]["categoryName"] = "";
            $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010101";
            $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
            $param["goodsDetails"][$ncnt]["exciseRate"] = "";
            $param["goodsDetails"][$ncnt]["exciseRule"] = "";
            $param["goodsDetails"][$ncnt]["exciseTax"] = "";
            $param["goodsDetails"][$ncnt]["pack"] = "";
            $param["goodsDetails"][$ncnt]["stick"] = "";
            $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
            $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
            $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
        }
        //RiderAdvTax -- Rider Advance Tax
        if ($RiderAdvTax > 0) {
            $ncnt2 = $ncnt2 + 1;
            //$param["taxDetails"][$ncnt2]["taxCategory"]="E: RiderAdvTax";
            //$param["taxDetails"][$ncnt2]["netAmount"]=$RiderAdvTax;
            //$param["taxDetails"][$ncnt2]["taxRate"]="0.00";
            //$param["taxDetails"][$ncnt2]["taxAmount"]="0";
            //$param["taxDetails"][$ncnt2]["grossAmount"]=$RiderAdvTax;
            //$param["taxDetails"][$ncnt2]["taxRateName"]="Tax";
            $ncnt = $ncnt + 1;
            $param["goodsDetails"][$ncnt]["item"] = "Rider Advance Tax";
            $param["goodsDetails"][$ncnt]["itemCode"] = "RiderAdvTax";
            $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
            $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
            $param["goodsDetails"][$ncnt]["unitPrice"] = round($RiderAdvTax / $tcnt, 2);
            $param["goodsDetails"][$ncnt]["total"] = $RiderAdvTax;
            $param["goodsDetails"][$ncnt]["taxRate"] = "";
            $param["goodsDetails"][$ncnt]["tax"] = "0";
            $param["goodsDetails"][$ncnt]["discountTotal"] = ""; //$row["DISCOUNT"];
            $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0; //$row["ADDLCOSTPERC"];
            $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
            $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
            $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
            $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
            $param["goodsDetails"][$ncnt]["categoryId"] = "";
            $param["goodsDetails"][$ncnt]["categoryName"] = "";
            $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010101";
            $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
            $param["goodsDetails"][$ncnt]["exciseRate"] = "";
            $param["goodsDetails"][$ncnt]["exciseRule"] = "";
            $param["goodsDetails"][$ncnt]["exciseTax"] = "";
            $param["goodsDetails"][$ncnt]["pack"] = "";
            $param["goodsDetails"][$ncnt]["stick"] = "";
            $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
            $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
            $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
        }
        //"Stamp Duty""StampDuty"
        if ($StampDuty > 0) {//$ncnt2=$ncnt2+1;
            //$param["taxDetails"][$ncnt2]["taxCategory"]="E: RiderAdvTax";
            //$param["taxDetails"][$ncnt2]["netAmount"]=$RiderAdvTax;
            //$param["taxDetails"][$ncnt2]["taxRate"]="0.00";
            //$param["taxDetails"][$ncnt2]["taxAmount"]="0";
            //$param["taxDetails"][$ncnt2]["grossAmount"]=$RiderAdvTax;
            //$param["taxDetails"][$ncnt2]["taxRateName"]="Tax";
            $ncnt = $ncnt + 1;
            $param["goodsDetails"][$ncnt]["item"] = "Stamp Duty";
            $param["goodsDetails"][$ncnt]["itemCode"] = "StampDuty";
            $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
            $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
            $param["goodsDetails"][$ncnt]["unitPrice"] = round($StampDuty / $tcnt, 2);
            $param["goodsDetails"][$ncnt]["total"] = $StampDuty;
            $param["goodsDetails"][$ncnt]["taxRate"] = "";
            $param["goodsDetails"][$ncnt]["tax"] = "0";
            $param["goodsDetails"][$ncnt]["discountTotal"] = ""; //$row["DISCOUNT"];
            $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0; //$row["ADDLCOSTPERC"];
            $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
            $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
            $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
            $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
            $param["goodsDetails"][$ncnt]["categoryId"] = "";
            $param["goodsDetails"][$ncnt]["categoryName"] = "";
            $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010101";
            $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
            $param["goodsDetails"][$ncnt]["exciseRate"] = "";
            $param["goodsDetails"][$ncnt]["exciseRule"] = "";
            $param["goodsDetails"][$ncnt]["exciseTax"] = "";
            $param["goodsDetails"][$ncnt]["pack"] = "";
            $param["goodsDetails"][$ncnt]["stick"] = "";
            $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
            $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
            $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
        }
        // if ($StampDuty>0) {$ncnt2=$ncnt2+1;
        // $param["taxDetails"][$ncnt2]["taxCategory"]="E: StampDuty";
        // $param["taxDetails"][$ncnt2]["netAmount"]="0";
        // $param["taxDetails"][$ncnt2]["taxRate"]="0.00";
        // $param["taxDetails"][$ncnt2]["taxAmount"]=$StampDuty;
        // $param["taxDetails"][$ncnt2]["grossAmount"]=$StampDuty;
        // $param["taxDetails"][$ncnt2]["taxRateName"]="Tax";
        // }
    }
    //$Charges=$StampDuty +$LoyalityAddition+$RegistrationFee+$NPTransferFee+$RiderAdvTax;
    //$ncnt=18;
//---------------------------
    $ncnt2 = -1;
    if ($taxDetails_grossAmount_ex != 0) {
        $ncnt2 = $ncnt2 + 1;
        $param["taxDetails"][$ncnt2]["taxCategory"] = "B: Exempted";
        $param["taxDetails"][$ncnt2]["taxCategoryCode"] = "03";
        $param["taxDetails"][$ncnt2]["netAmount"] = $taxDetails_netAmount_ex;
        $param["taxDetails"][$ncnt2]["taxRate"] = "-";
        $param["taxDetails"][$ncnt2]["taxAmount"] = "0";
        $param["taxDetails"][$ncnt2]["grossAmount"] = $taxDetails_grossAmount_ex;
        $param["taxDetails"][$ncnt2]["taxRateName"] = "-";
        $param["taxDetails"][$ncnt2]["exciseUnit"] = "";
        $param["taxDetails"][$ncnt2]["exciseCurrency"] = "";
    }
    if ($taxDetails_grossAmount_std != 0) {
        $ncnt2 = $ncnt2 + 1;
        $param["taxDetails"][$ncnt2]["taxCategory"] = "A: Standard";
        $param["taxDetails"][$ncnt2]["taxCategoryCode"] = "01";
        $param["taxDetails"][$ncnt2]["netAmount"] = $taxDetails_netAmount_std;
        $param["taxDetails"][$ncnt2]["taxRate"] = "0.18";
        $param["taxDetails"][$ncnt2]["taxAmount"] = $taxDetails_taxAmount_std;
        $param["taxDetails"][$ncnt2]["grossAmount"] = $taxDetails_grossAmount_std;
        $param["taxDetails"][$ncnt2]["taxRateName"] = "18%";
        $param["taxDetails"][$ncnt2]["exciseUnit"] = "";
        $param["taxDetails"][$ncnt2]["exciseCurrency"] = "";
    }
    if ($TnetAmount != 0) {
        $param["summary"]["netAmount"] = $TnetAmount - $TVat + $ExemptCharges;
        $param["summary"]["taxAmount"] = $TVat;
        $param["summary"]["grossAmount"] = $TnetAmount + $ExemptCharges;
        $param["summary"]["itemCount"] = $ncnt + 1;
        $param["summary"]["modeCode"] = "1";
        $param["summary"]["remarks"] = $row["Narration"];
        $param["summary"]["qrCode"] = "";
//---------------------------
        $param["payWay"]["paymentMode"] = "101";
        $param["payWay"]["paymentAmount"] = $TnetAmount + $ExemptCharges;
        $param["payWay"]["orderNumber"] = "a";

        $param["extend"]["reason"] = "";
        $param["extend"]["reasonCode"] = "";
    }
//---------------------------
    return $param;
}

function getSubmit($param)
{
    global $conni;
    $VOUCHERNUMBER = $param["sellerDetails"]["referenceNo"];
    //if ($VOUCHERNUMBER=="CS/LUG21/7586") {
    //echo "<pre style='color:#fff;'>22222222["; print_r( $param); echo "]</pre>"; exit;
    //}
    $REFERENCE = $param["sellerDetails"]["REFERENCE"];
    $sPARTYNAME = $param["buyerDetails"]["PARTYNAME"];
    $sPARTYLEDGERNAME = $param["buyerDetails"]["buyerLegalName"];
    $sDate = $param["sellerDetails"]["DATE"];
    //echo $sDate.">>>===<br>";
    $qrMessage = "";
    $T109c = new T109($param);
    $T109a = $T109c->Response();
    $retu = array();
    $retu["returnStateInfo"] = "";
    $retu["returnCode"] = "";
    $retu["qrMessage"] = $qrMessage;
    //if ($UserName=="admin") {
    //echo "<pre style='color:#fff;'>22222222["; print_r( $T109a->data); echo "]</pre>";
    if (isset($T109a->data)) {
        //
        $select_chk = 'select ef.qrcode,ef.antifakeCode,ef.invoiceNo from efris_invoice as ef where ef.VOUCHERNUMBER="' . $VOUCHERNUMBER . '"';
        $result_chk = mysqli_query($conni, $select_chk);

        $count_chk = 0;
        $count_chk = mysqli_num_rows($result_chk);
        //echo "<br>-----------[".intval($count_chk)."]-------------------<br>.";
        if ($count_chk == 0) {
            $T109v = array();
            $T109v["VOUCHERNUMBER"] = $VOUCHERNUMBER;
            $T109v["VOUCHERTYPENAME"] = $VOUCHERTYPENAME;
            $T109v["REFERENCE"] = $REFERENCE;
            $T109v["PARTYNAME"] = $sPARTYNAME;
            $T109v["PARTYLEDGERNAME"] = $sPARTYLEDGERNAME;
            $T109v["invoiceNo"] = $T109a->data->basicInformation->invoiceNo;
            $T109v["antifakeCode"] = $T109a->data->basicInformation->antifakeCode;
            $T109v["deviceNo"] = $T109a->data->basicInformation->deviceNo;
            $T109v["issuedDate"] = $sDate;
            $T109v["invoiceType"] = $T109a->data->basicInformation->invoiceType;
            $T109v["invoiceKind"] = $T109a->data->basicInformation->invoiceKind;
            $T109v["buyerTin"] = $T109a->data->buyerDetails->buyerTin;
            $T109v["buyerLegalName"] = $T109a->data->buyerDetails->buyerLegalName;
            $T109v["buyerBusinessName"] = $T109a->data->buyerDetails->buyerBusinessName;
            $T109v["netAmount"] = $T109a->data->summary->netAmount;
            $T109v["taxAmount"] = $T109a->data->summary->taxAmount;
            $T109v["grossAmount"] = $T109a->data->summary->grossAmount;
            $T109v["itemCount"] = $T109a->data->summary->itemCount;
            $T109v["qrCode"] = $T109a->data->summary->qrCode;


            $sql4 = 'INSERT INTO efris_invoice (DATE,VOUCHERNUMBER,VOUCHERTYPENAME,REFERENCE,PARTYNAME,PARTYLEDGERNAME,invoiceNo,antifakeCode,
					deviceNo,issuedDate,invoiceType,invoiceKind,buyerTin,buyerLegalName,buyerBusinessName,netAmount,taxAmount,
					grossAmount,itemCount,qrCode) VALUES ("' . $sDate . '","' . $T109v["VOUCHERNUMBER"] . '","' . $T109v["VOUCHERTYPENAME"] . '","' . $T109v["REFERENCE"] . '",
					"' . $T109v["PARTYNAME"] . '","' . $T109v["PARTYLEDGERNAME"] . '","' . $T109v["invoiceNo"] . '","' . $T109v["antifakeCode"] . '",
					"' . $T109v["deviceNo"] . '","' . $T109v["issuedDate"] . '","' . $T109v["invoiceType"] . '","' . $T109v["invoiceKind"] . '","' . $T109v["buyerTin"] . '",
					"' . $T109v["buyerLegalName"] . '","' . $T109v["buyerBusinessName"] . '","' . $T109v["netAmount"] . '","' . $T109v["taxAmount"] . '",
					"' . $T109v["grossAmount"] . '","' . $T109v["itemCount"] . '","' . $T109v["qrCode"] . '")';

            $qrcode = $T109v["qrCode"];
            $invoiceNo = $T109v["invoiceNo"];
            mysqli_query($conni, $sql4); //echo "<pre style='color:#fff;'>".$sql4."</pre>";

            $sql5 = "update transaction set efris_error=''  where  VOUCHERNUMBER ='" . $VOUCHERNUMBER . "'";
            mysqli_query($conni, $sql5);

            if ($buyerTin != "") {
                $sql1 = 'SELECT LEDGEREMAIL,ura_contactEmail
					FROM masterparty where id="' . $PARTYNAME . '"   ';
                $result1 = mysqli_query($conni, $sql1);
                $count = 0;
                $count = mysqli_num_rows($result1);
                if ($count > 0) {
                    $row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
                    if ($row1["ura_tinstatus"] == "") {
                        $sT119 = "{\n  \"tin\": \"" . $buyerTin . "\", \n \"ninBrn\": \"\" \n  }\n	";
                        $json_119 = urlencode($sT119);
                        $out = json_decode(str_replace("\\", "", shell_exec('"C:\Program Files\Java\jdk-14.0.2\bin\java" -jar "C:\xampp\htdocs\sas\efris\SASEfris.jar" "T119" "' . $json_119 . '"  2>&1')));

                        if ($out->returnStateInfo->returnCode == "00") {
                            $ura_ninBrn = $out->data->taxpayer->ninBrn;
                            $ura_legalName = $out->data->taxpayer->legalName;
                            $ura_businessName = $out->data->taxpayer->businessName;
                            $ura_contactNumber = $out->data->taxpayer->contactNumber;
                            $ura_contactEmail = $out->data->taxpayer->contactEmail;
                            $ura_address = $out->data->taxpayer->address;
                            $ura_taxpayerStatus = $out->data->taxpayer->taxpayerStatus;
                            $ura_tinstatus = "Valid";
                            $sql2 = "update `masterparty` set ura_ninBrn='" . $ura_ninBrn . "',
										ura_legalName='" . $ura_legalName . "',
										ura_businessName='" . $ura_businessName . "',
										ura_contactNumber='" . $ura_contactNumber . "',
										ura_contactEmail='" . $ura_contactEmail . "',
										ura_address='" . $ura_address . "' ,
										ura_taxpayerStatus='" . $ura_taxpayerStatus . "' ,
										ura_tinstatus='" . $ura_tinstatus . "' WHERE id = " . $row1["ID"] . " ";
                            mysqli_query($conni, $sql2);
                        } else {
                            $ura_tinstatus = "InValid";
                            $sql2 = "update `masterparty` set ura_tinstatus='" . $ura_tinstatus . "' WHERE id = " . $row1["ID"] . " ";
                            mysqli_query($conni, $sql2);
                        }

                    }
                }
            }
            PrintingMaiiling(1, $T109v["VOUCHERNUMBER"], $T109v["PARTYLEDGERNAME"], $sPARTYNAME);
            $qrMessage = '<div class="alert alert-success" role="alert" style="margin: 0px;padding-top: 0px;padding-bottom: 0px;text-align:left;">
							<span style=" font-weight: 900; vertical-align: middle; line-height: 32px;">
							Invoice Sucessfully Submited to URA
							<img src="efris/ura_icon.jpg" alt="URA EFRIS" style="width:25px;margin: 0px 0px 0px 10px;" >
							</span>
						</div>';

            //exit;
        } else {
            $row_chk = mysqli_fetch_array($result_chk, MYSQLI_ASSOC);
            $qrcode = $row_chk["qrcode"];
            $invoiceNo = $row_chk["invoiceNo"];
            $antifakeCode = $row_chk["antifakeCode"];
            //$qrfile = print_qrcode($antifakeCode,$qrcode,"temp/",'L',3);
            $qrMessage = '<div class="alert alert-success" role="alert" style="margin: 0px;padding-top: 0px;padding-bottom: 0px;text-align:left;">
							<span style=" font-weight: 900; vertical-align: middle; line-height: 32px;">
							This Invoice is already submitted to URA
							<img src="efris/ura_icon.jpg" alt="URA EFRIS" style="width:25px;margin: 0px 0px 0px 10px;" >
							</span>
						</div>';
            $sql5 = "update transaction set efris_error=''  where  VOUCHERNUMBER ='" . $VOUCHERNUMBER . "'";
            mysqli_query($conni, $sql5);

        }
        $retu["returnStateInfo"] = $T109a->returnStateInfo;
        $retu["returnCode"] = $T109a->returnStateInfo->returnCode;
        $retu["qrMessage"] = $qrMessage;
        //} else if ($T109a["returnStateInfo"]["returnCode"]=="2253") {
        //	echo "<pre style='color:#fff;'>2253ar["; print_r( $T109a); echo "]</pre>";
    } else if ($T109a->returnStateInfo->returnCode == "2253") {
        //echo "<pre style='color:#fff;'>2253obj["; print_r( $T109a); echo "]</pre>";
        $returnMessage = str_replace("Invoice(s)/receipt(s) (", "", $T109a->returnStateInfo->returnMessage);
        $r = explode(")", $returnMessage);
        $invoiceNo = $r["0"];
        //echo "------->".$invoiceNo;
        $sT108 = "{ \"invoiceNo\": \"" . $invoiceNo . "\" }";
        $json_108 = urlencode($sT108);
        $out = json_decode(str_replace('\\"', "", shell_exec('"C:\Program Files\Java\jdk-14.0.2\bin\java" -jar "C:\xampp\htdocs\sas\efris\SASEfris.jar" "T108" "' . $json_108 . '"  2>&1')));
        //echo "<pre style='color:#fff;'>3333333["; print_r( $out); echo "]</pre>";
        if (isset($out->data)) {
            $select_chk = 'select ef.qrcode,ef.antifakeCode,ef.invoiceNo from efris_invoice as ef where ef.VOUCHERNUMBER="' . $VOUCHERNUMBER . '"';
            $result_chk = mysqli_query($conni, $select_chk);

            $count_chk = 0;
            $count_chk = mysqli_num_rows($result_chk);
            //echo "<br>-----------[".intval($count_chk)."]-------------------<br>.";
            if ($count_chk == 0) {
                $T109v = array();
                $T109v["VOUCHERNUMBER"] = $VOUCHERNUMBER;
                $T109v["VOUCHERTYPENAME"] = $VOUCHERTYPENAME;
                $T109v["REFERENCE"] = $REFERENCE;
                $T109v["PARTYNAME"] = $sPARTYNAME;
                $T109v["PARTYLEDGERNAME"] = $sPARTYLEDGERNAME;
                $T109v["issuedDate"] = $sDate;
                $T109v["invoiceNo"] = $out->data->basicInformation->invoiceNo;
                $T109v["antifakeCode"] = $out->data->basicInformation->antifakeCode;
                $T109v["deviceNo"] = $out->data->basicInformation->deviceNo;
                $T109v["invoiceType"] = $out->data->basicInformation->invoiceType;
                $T109v["invoiceKind"] = $out->data->basicInformation->invoiceKind;
                $T109v["buyerTin"] = $out->data->buyerDetails->buyerTin;
                $T109v["buyerLegalName"] = $out->data->buyerDetails->buyerLegalName;
                $T109v["buyerBusinessName"] = $out->data->buyerDetails->buyerBusinessName;
                $T109v["netAmount"] = $out->data->summary->netAmount;
                $T109v["taxAmount"] = $out->data->summary->taxAmount;
                $T109v["grossAmount"] = $out->data->summary->grossAmount;
                $T109v["itemCount"] = $out->data->summary->itemCount;
                $T109v["qrCode"] = $out->data->summary->qrCode;
                $sql4 = 'INSERT INTO efris_invoice (DATE,VOUCHERNUMBER,VOUCHERTYPENAME,REFERENCE,PARTYNAME,PARTYLEDGERNAME,invoiceNo,antifakeCode,
						deviceNo,issuedDate,invoiceType,invoiceKind,buyerTin,buyerLegalName,buyerBusinessName,netAmount,taxAmount,
						grossAmount,itemCount,qrCode) VALUES ("' . $sDate . '","' . $T109v["VOUCHERNUMBER"] . '","' . $T109v["VOUCHERTYPENAME"] . '","' . $T109v["REFERENCE"] . '",
						"' . $T109v["PARTYNAME"] . '","' . $T109v["PARTYLEDGERNAME"] . '","' . $T109v["invoiceNo"] . '","' . $T109v["antifakeCode"] . '",
						"' . $T109v["deviceNo"] . '","' . $T109v["issuedDate"] . '","' . $T109v["invoiceType"] . '","' . $T109v["invoiceKind"] . '","' . $T109v["buyerTin"] . '",
						"' . $T109v["buyerLegalName"] . '","' . $T109v["buyerBusinessName"] . '","' . $T109v["netAmount"] . '","' . $T109v["taxAmount"] . '",
						"' . $T109v["grossAmount"] . '","' . $T109v["itemCount"] . '","' . $T109v["qrCode"] . '")';

                $qrcode = $T109v["qrCode"];
                $invoiceNo = $T109v["invoiceNo"];
                //echo "<pre style='color:#fff;'>".$sql4."</pre>";
                mysqli_query($conni, $sql4); //echo "<pre style='color:#fff;'>".$sql4."</pre>";
                $this->PrintingMaiiling(1, $T109v["VOUCHERNUMBER"], $T109v["PARTYLEDGERNAME"]);
                $qrMessage = '<div class="alert alert-success" role="alert" style="margin: 0px;padding-top: 0px;padding-bottom: 0px;text-align:left;">
								<span style=" font-weight: 900; vertical-align: middle; line-height: 32px;">
								Invoice Sucessfully Submited to URA
								<img src="efris/ura_icon.jpg" alt="URA EFRIS" style="width:25px;margin: 0px 0px 0px 10px;" >
								</span>
							</div>';

                //exit;
            } else {
                $row_chk = mysqli_fetch_array($result_chk, MYSQLI_ASSOC);
                $qrcode = $row_chk["qrcode"];
                $invoiceNo = $row_chk["invoiceNo"];
                $antifakeCode = $row_chk["antifakeCode"];
                //$qrfile = print_qrcode($antifakeCode,$qrcode,"temp/",'L',3);
                $qrMessage = '<div class="alert alert-success" role="alert" style="margin: 0px;padding-top: 0px;padding-bottom: 0px;text-align:left;">
								<span style=" font-weight: 900; vertical-align: middle; line-height: 32px;">
								This Invoice is already submitted to URA
								<img src="efris/ura_icon.jpg" alt="URA EFRIS" style="width:25px;margin: 0px 0px 0px 10px;" >
								</span>
							</div>';
            }
            $retu["returnStateInfo"] = $T109a->returnStateInfo;
            $retu["returnCode"] = $T109a->returnStateInfo->returnCode;
            $retu["qrMessage"] = $qrMessage;
        }
    } else {
        //echo "<pre style='color:#fff;'>Param["; print_r( $param); echo "]</pre>";
        //echo "<pre style='color:#fff;'>Error["; print_r( $T109a); echo "]</pre>";
        $qrMessage = '<div class="alert alert-danger" role="alert" style="margin: 0px;padding-top: 0px;padding-bottom: 0px;text-align:left;">
							<span style=" font-weight: 900; vertical-align: middle; line-height: 32px;">
							Error found (' . $T109a->returnStateInfo->returnCode . ') - ' . $T109a->returnStateInfo->returnMessage . '
							<img src="efris/ura_icon_grey_dark.jpg" alt="URA EFRIS" style="width:25px;margin: 0px 0px 0px 10px;" >
							</span>
						</div>';
        $sql4 = "update transaction set efris_error='" . str_replace("'", "\\'", $qrMessage) . "'  where  VOUCHERNUMBER ='" . $VOUCHERNUMBER . "'";
        //echo ">>>>".$sql4."<br>";
        mysqli_query($conni, $sql4);
        $retu["returnStateInfo"] = $T109a->returnStateInfo;
        $retu["returnCode"] = $T109a->returnStateInfo->returnCode;
        $retu["returnMessage"] = $T109a->returnStateInfo->returnMessage;

        $retu["qrMessage"] = $qrMessage;
    }
//	echo "<pre style='color:#fff;'>22222222["; print_r( $param); echo "]</pre>"; exit;

    return $retu;
}

function getBranchID($docty, $type = "")
{ //$branchId="E002345";//{"branchName":"VERMA CO. LIMITED"},
    if ($docty == "SINV/LUG" || $docty == "CSIB/LUG") { //11-Lugogo{"branchName":"Lugogo"},
        if ($type == "Code") {
            $branchId = "11";
        } else {
            $branchId = "404724745083932254";
        }
    } else if ($docty == "CS/LUG") { //02-Lugogo-SHW {"branchName":"Lugogo-SHW"},
        if ($type == "Code") {
            $branchId = "02";
        } else {
            $branchId = "404724737024336704";
        }
    } else if ($docty == "JBI/WSP") { //10-Lugogo-WSP{"branchName":"Lugogo-WSP"},
        if ($type == "Code") {
            $branchId = "10";
        } else {
            $branchId = "404724744327400952";
        }
    } else if ($docty == "SIS/LUG" || $docty == "SCI/LUG") { //{"branchName":"Lugogo"},
        if ($type == "Code") {
            $branchId = "11";
        } else {
            $branchId = "404724745083932254";
        }
    } else if ($docty == "SINV/BMB" || $docty == "CSIB/BMB") { //01-Bombo Road{"branchName":"Bombo Road"},
        if ($type == "Code") {
            $branchId = "01";
        } else {
            $branchId = "404724736259113052";
        }
    } else if ($docty == "CS/BMB") { //01-Bombo Road
        if ($type == "Code") {
            $branchId = "01";
        } else {
            $branchId = "404724736259113052";
        }
    } else if ($docty == "SINV/NDB" || $docty == "CSIB/NDB") { //06-Ndeeba Branch {"branchName":"Ndeeba Branch"},
        if ($type == "Code") {
            $branchId = "06";
        } else {
            $branchId = "404724740493471603";
        }
    } else if ($docty == "CS/NDB") { //03-Ndeeba STR-2 {"branchName":"Ndeeba STR-2"},
        if ($type == "Code") {
            $branchId = "03";
        } else {
            $branchId = "404724737796228606";
        }
    } else if ($docty == "SINV/NDW" || $docty == "CSIB/NDW") { //12-Ndeeba WH{"branchName":"Ndeeba WH"},
        if ($type == "Code") {
            $branchId = "12";
        } else {
            $branchId = "404724745833492756";
        }
    } else if ($docty == "SINV/MSK" || $docty == "CSIB/MSK") { //05-Masaka {"branchName":"Masaka"},
        if ($type == "Code") {
            $branchId = "05";
        } else {
            $branchId = "404724739880075701";
        }
    } else if ($docty == "SINV/MBR" || $docty == "CSIB/MBR") { //07-Mbarara {"branchName":"Mbarara"},
        if ($type == "Code") {
            $branchId = "07";
        } else {
            $branchId = "404724741329492105";
        }
    } else if ($docty == "SCI/MBR" || $docty == "SIS/MBR") { //07-Mbarara {"branchName":"Mbarara"},
        if ($type == "Code") {
            $branchId = "07";
        } else {
            $branchId = "404724741329492105";
        }
    } else if ($docty == "SINV/JNJ" || $docty == "CSIB/JNJ") { //09-Jinja{"branchName":"Jinja"},
        if ($type == "Code") {
            $branchId = "09";
        } else {
            $branchId = "404724743318115459";
        }
    } else if ($docty == "SINV/MBL" || $docty == "CSIB/MBL") { //08-Mbale{"branchName":"Mbale"},
        if ($type == "Code") {
            $branchId = "08";
        } else {
            $branchId = "404724742247143207";
        }
    } else if ($docty == "SINV/GUL" || $docty == "CSIB/GUL" || $docty == "SCI/GUL" || $docty == "SIS/GUL") { //04-Gulu {"branchName":"Gulu"},
        if ($type == "Code") {
            $branchId = "04";
        } else {
            $branchId = "404724738683503208";
        }
    } else if ($docty == "SINV/FPL" || $docty == "CSIB/FPL" || $docty == "SCI/FPL" || $docty == "SIS/FPL") { //13-Fort Portal{"branchName":"Fort Portal"},
        if ($type == "Code") {
            $branchId = "13";
        } else {
            $branchId = "404724746840610458";
        }
    } else if ($docty == "SINV/LRA" || $docty == "CSIB/LRA") { //14-Lira{"branchName":"Lira"},
        if ($type == "Code") {
            $branchId = "14";
        } else {
            $branchId = "404724748112561851";
        }
    } else if ($docty == "SINV/HMA" || $docty == "CSIB/HMA") { //15-Hoima{"branchName":"Hoima"}
        if ($type == "Code") {
            $branchId = "15";
        } else {
            $branchId = "404724748593400053";
        }
    } else if ($docty == "SINV/IGN" || $docty == "CSIB/IGN") { //16-Iganga{"branchName":"Iganga"},
        if ($type == "Code") {
            $branchId = "16";
        } else {
            $branchId = "404724749374648055";
        }
    } else if ($docty == "RS/RTS") { //17-Route Sales Van,
        if ($type == "Code") {
            $branchId = "17";
        } else {
            $branchId = "414391414125100586";
        }
    }
    return $branchId;
}


function PrintingMaiiling($id, $VOUCHERNUMBER, $Customer, $sPARTYNAME)
{
    $url = "http://localhost/sas/pages/forms/reports/reportsPrintInvoice.php?type=printinvoice&id="
        . $id . "&VOUCHERNUMBER=" . $VOUCHERNUMBER
        . "&UserName=sas&IsSignature=true&IsURASubmit=true&ptype=sendmail&Customer="
        . urlencode($Customer) . "&PARTYNAME=" . urlencode($sPARTYNAME);
    //echo $url."<br>";
    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_PORT, 80);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE); //$lines = array(); $lines = explode("n", $server_output); foreach($lines as $line_num => $line) {echo "Line # {$line_num} : ".htmlspecialchars($line)."n";}
        if (FALSE === $server_output) {
            throw new Exception(curl_error($ch), curl_errno($ch));
        }
        curl_close($ch);

    } catch (Exception $e) {
        echo 'Curl failed with error ' . $e->getCode() . ' ' . $e->getMessage();
    }

}

function getInvoiceList($VOUCHERNUMBER)
{
    $retu = false;
    $inv_list = array("SINV/NDB21/0556", "SINV/NDW21/0237", "SINV/JNJ21/0385", "SINV/NDW21/0236",
        "SINV/NDW21/0235", "SINV/NDB21/0450", "SINV/MSK21/0281", "SINV/NDB21/0449", "SINV/LUG21/0604",
        "CSIB/NDW21/0023", "SINV/NDW21/0234", "SINV/JNJ21/0384", "SINV/NDW21/0233", "CSIB/NDW21/0022",
        "SINV/LUG21/0603", "CSIB/NDW21/0021", "CSIB/LUG21/0020", "SINV/NDB21/0448", "SINV/MBR21/0221",
        "SINV/BMB21/0482", "SINV/BMB21/0481", "SINV/BMB21/0480", "SINV/BMB21/0479", "SINV/BMB21/0478",
        "SINV/NDB21/0447", "SINV/BMB21/0477", "SINV/BMB21/0476", "SINV/NDW21/0232", "SINV/NDB21/0446",
        "SINV/NDW21/0230", "SINV/NDW21/0229", "SINV/NDW21/0231", "SINV/NDB21/0445", "SINV/NDB21/0444",
        "SINV/NDB21/0443", "SINV/MSK21/0280", "SINV/MSK21/0279", "SINV/MSK21/0278", "SINV/MBR21/0220",
        "SINV/MBR21/0219", "SINV/MBR21/0218", "SINV/MBR21/0217", "SINV/LUG21/0601", "SINV/LUG21/0599",
        "SINV/LUG21/0600", "SINV/BMB21/0475", "SINV/JNJ21/0383", "SINV/BMB21/0474", "SINV/BMB21/0473",
        "SINV/MBR21/0216", "SINV/JNJ21/0382", "SINV/LUG21/0598", "SINV/GUL21/0077", "SINV/GUL21/0076",
        "SINV/BMB21/0472", "SINV/NDB21/0442", "SINV/BMB21/0471", "SINV/LUG21/0597", "SINV/NDB21/0441",
        "SINV/MSK21/0277", "SINV/MSK21/0276", "SINV/BMB21/0470", "SINV/BMB21/0469", "SINV/NDB21/0440",
        "SINV/MSK21/0275", "SINV/LUG21/0596", "SINV/NDB21/0439", "SINV/JNJ21/0381", "SINV/LUG21/0595",
        "SINV/NDB21/0438", "SINV/BMB21/0468", "SINV/LUG21/0594", "SINV/BMB21/0467", "SINV/MBR21/0215",
        "SINV/MSK21/0274", "SINV/LUG21/0593", "SINV/NDB21/0437", "SINV/MBR21/0214", "SINV/JNJ21/0380",
        "SINV/BMB21/0466", "SINV/BMB21/0465", "SINV/BMB21/0464", "SINV/MSK21/0273", "SINV/LUG21/0592",
        "SINV/JNJ21/0379", "SINV/NDB21/0436", "SINV/NDW21/0227", "SINV/NDW21/0226", "SINV/NDB21/0431",
        "SINV/LUG21/0591", "SINV/NDB21/0429", "SINV/NDB21/0428", "SINV/MSK21/0267", "SINV/NDB21/0427",
        "SINV/MBR21/0213", "SINV/MBR21/0212", "SINV/NDB21/0426", "SINV/NDB21/0425", "SINV/MBR21/0211",
        "SINV/MBR21/0210", "SINV/LUG21/0588", "SINV/LUG21/0587", "SINV/JNJ21/0375", "SINV/JNJ21/0374",
        "SINV/JNJ21/0373", "SINV/BMB21/0460", "SINV/BMB21/0459", "SINV/BMB21/0458", "CSIB/MSK21/0004",
        "CSIB/MBR21/0005", "CSIB/JNJ21/0014", "SINV/MSK21/0272", "SINV/NDW21/0228", "SINV/BMB21/0463",
        "SINV/NDB21/0435", "SINV/NDB21/0434", "SINV/NDB21/0433", "SINV/MSK21/0271", "SINV/MSK21/0270",
        "SINV/MSK21/0269", "SINV/MSK21/0268", "SINV/NDB21/0432", "SINV/LUG21/0590", "SINV/LUG21/0589",
        "SINV/JNJ21/0377", "SINV/JNJ21/0376", "SINV/BMB21/0461", "SINV/BMB21/0462", "SINV/JNJ21/0372",
        "SINV/JNJ21/0371", "SINV/LUG21/0586", "SINV/LUG21/0585", "SINV/BMB21/0457", "SINV/JNJ21/0370",
        "SINV/LUG21/0584", "SINV/MSK21/0266", "SINV/NDB21/0424", "SINV/JNJ21/0369", "SINV/LUG21/0583",
        "SINV/JNJ21/0368", "SINV/LUG21/0548", "SINV/BMB21/0437", "SINV/BMB21/0412", "SINV/MSK21/0265",
        "SINV/LUG21/0494", "SINV/MSK21/0264", "SINV/JNJ21/0367", "SINV/NDB21/0355", "SINV/NDB21/0352",
        "SINV/NDB21/0350", "SINV/NDB21/0347", "SINV/NDB21/0353", "SINV/BMB21/0362", "SINV/NDB21/0343",
        "SINV/BMB21/0359", "SINV/NDB21/0342", "SINV/BMB21/0360", "SINV/NDB21/0423", "SINV/NDB21/0422",
        "SINV/NDB21/0316", "SINV/NDB21/0325", "SINV/NDW21/0225", "SINV/LUG21/0513", "SINV/BMB21/0456",
        "SINV/NDB21/0421", "SINV/NDB21/0420", "SINV/GUL21/0069", "SINV/NDB21/0419", "SINV/BMB21/0455",
        "SINV/NDW21/0215", "SINV/NDW21/0210", "SINV/NDW21/0214", "SINV/NDW21/0209", "SINV/NDW21/0213",
        "SINV/NDW21/0208", "SINV/NDW21/0212", "SINV/NDW21/0207", "SINV/NDW21/0211", "SINV/NDW21/0205",
        "SINV/NDW21/0204", "SINV/NDW21/0203", "SINV/NDW21/0206", "SINV/NDW21/0202", "SINV/NDW21/0199",
        "SINV/NDW21/0198", "SINV/NDW21/0201", "SINV/NDW21/0200", "SINV/NDW21/0195", "SINV/NDB21/0384",
        "SINV/NDB21/0383", "SINV/NDW21/0197", "SINV/NDW21/0196", "SINV/NDB21/0380", "SINV/NDB21/0382",
        "SINV/MSK21/0234", "SINV/NDB21/0381", "SINV/MSK21/0232", "SINV/MSK21/0235", "SINV/MSK21/0231",
        "SINV/MSK21/0230", "SINV/MSK21/0233", "SINV/MSK21/0229", "SINV/MBR21/0193", "SINV/MSK21/0228",
        "SINV/MSK21/0227", "SINV/MBL21/0062", "SINV/MBR21/0194", "SINV/LUG21/0523", "SINV/MBR21/0192",
        "SINV/LUG21/0522", "SINV/LUG21/0521", "SINV/MBR21/0191", "SINV/LUG21/0520", "SINV/LUG21/0519",
        "SINV/LUG21/0518", "SINV/LUG21/0517", "SINV/LUG21/0516", "SINV/LUG21/0515", "SINV/LUG21/0512",
        "SINV/LUG21/0514", "SINV/LUG21/0509", "SINV/LUG21/0508", "SINV/LUG21/0511", "SINV/LUG21/0510",
        "SINV/LUG21/0506", "SINV/LUG21/0505", "SINV/LUG21/0507", "SINV/LUG21/0501", "SINV/LUG21/0500",
        "SINV/LUG21/0504", "SINV/JNJ21/0341", "SINV/LUG21/0503", "SINV/LUG21/0502", "SINV/JNJ21/0337",
        "SINV/JNJ21/0340", "SINV/JNJ21/0336", "SINV/JNJ21/0339", "SINV/JNJ21/0334", "SINV/JNJ21/0338",
        "SINV/JNJ21/0333", "SINV/JNJ21/0335", "SINV/GUL21/0073", "SINV/JNJ21/0332", "SINV/GUL21/0074",
        "SINV/GUL21/0072", "SINV/GUL21/0071", "SINV/BMB21/0408", "SINV/GUL21/0070", "SINV/BMB21/0407",
        "SINV/MSK21/0263", "SINV/BMB21/0406", "SINV/BMB21/0454", "SINV/BMB21/0409", "SINV/BMB21/0405",
        "SINV/BMB21/0401", "SINV/BMB21/0404", "SINV/BMB21/0403", "SINV/BMB21/0399", "SINV/BMB21/0398",
        "SINV/BMB21/0402", "SINV/BMB21/0400", "CSIB/NDW21/0020", "CSIB/LUG21/0019", "SINV/BMB21/0397",
        "SINV/MSK21/0262", "SINV/LUG21/0582", "SINV/BMB21/0453", "SINV/JNJ21/0366", "SINV/JNJ21/0365",
        "SINV/BMB21/0452", "SINV/MBR21/0209", "SINV/LUG21/0581", "SINV/MSK21/0261", "SINV/MBR21/0208",
        "SINV/BMB21/0451", "SINV/MBR21/0207", "SINV/BMB21/0450", "SINV/LUG21/0580", "SINV/MSK21/0260",
        "SINV/LUG21/0579", "SINV/LUG21/0578", "SINV/NDB21/0418", "SINV/BMB21/0449", "SINV/LUG21/0577",
        "SINV/LUG21/0576", "SINV/LUG21/0575", "CSIB/MBL21/0003", "CSIB/JNJ21/0013", "SINV/LUG21/0574",
        "SINV/JNJ21/0364", "SINV/NDB21/0417", "SINV/JNJ21/0363", "SINV/MSK21/0259", "SINV/MSK21/0258",
        "SINV/NDB21/0416", "SINV/BMB21/0448", "SINV/NDB21/0415", "SINV/LUG21/0573", "SINV/NDB21/0414",
        "SINV/LUG21/0572", "SINV/JNJ21/0362", "SINV/NDB21/0413", "SINV/BMB21/0447", "SINV/MSK21/0257",
        "SINV/MBR21/0206", "SINV/LUG21/0571", "SINV/JNJ21/0361", "SINV/NDW21/0224", "SINV/NDW21/0223",
        "SINV/NDW21/0222", "SINV/MSK21/0256", "SINV/LUG21/0570", "SINV/LUG21/0569", "SINV/BMB21/0446",
        "SINV/JNJ21/0360", "SINV/NDB21/0412", "SINV/BMB21/0445", "SINV/LUG21/0568", "SINV/BMB21/0444",
        "SINV/JNJ21/0359", "SINV/JNJ21/0358", "SINV/MBR21/0205", "SINV/BMB21/0443", "SINV/LUG21/0567",
        "SINV/JNJ21/0357", "SINV/NDB21/0411", "SINV/JNJ21/0356", "SINV/MBR21/0204", "SINV/MSK21/0255",
        "SINV/JNJ21/0355", "SINV/NDB21/0410", "SINV/LUG21/0566", "SINV/BMB21/0442", "SINV/MSK21/0254",
        "SINV/JNJ21/0354", "SINV/NDB21/0409", "SINV/MSK21/0253", "SINV/GUL21/0075", "SINV/JNJ21/0353",
        "SINV/MSK21/0252", "SINV/NDB21/0408", "SINV/BMB21/0441", "SINV/NDW21/0220", "SINV/BMB21/0440",
        "SINV/NDW21/0219", "SINV/NDW21/0218", "SINV/NDW21/0216", "SINV/NDB21/0407", "SINV/NDB21/0406",
        "SINV/NDB21/0405", "SINV/NDB21/0404", "SINV/NDB21/0403", "SINV/NDB21/0402", "SINV/NDB21/0401",
        "SINV/NDB21/0400", "SINV/NDB21/0397", "SINV/NDB21/0399", "SINV/NDB21/0398", "SINV/NDB21/0396",
        "SINV/NDB21/0395", "SINV/NDB21/0394", "SINV/NDB21/0393", "SINV/NDB21/0392", "SINV/NDB21/0389",
        "SINV/NDW21/0221", "SINV/NDB21/0388", "SINV/NDB21/0387", "SINV/NDB21/0386", "SINV/NDB21/0385",
        "SINV/NDW21/0217", "SINV/MSK21/0251", "SINV/MSK21/0250", "SINV/MSK21/0249", "SINV/MSK21/0247",
        "SINV/MSK21/0246", "SINV/MSK21/0245", "SINV/MSK21/0244", "SINV/MSK21/0243", "SINV/MSK21/0242",
        "SINV/MSK21/0241", "SINV/MSK21/0240", "SINV/MSK21/0238", "SINV/MSK21/0237", "SINV/MBR21/0203",
        "SINV/MBR21/0202", "SINV/MBR21/0201", "SINV/NDB21/0391", "SINV/NDB21/0390", "SINV/MBR21/0197",
        "SINV/MBR21/0196", "SINV/MBR21/0195", "SINV/MBL21/0068", "SINV/MBL21/0067", "SINV/MBL21/0066",
        "SINV/MSK21/0248", "SINV/MBL21/0063", "SINV/LUG21/0565", "SINV/LUG21/0564", "SINV/LUG21/0563",
        "SINV/LUG21/0562", "SINV/LUG21/0561", "SINV/LUG21/0560", "SINV/LUG21/0559", "SINV/LUG21/0558",
        "SINV/MSK21/0239", "SINV/LUG21/0557", "SINV/LUG21/0555", "SINV/MSK21/0236", "SINV/LUG21/0552",
        "SINV/LUG21/0551", "SINV/MBR21/0200", "SINV/LUG21/0550", "SINV/MBR21/0199", "SINV/LUG21/0549",
        "SINV/MBR21/0198", "SINV/LUG21/0547", "SINV/LUG21/0546", "SINV/LUG21/0545", "SINV/MBL21/0069",
        "SINV/LUG21/0544", "SINV/LUG21/0543", "SINV/LUG21/0541", "SINV/MBL21/0065", "SINV/MBL21/0064",
        "SINV/LUG21/0539", "SINV/LUG21/0538", "SINV/LUG21/0536", "SINV/LUG21/0534", "SINV/LUG21/0533",
        "SINV/LUG21/0532", "SINV/LUG21/0531", "SINV/LUG21/0529", "SINV/LUG21/0556", "SINV/LUG21/0528",
        "SINV/LUG21/0528", "SINV/LUG21/0554", "SINV/LUG21/0553", "SINV/LUG21/0526", "SINV/LUG21/0525",
        "SINV/LUG21/0524", "SINV/JNJ21/0351", "SINV/JNJ21/0349", "SINV/JNJ21/0348", "SINV/LUG21/0542",
        "SINV/JNJ21/0345", "SINV/JNJ21/0344", "SINV/JNJ21/0343", "SINV/LUG21/0540", "SINV/BMB21/0438",
        "SINV/LUG21/0537", "SINV/LUG21/0535", "SINV/LUG21/0530", "SINV/BMB21/0431", "SINV/BMB21/0430",
        "SINV/BMB21/0429", "SINV/BMB21/0428", "SINV/BMB21/0425", "SINV/BMB21/0424", "SINV/BMB21/0423",
        "SINV/BMB21/0422", "SINV/JNJ21/0350", "SINV/BMB21/0421", "SINV/BMB21/0420", "SINV/BMB21/0419",
        "SINV/JNJ21/0347", "SINV/JNJ21/0346", "SINV/BMB21/0418", "SINV/BMB21/0417", "SINV/BMB21/0416",
        "SINV/BMB21/0415", "SINV/JNJ21/0342", "SINV/BMB21/0414", "SINV/BMB21/0413", "SINV/BMB21/0439",
        "SINV/BMB21/0411", "SINV/BMB21/0436", "CSIB/JNJ21/0012", "SINV/BMB21/0435", "SINV/BMB21/0434",
        "SINV/BMB21/0433", "SINV/BMB21/0432", "SINV/BMB21/0427", "SINV/BMB21/0426", "SINV/NDB21/0379",
        "SINV/NDB21/0378", "SINV/NDW21/0194", "SINV/NDB21/0377", "SINV/MSK21/0226", "SINV/MSK21/0225",
        "SINV/LUG21/0499", "SINV/BMB21/0396", "SINV/JNJ21/0331", "SINV/JNJ21/0330", "SINV/BMB21/0395",
        "SINV/NDB21/0376", "SINV/BMB21/0394", "SINV/LUG21/0498", "SINV/JNJ21/0329", "SINV/BMB21/0393",
        "SINV/JNJ21/0328", "SINV/MBR21/0190", "SINV/NDW21/0193", "SINV/BMB21/0392", "SINV/JNJ21/0327",
        "SINV/LUG21/0497", "SINV/BMB21/0391", "SINV/BMB21/0390", "SINV/NDW21/0192", "SINV/BMB21/0389",
        "SINV/NDW21/0191", "SINV/LUG21/0496", "SINV/BMB21/0388", "SINV/MBL21/0061", "SINV/LUG21/0495",
        "SINV/JNJ21/0326", "SINV/BMB21/0387", "CSIB/MBR21/0004", "SINV/NDW21/0190", "SINV/NDB21/0375",
        "SINV/BMB21/0386", "SINV/MSK21/0224", "SINV/MBR21/0189", "SINV/NDB21/0374", "SINV/NDB21/0373",
        "SINV/NDW21/0189", "SINV/NDW21/0188", "SINV/NDB21/0370", "SINV/NDW21/0187", "SINV/NDB21/0369",
        "SINV/NDW21/0186", "SINV/NDW21/0185", "SINV/NDW21/0183", "SINV/NDB21/0372", "SINV/NDB21/0371",
        "SINV/NDB21/0362", "SINV/NDB21/0368", "SINV/NDB21/0367", "SINV/NDB21/0366", "SINV/MSK21/0221",
        "SINV/NDB21/0365", "SINV/NDB21/0364", "SINV/NDB21/0363", "SINV/MSK21/0217", "SINV/MBR21/0188",
        "SINV/NDB21/0361", "SINV/NDB21/0360", "SINV/MSK21/0223", "SINV/LUG21/0493", "SINV/MSK21/0222",
        "SINV/LUG21/0491", "SINV/MSK21/0220", "SINV/MSK21/0219", "SINV/MSK21/0218", "SINV/MBL21/0060",
        "SINV/JNJ21/0325", "SINV/JNJ21/0324", "SINV/LUG21/0492", "SINV/JNJ21/0322", "SINV/LUG21/0490",
        "SINV/JNJ21/0321", "SINV/LUG21/0489", "SINV/LUG21/0488", "SINV/LUG21/0487", "SINV/JNJ21/0318",
        "SINV/LUG21/0486", "SINV/GUL21/0068", "SINV/LUG21/0485", "SINV/BMB21/0384", "SINV/JNJ21/0323",
        "SINV/BMB21/0383", "SINV/BMB21/0382", "SINV/JNJ21/0320", "SINV/BMB21/0381", "SINV/JNJ21/0319",
        "SINV/GUL21/0067", "SINV/BMB21/0385", "SINV/BMB21/0380", "SINV/BMB21/0379", "CSIB/NDW21/0019",
        "SINV/NDW21/0184", "SINV/MBL21/0058", "SINV/GUL21/0066", "SINV/LUG21/0484", "SINV/JNJ21/0317",
        "SINV/LUG21/0483", "SINV/LUG21/0482", "SINV/NDB21/0359", "SINV/MSK21/0216", "SINV/JNJ21/0316",
        "SINV/BMB21/0378", "SINV/MSK21/0215", "SINV/JNJ21/0315", "SINV/LUG21/0481", "SINV/LUG21/0480",
        "SINV/JNJ21/0314", "SINV/LUG21/0479", "SINV/BMB21/0377", "SINV/LUG21/0478", "SINV/LUG21/0477",
        "SINV/NDB21/0358", "SINV/LUG21/0476", "SINV/LUG21/0475", "SINV/MSK21/0214", "SINV/BMB21/0376",
        "SINV/MSK21/0213", "SINV/LUG21/0474", "SINV/NDW21/0182", "SINV/NDW21/0181", "SINV/NDB21/0357",
        "SINV/LUG21/0473", "SINV/JNJ21/0313", "SINV/JNJ21/0312", "SINV/GUL21/0064", "SINV/MSK21/0212",
        "SINV/JNJ21/0311", "SINV/MBR21/0187", "SINV/MSK21/0211", "SINV/NDB21/0326", "SINV/NDB21/0320",
        "SINV/MBR21/0186", "SINV/JNJ21/0310", "SINV/NDW21/0180", "SINV/BMB21/0375", "SINV/MBR21/0185",
        "SINV/LUG21/0472", "SINV/BMB21/0374", "SINV/LUG21/0471", "SINV/BMB21/0373", "SINV/BMB21/0372",
        "SINV/NDB21/0356", "SINV/MSK21/0210", "SINV/NDW21/0179", "SINV/LUG21/0470", "SINV/LUG21/0469",
        "SINV/BMB21/0371", "SINV/MSK21/0209", "SINV/NDW21/0178", "SINV/NDW21/0177", "SINV/NDW21/0176",
        "SINV/LUG21/0468", "SINV/LUG21/0467", "SINV/BMB21/0370", "SINV/NDW21/0175", "SINV/MSK21/0208",
        "SINV/BMB21/0369", "SINV/LUG21/0466", "SINV/MSK21/0207", "SINV/LUG21/0465", "SINV/MSK21/0206",
        "SINV/NDB21/0354", "SINV/JNJ21/0309", "SINV/MSK21/0205", "SINV/JNJ21/0308", "SINV/BMB21/0368",
        "SINV/LUG21/0463", "SINV/LUG21/0464", "SINV/MSK21/0204", "SINV/LUG21/0462", "SINV/MSK21/0203",
        "SINV/MSK21/0202", "SINV/NDW21/0174", "SINV/MSK21/0201", "SINV/NDB21/0351", "SINV/JNJ21/0307",
        "SINV/LUG21/0461", "SINV/BMB21/0367", "SINV/NDW21/0173", "SINV/MBL21/0057", "SINV/LUG21/0460",
        "SINV/NDW21/0172", "SINV/NDW21/0171", "SINV/NDW21/0169", "SINV/NDB21/0346", "SINV/NDW21/0170",
        "SINV/NDW21/0168", "SINV/MSK21/0200", "SINV/NDB21/0349", "SINV/MSK21/0198", "SINV/NDB21/0348",
        "SINV/NDB21/0345", "SINV/NDB21/0344", "SINV/MBR21/0183", "SINV/MBR21/0182", "SINV/NDB21/0341",
        "SINV/NDB21/0340", "SINV/MSK21/0199", "SINV/MBL21/0054", "SINV/MSK21/0197", "SINV/MSK21/0196",
        "SINV/LUG21/0458", "SINV/MBR21/0184", "SINV/LUG21/0456", "SINV/MBR21/0181", "SINV/MBL21/0056",
        "SINV/MBL21/0055", "SINV/LUG21/0455", "SINV/MBL21/0053", "SINV/LUG21/0459", "SINV/LUG21/0457",
        "SINV/JNJ21/0306", "SINV/LUG21/0454", "SINV/LUG21/0453", "SINV/JNJ21/0305", "SINV/JNJ21/0304",
        "SINV/JNJ21/0303", "SINV/JNJ21/0302", "SINV/JNJ21/0301", "SINV/GUL21/0063", "SINV/BMB21/0366",
        "SINV/BMB21/0365", "SINV/BMB21/0364", "SINV/BMB21/0361", "SINV/BMB21/0363", "SINV/BMB21/0358",
        "SINV/BMB21/0357", "SINV/NDB21/0329", "SINV/NDB21/0328", "SINV/NDB21/0327", "SINV/MSK21/0190",
        "SINV/MSK21/0189", "SINV/MSK21/0188", "SINV/MSK21/0187", "SINV/MBR21/0175", "SINV/MBR21/0174",
        "SINV/MBR21/0173", "SINV/MBR21/0172", "SINV/MBL21/0051", "SINV/MBL21/0050", "SINV/LUG21/0436",
        "SINV/LUG21/0435", "SINV/LUG21/0434", "SINV/LUG21/0433", "SINV/LUG21/0432", "SINV/LUG21/0431",
        "SINV/LUG21/0430", "SINV/JNJ21/0288", "SINV/LUG21/0428", "SINV/LUG21/0427", "SINV/LUG21/0426",
        "SINV/LUG21/0425", "SINV/LUG21/0424", "SINV/LUG21/0423", "SINV/LUG21/0422", "SINV/JNJ21/0289",
        "SINV/BMB21/0325", "SINV/JNJ21/0287", "SINV/JNJ21/0286", "SINV/JNJ21/0285", "SINV/BMB21/0329",
        "SINV/BMB21/0328", "SINV/BMB21/0327", "SINV/BMB21/0326", "CSIB/JNJ21/0011", "SINV/NDW21/0167",
        "SINV/NDW21/0166", "SINV/NDW21/0165", "SINV/NDW21/0164", "SINV/NDW21/0163", "SINV/NDW21/0162",
        "SINV/NDW21/0161", "SINV/NDW21/0152", "SINV/NDW21/0160", "SINV/NDW21/0159", "SINV/NDW21/0158",
        "SINV/NDW21/0157", "SINV/NDW21/0156", "SINV/NDW21/0155", "SINV/NDW21/0154", "SINV/NDW21/0153",
        "SINV/NDB21/0339", "SINV/NDB21/0338", "SINV/NDB21/0337", "SINV/NDB21/0336", "SINV/NDB21/0335",
        "SINV/NDB21/0334", "SINV/NDB21/0333", "SINV/NDB21/0332", "SINV/NDB21/0331", "SINV/NDB21/0330",
        "SINV/MSK21/0195", "SINV/MSK21/0194", "SINV/MSK21/0193", "SINV/MSK21/0192", "SINV/MSK21/0191",
        "SINV/MBR21/0180", "SINV/LUG21/0448", "SINV/MBR21/0179", "SINV/MBR21/0178", "SINV/MBR21/0177",
        "SINV/MBR21/0176", "SINV/MBL21/0052", "SINV/LUG21/0452", "SINV/LUG21/0451", "SINV/LUG21/0450",
        "SINV/LUG21/0449", "SINV/LUG21/0447", "SINV/LUG21/0446", "SINV/LUG21/0445", "SINV/LUG21/0444",
        "SINV/LUG21/0443", "SINV/LUG21/0442", "SINV/LUG21/0441", "SINV/LUG21/0440", "SINV/LUG21/0439",
        "SINV/LUG21/0438", "SINV/LUG21/0437", "SINV/JNJ21/0291", "SINV/JNJ21/0300", "SINV/JNJ21/0299",
        "SINV/JNJ21/0298", "SINV/GUL21/0061", "SINV/JNJ21/0297", "SINV/JNJ21/0296", "SINV/JNJ21/0295",
        "SINV/JNJ21/0294", "SINV/JNJ21/0293", "SINV/JNJ21/0292", "SINV/BMB21/0352", "SINV/JNJ21/0290",
        "SINV/GUL21/0062", "SINV/GUL21/0060", "SINV/BMB21/0356", "SINV/BMB21/0355", "SINV/BMB21/0354",
        "SINV/BMB21/0353", "SINV/BMB21/0351", "SINV/BMB21/0350", "SINV/BMB21/0349", "SINV/BMB21/0348",
        "SINV/BMB21/0347", "SINV/BMB21/0346", "SINV/BMB21/0345", "SINV/BMB21/0344", "SINV/BMB21/0343",
        "SINV/BMB21/0342", "SINV/BMB21/0341", "SINV/BMB21/0340", "SINV/BMB21/0339", "SINV/BMB21/0338",
        "SINV/BMB21/0337", "SINV/BMB21/0336", "SINV/BMB21/0335", "SINV/BMB21/0334", "SINV/BMB21/0333",
        "SINV/BMB21/0332", "SINV/BMB21/0331", "SINV/BMB21/0330", "CSIB/NDW21/0018", "CSIB/NDW21/0017",
        "SINV/BMB21/0314", "SINV/BMB21/0324", "SINV/BMB21/0323", "SINV/BMB21/0322", "SINV/BMB21/0321",
        "SINV/LUG21/0421", "SINV/JNJ21/0284", "SINV/LUG21/0420", "SINV/BMB21/0320", "SINV/JNJ21/0283",
        "SINV/BMB21/0319", "SINV/NDW21/0151", "SINV/LUG21/0419", "SINV/JNJ21/0282", "SINV/NDB21/0324",
        "SINV/NDB21/0323", "SINV/LUG21/0418", "SINV/JNJ21/0281", "SINV/NDB21/0319", "SINV/NDB21/0322",
        "SINV/NDB21/0321", "SINV/LUG21/0417", "SINV/NDB21/0318", "SINV/LUG21/0415", "SINV/LUG21/0414",
        "SINV/NDB21/0317", "SINV/MSK21/0186", "SINV/LUG21/0411", "SINV/MSK21/0185", "SINV/LUG21/0410",
        "SINV/LUG21/0416", "SINV/JNJ21/0277", "SINV/LUG21/0413", "SINV/LUG21/0412", "SINV/JNJ21/0280",
        "SINV/JNJ21/0279", "SINV/JNJ21/0278", "SINV/GUL21/0059", "SINV/GUL21/0058", "SINV/BMB21/0316",
        "SINV/GUL21/0057", "SINV/BMB21/0318", "SINV/BMB21/0317", "SINV/NDW21/0150", "SINV/NDW21/0148",
        "SINV/NDB21/0313", "SINV/NDB21/0312", "SINV/NDB21/0309", "SINV/NDB21/0308", "SINV/NDB21/0305",
        "SINV/NDB21/0304", "SINV/NDB21/0303", "SINV/NDB21/0302", "SINV/MSK21/0182", "SINV/MSK21/0178",
        "SINV/NDW21/0149", "SINV/NDW21/0147", "SINV/NDW21/0146", "SINV/NDB21/0315", "SINV/NDB21/0314",
        "SINV/MBR21/0171", "SINV/NDB21/0311", "SINV/NDB21/0310", "SINV/NDB21/0307", "SINV/MBR21/0166",
        "SINV/NDB21/0306", "SINV/MBR21/0165", "SINV/MBR21/0164", "SINV/MBL21/0049", "SINV/LUG21/0408",
        "SINV/NDB21/0301", "SINV/NDB21/0300", "SINV/LUG21/0405", "SINV/NDB21/0299", "SINV/LUG21/0404",
        "SINV/NDB21/0298", "SINV/MSK21/0184", "SINV/MSK21/0183", "SINV/LUG21/0400", "SINV/MSK21/0181",
        "SINV/MSK21/0180", "SINV/MSK21/0179", "SINV/MSK21/0177", "SINV/MSK21/0176", "SINV/MSK21/0175",
        "SINV/LUG21/0393", "SINV/MSK21/0174", "SINV/MSK21/0173", "SINV/MSK21/0172", "SINV/MSK21/0171",
        "SINV/LUG21/0390", "SINV/MBR21/0170", "SINV/LUG21/0388", "SINV/MBR21/0169", "SINV/MBR21/0168",
        "SINV/LUG21/0387", "SINV/LUG21/0385", "SINV/MBR21/0167", "SINV/LUG21/0384", "SINV/LUG21/0383",
        "SINV/LUG21/0382", "SINV/LUG21/0381", "SINV/LUG21/0380", "SINV/LUG21/0409", "SINV/LUG21/0407",
        "SINV/LUG21/0406", "INV/LUG21/0376", "SINV/LUG21/0375", "SINV/JNJ21/0276", "SINV/LUG21/0403",
        "SINV/LUG21/0402", "SINV/JNJ21/0274", "SINV/LUG21/0401", "SINV/JNJ21/0273", "SINV/LUG21/0399",
        "SINV/JNJ21/0271", "SINV/LUG21/0398", "SINV/JNJ21/0270", "SINV/LUG21/0397", "SINV/JNJ21/0269",
        "SINV/LUG21/0396", "SINV/JNJ21/0268", "SINV/LUG21/0395", "SINV/JNJ21/0267", "SINV/LUG21/0394",
        "SINV/JNJ21/0266", "SINV/JNJ21/0265", "SINV/JNJ21/0264", "SINV/GUL21/0056", "SINV/LUG21/0392",
        "SINV/LUG21/0391", "SINV/LUG21/0389", "SINV/BMB21/0313", "SINV/BMB21/0312", "SINV/BMB21/0311",
        "SINV/LUG21/0386", "SINV/BMB21/0310", "SINV/BMB21/0308", "SINV/BMB21/0303", "SINV/LUG21/0379",
        "SINV/BMB21/0302", "SINV/LUG21/0378", "SINV/BMB21/0301", "SINV/LUG21/0377", "SINV/BMB21/0298",
        "SINV/JNJ21/0275", "SINV/JNJ21/0272", "SINV/BMB21/0315", "SINV/BMB21/0309", "SINV/BMB21/0307",
        "SINV/BMB21/0306", "SINV/BMB21/0305", "SINV/BMB21/0304", "SINV/BMB21/0300", "SINV/BMB21/0299",
        "CSIB/NDB21/0001");
    //echo "<pre>".$VOUCHERNUMBER; print_r($inv_list); echo "</pre>";
    foreach ($inv_list as $key => $value) { //echo $key."======================".$value."<br>";
        //if ($VOUCHERNUMBER == "SINV/BMB21/0314") { echo "======================".$value."<br>";}
        if ($value == $VOUCHERNUMBER) {
            return true;
            $retu = true;
        }
    } //exit;
    return $retu;
}


?>
