USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_HeaderData3333_0]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_HeaderData3333_0](
	[iHeaderId] [int] NOT NULL,
	[sNarration] [nvarchar](200) NULL,
	[Delivery_Terms] [nvarchar](150) NULL,
	[Delivery_At] [nvarchar](100) NULL,
	[Payment_Terms] [nvarchar](200) NULL,
	[PO_No] [nvarchar](30) NULL,
	[PO_Date] [int] NULL,
	[Receiver_Name] [nvarchar](60) NULL,
	[Receiver_Mobile_No] [nvarchar](12) NULL,
	[Available_Points] [numeric](18, 5) NULL,
 CONSTRAINT [pk_tCore_HeaderData3333_0] PRIMARY KEY CLUSTERED
(
	[iHeaderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
