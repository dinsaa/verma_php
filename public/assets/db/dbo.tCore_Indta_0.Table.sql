USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_Indta_0]    Script Date: 19-04-2023 9:27:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_Indta_0](
	[iBodyId] [int] NULL,
	[iProduct] [int] NULL,
	[fQuantity] [numeric](18, 7) NULL,
	[iUnit] [int] NULL,
	[mRate] [numeric](28, 10) NULL,
	[mGross] [numeric](28, 10) NULL,
	[mStockValue] [numeric](28, 10) NULL,
	[fQuantityInBase] [numeric](18, 7) NOT NULL,
	[fAlternateQty] [numeric](18, 7) NULL,
	[bIsSetName] [bit] NULL,
	[bQCPending] [bit] NULL,
	[bQCDone] [bit] NULL,
	[iQCStatus] [tinyint] NULL,
	[iSetId] [int] NULL,
	[iComboId] [int] NULL,
	[bBinConf] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tCore_Indta_0]  WITH CHECK ADD  CONSTRAINT [fk_iBodyId_tCore_Indta_0] FOREIGN KEY([iBodyId])
REFERENCES [dbo].[tCore_Data_0] ([iBodyId])
GO
ALTER TABLE [dbo].[tCore_Indta_0] CHECK CONSTRAINT [fk_iBodyId_tCore_Indta_0]
GO
ALTER TABLE [dbo].[tCore_Indta_0]  WITH CHECK ADD  CONSTRAINT [fk_iProduct_tCore_Indta_0] FOREIGN KEY([iProduct])
REFERENCES [dbo].[mCore_Product] ([iMasterId])
GO
ALTER TABLE [dbo].[tCore_Indta_0] CHECK CONSTRAINT [fk_iProduct_tCore_Indta_0]
GO
ALTER TABLE [dbo].[tCore_Indta_0]  WITH CHECK ADD  CONSTRAINT [fk_iUnit_tCore_Indta_0] FOREIGN KEY([iUnit])
REFERENCES [dbo].[mCore_Units] ([iMasterId])
GO
ALTER TABLE [dbo].[tCore_Indta_0] CHECK CONSTRAINT [fk_iUnit_tCore_Indta_0]
GO
