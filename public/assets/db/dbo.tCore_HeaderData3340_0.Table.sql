USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_HeaderData3340_0]    Script Date: 19-04-2023 9:27:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_HeaderData3340_0](
	[iHeaderId] [int] NOT NULL,
	[sNarration] [nvarchar](60) NULL,
	[Delivery_Terms] [nvarchar](150) NULL,
	[Delivery_At] [nvarchar](100) NULL,
	[Payment_Terms] [nvarchar](200) NULL,
	[Receiver_Name] [nvarchar](60) NULL,
	[Receiver_Mobile_No] [nvarchar](10) NULL
) ON [PRIMARY]
GO
