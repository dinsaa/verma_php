USE [Focus8040]
GO
/****** Object:  Table [dbo].[mSec_Users_Roles]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mSec_Users_Roles](
	[iUserId] [int] NOT NULL,
	[iYearId] [tinyint] NOT NULL,
	[iERPRole] [int] NULL,
	[iCRMRole] [int] NULL,
 CONSTRAINT [pk_mSec_Users_Roles] PRIMARY KEY CLUSTERED
(
	[iUserId] ASC,
	[iYearId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[mSec_Users_Roles]  WITH CHECK ADD  CONSTRAINT [fk_iERPRole_mSec_Users_Roles] FOREIGN KEY([iERPRole])
REFERENCES [dbo].[mSec_RoleHeader] ([iRoleId])
GO
ALTER TABLE [dbo].[mSec_Users_Roles] CHECK CONSTRAINT [fk_iERPRole_mSec_Users_Roles]
GO
