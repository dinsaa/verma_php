USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_IndtaBodyScreenData_0]    Script Date: 19-04-2023 9:27:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_IndtaBodyScreenData_0](
	[iBodyId] [int] NOT NULL,
	[mInput0] [money] NULL,
	[mVal0] [money] NULL,
	[iAccount0] [int] NULL,
	[mInput1] [decimal](18, 7) NULL,
	[mVal1] [decimal](18, 7) NULL,
	[iAccount1] [int] NULL,
	[mInput2] [decimal](18, 7) NULL,
	[mVal2] [decimal](18, 7) NULL,
	[iAccount2] [int] NULL,
	[mInput3] [decimal](18, 7) NULL,
	[mVal3] [decimal](18, 7) NULL,
	[iAccount3] [int] NULL,
	[mInput4] [decimal](18, 7) NULL,
	[mVal4] [decimal](18, 7) NULL,
	[iAccount4] [int] NULL,
	[mInput5] [decimal](18, 7) NULL,
	[mVal5] [decimal](18, 7) NULL,
	[iAccount5] [int] NULL,
	[mInput6] [decimal](18, 7) NULL,
	[mVal6] [decimal](18, 7) NULL,
	[iAccount6] [int] NULL,
	[mInput7] [decimal](18, 7) NULL,
	[mVal7] [decimal](18, 7) NULL,
	[iAccount7] [int] NULL,
	[mInput8] [decimal](18, 7) NULL,
	[mVal8] [decimal](18, 7) NULL,
	[iAccount8] [int] NULL,
	[mInput9] [decimal](18, 7) NULL,
	[mVal9] [decimal](18, 7) NULL,
	[iAccount9] [int] NULL,
 CONSTRAINT [pk_tCore_IndtaBodyScreenData_0] PRIMARY KEY CLUSTERED
(
	[iBodyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
