USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_IndtaFooterScreenData_0]    Script Date: 19-04-2023 9:27:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_IndtaFooterScreenData_0](
	[iHeaderId] [int] NOT NULL,
	[mInput0] [decimal](18, 7) NULL,
	[mVal0] [decimal](18, 7) NULL,
	[iAccount0] [int] NULL,
	[iCode0] [int] NULL,
	[mInput1] [decimal](18, 7) NULL,
	[mVal1] [decimal](18, 7) NULL,
	[iAccount1] [int] NULL,
	[iCode1] [int] NULL,
	[mInput2] [decimal](18, 7) NULL,
	[mVal2] [decimal](18, 7) NULL,
	[iAccount2] [int] NULL,
	[iCode2] [int] NULL,
	[mInput3] [decimal](18, 7) NULL,
	[mVal3] [decimal](18, 7) NULL,
	[iAccount3] [int] NULL,
	[iCode3] [int] NULL,
	[mInput4] [decimal](18, 7) NULL,
	[mVal4] [decimal](18, 7) NULL,
	[iAccount4] [int] NULL,
	[iCode4] [int] NULL,
 CONSTRAINT [pk_tCore_IndtaFooterScreenData_0] PRIMARY KEY CLUSTERED
(
	[iHeaderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
