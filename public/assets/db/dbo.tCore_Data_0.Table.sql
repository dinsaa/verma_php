USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_Data_0]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_Data_0](
	[iBodyId] [int] NOT NULL,
	[iHeaderId] [int] NOT NULL,
	[iTransactionId] [bigint] NOT NULL,
	[iSerialNo] [smallint] NULL,
	[iCode] [int] NULL,
	[iBookNo] [int] NULL,
	[iFaTag] [int] NULL,
	[iInvTag] [int] NULL,
	[iInvSettingTag] [int] NULL,
	[mAmount1] [numeric](28, 10) NULL,
	[mAmount2] [numeric](28, 10) NULL,
	[mAmount3] [numeric](28, 10) NULL,
	[mOriginalAmount] [numeric](28, 10) NULL,
	[iDueDate] [int] NOT NULL,
	[bSuspendUpdateStocks] [bit] NOT NULL,
	[bSuspendUpdateFA] [bit] NOT NULL,
	[bSuspendLinkSaved] [bit] NOT NULL,
	[bSuspendBaseSaved] [bit] NOT NULL,
	[bSuspendReservation] [bit] NOT NULL,
	[bChecked] [bit] NOT NULL,
	[bSpecialMeaning] [bit] NOT NULL,
	[bBrs] [bit] NOT NULL,
	[bPdc] [bit] NOT NULL,
	[bCOGS] [bit] NOT NULL,
	[bForexFlux] [bit] NULL,
	[bSuspendRef] [bit] NULL,
	[iType] [tinyint] NOT NULL,
	[iAuthStatus] [tinyint] NOT NULL,
	[bVoid] [bit] NULL,
	[bPnL] [bit] NULL,
	[bFreeQty] [bit] NULL,
	[bPDCDisc] [bit] NULL,
	[iMainBodyId] [int] NULL,
	[iLocationId] [smallint] NULL,
	[iBrsDate] [int] NULL,
	[iBankStatus] [tinyint] NULL,
	[bLocked] [bit] NULL,
	[bUpdateFA] [bit] NULL,
	[iCurrencyId] [int] NULL,
    [sync_status] [int] NULL,
 CONSTRAINT [pk_tCore_Data_0] PRIMARY KEY CLUSTERED
(
	[iBodyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[iTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tCore_Data_0]  WITH CHECK ADD  CONSTRAINT [fk_iBookNo_tCore_Data_0] FOREIGN KEY([iBookNo])
REFERENCES [dbo].[mCore_Account] ([iMasterId])
GO
ALTER TABLE [dbo].[tCore_Data_0] CHECK CONSTRAINT [fk_iBookNo_tCore_Data_0]
GO
ALTER TABLE [dbo].[tCore_Data_0]  WITH CHECK ADD  CONSTRAINT [fk_iCode_tCore_Data_0] FOREIGN KEY([iCode])
REFERENCES [dbo].[mCore_Account] ([iMasterId])
GO
ALTER TABLE [dbo].[tCore_Data_0] CHECK CONSTRAINT [fk_iCode_tCore_Data_0]
GO
ALTER TABLE [dbo].[tCore_Data_0]  WITH CHECK ADD  CONSTRAINT [fk_iFaTag_tCore_Data_0] FOREIGN KEY([iFaTag])
REFERENCES [dbo].[mCore_Location] ([iMasterId])
GO
ALTER TABLE [dbo].[tCore_Data_0] CHECK CONSTRAINT [fk_iFaTag_tCore_Data_0]
GO
ALTER TABLE [dbo].[tCore_Data_0]  WITH CHECK ADD  CONSTRAINT [fk_iHeaderId_tCore_Data_0] FOREIGN KEY([iHeaderId])
REFERENCES [dbo].[tCore_Header_0] ([iHeaderId])
GO
ALTER TABLE [dbo].[tCore_Data_0] CHECK CONSTRAINT [fk_iHeaderId_tCore_Data_0]
GO
ALTER TABLE [dbo].[tCore_Data_0]  WITH CHECK ADD  CONSTRAINT [fk_iInvTag_tCore_Data_0] FOREIGN KEY([iInvTag])
REFERENCES [dbo].[mCore_Warehouse] ([iMasterId])
GO
ALTER TABLE [dbo].[tCore_Data_0] CHECK CONSTRAINT [fk_iInvTag_tCore_Data_0]
GO
