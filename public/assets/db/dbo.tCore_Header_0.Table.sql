USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_Header_0]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_Header_0](
	[iHeaderId] [int] NOT NULL,
	[iDate] [int] NOT NULL,
	[iVoucherType] [smallint] NOT NULL,
	[sVoucherNo] [nvarchar](60) NOT NULL,
	[iUserId] [int] NULL,
	[iModifiedBy] [int] NULL,
	[iCreatedDate] [int] NULL,
	[iCreatedTime] [int] NULL,
	[iModifiedDate] [int] NULL,
	[iModifiedTime] [int] NULL,
	[bPostCashEntry] [bit] NOT NULL,
	[bTDSCertPrepared] [bit] NOT NULL,
	[bTDSPaid] [bit] NOT NULL,
	[bUpdateStocks] [bit] NOT NULL,
	[bSuspended] [bit] NOT NULL,
	[bCancelled] [bit] NOT NULL,
	[bVersion] [bit] NOT NULL,
	[iPrintCount] [smallint] NULL,
	[bModifiedDiffLoc] [bit] NULL,
	[bInternal] [bit] NULL,
	[iSyncReceivedDate] [bigint] NULL,
	[iEditingLocation] [int] NULL,
	[bSynced] [bit] NULL,
	[iTriggerBaseHeaderId] [int] NULL,
	[iGSTFlag] [tinyint] NULL,
	[bAmended] [bit] NULL,
	[iEmailCount] [smallint] NULL,
	[iSource] [smallint] NULL,
	[fNet] [numeric](28, 10) NULL,
	[fOrigNet] [numeric](28, 10) NULL,
	[iRepostId] [int] NULL,
	[bChequeReturn] [bit] NULL,
	[iAuth] [tinyint] NULL,
	[iVoucherClass] [int] NULL,
	[bDraft] [bit] NULL,
 CONSTRAINT [pk_tCore_Header_0] PRIMARY KEY CLUSTERED
(
	[iHeaderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tCore_Header_0] ADD  DEFAULT ((0)) FOR [bModifiedDiffLoc]
GO
ALTER TABLE [dbo].[tCore_Header_0]  WITH CHECK ADD  CONSTRAINT [fk_iModifiedBy_tCore_Header_0] FOREIGN KEY([iModifiedBy])
REFERENCES [dbo].[mSec_Users] ([iUserId])
GO
ALTER TABLE [dbo].[tCore_Header_0] CHECK CONSTRAINT [fk_iModifiedBy_tCore_Header_0]
GO
ALTER TABLE [dbo].[tCore_Header_0]  WITH CHECK ADD  CONSTRAINT [fk_iUserId_tCore_Header_0] FOREIGN KEY([iUserId])
REFERENCES [dbo].[mSec_Users] ([iUserId])
GO
ALTER TABLE [dbo].[tCore_Header_0] CHECK CONSTRAINT [fk_iUserId_tCore_Header_0]
GO
