USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_Data_Tags_0]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_Data_Tags_0](
	[iBodyId] [int] NOT NULL,
	[iTag3001] [int] NULL,
	[iTag3002] [int] NULL,
	[iTag3007] [int] NULL,
	[iTag3008] [int] NULL,
	[iTag3009] [int] NULL,
	[iTag3010] [int] NULL,
	[iTag3013] [int] NULL,
	[iTag3014] [int] NULL,
	[iTag3017] [int] NULL,
	[iTag3019] [int] NULL,
	[iTag3020] [int] NULL,
	[iTag3021] [int] NULL,
	[iTag3022] [int] NULL,
	[iTag3023] [int] NULL,
	[iTag3024] [int] NULL,
	[iTag3025] [int] NULL,
	[iTag3026] [int] NULL,
	[iTag5] [int] NULL,
	[iTag800] [int] NULL,
	[iTag818] [int] NULL,
	[iTag3005] [int] NULL,
PRIMARY KEY NONCLUSTERED
(
	[iBodyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
