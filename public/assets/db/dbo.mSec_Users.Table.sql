USE [Focus8040]
GO
/****** Object:  Table [dbo].[mSec_Users]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mSec_Users](
	[iUserId] [int] IDENTITY(0,1) NOT NULL,
	[sLoginName] [nvarchar](30) NOT NULL,
	[sLoginAbbr] [nvarchar](10) NULL,
	[iPwdPolicyId] [int] NULL,
	[sPassword] [varchar](255) NOT NULL,
	[sUserName] [nvarchar](100) NULL,
	[sEmail] [varchar](60) NULL,
	[sPhone] [varchar](20) NULL,
	[sMobile] [varchar](20) NULL,
	[binImage] [varbinary](max) NULL,
	[iGroupId] [int] NULL,
	[sSecurityQuestion] [nvarchar](255) NULL,
	[sSecurityAnswer] [nvarchar](255) NULL,
	[iUserType] [tinyint] NULL,
	[iLinkId] [int] NULL,
	[sDomainName] [nvarchar](50) NULL,
	[sDomainUser] [nvarchar](40) NULL,
	[bAccountDisabled] [bit] NULL,
	[bSendEmailNotification] [bit] NULL,
	[bAllowMultipleLogin] [bit] NULL,
	[bEmailonLoginFailure] [bit] NULL,
	[bEmailUseronLoginSuccess] [bit] NULL,
	[bDontLockAccount] [bit] NULL,
	[iStatus] [tinyint] NULL,
	[iNumInvalidAttempts] [tinyint] NULL,
	[iLockedTill] [bigint] NULL,
	[iDays] [int] NULL,
	[iBlockFromDate] [bigint] NULL,
	[iBlockToDate] [bigint] NULL,
	[iTimeRestrictionStartDate] [bigint] NULL,
	[iTimeRestrictionEndDate] [bigint] NULL,
	[iTimeRestrictionStartTime] [int] NULL,
	[iTimeRestrictionEndTime] [int] NULL,
	[iLocation] [int] NULL,
	[iLanguage] [int] NULL,
	[iAltLanguage] [int] NULL,
	[iTimeZone] [int] NULL,
	[iUserAccess] [smallint] NULL,
	[iPWDChangeDate] [int] NULL,
	[fVal0] [numeric](18, 7) NULL,
	[fVal1] [numeric](18, 7) NULL,
	[fVal2] [numeric](18, 7) NULL,
	[fVal3] [numeric](18, 7) NULL,
	[fVal4] [numeric](18, 7) NULL,
	[iCreatedBy] [int] NULL,
	[iModifiedBy] [int] NULL,
	[iCreatedDate] [bigint] NULL,
	[iModifiedDate] [bigint] NULL,
	[iCreatedTime] [int] NULL,
	[iModifiedTime] [int] NULL,
	[bModifiedDiffLoc] [bit] NULL,
	[iSyncReceivedDate] [bigint] NULL,
	[iEditingLocation] [int] NULL,
	[iLocationId] [int] NULL,
	[sAuthenticationCode] [varchar](16) NULL,
	[biSignature] [varbinary](max) NULL,
	[sEmailPwd] [varchar](200) NULL,
	[bEmailAuthPermission] [bit] NULL,
	[sMacAddress] [varchar](24) NULL,
 CONSTRAINT [pk_mSec_Users] PRIMARY KEY CLUSTERED
(
	[iUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY],
UNIQUE NONCLUSTERED
(
	[sLoginName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[mSec_Users]  WITH CHECK ADD  CONSTRAINT [fk_iAltLanguage_mSec_Users] FOREIGN KEY([iAltLanguage])
REFERENCES [dbo].[cCore_LanguageSupported] ([iLanguageId])
GO
ALTER TABLE [dbo].[mSec_Users] CHECK CONSTRAINT [fk_iAltLanguage_mSec_Users]
GO
ALTER TABLE [dbo].[mSec_Users]  WITH CHECK ADD  CONSTRAINT [fk_iGroupId_mSec_Users] FOREIGN KEY([iGroupId])
REFERENCES [dbo].[mSec_Users] ([iUserId])
GO
ALTER TABLE [dbo].[mSec_Users] CHECK CONSTRAINT [fk_iGroupId_mSec_Users]
GO
ALTER TABLE [dbo].[mSec_Users]  WITH CHECK ADD  CONSTRAINT [fk_iLanguage_mSec_Users] FOREIGN KEY([iLanguage])
REFERENCES [dbo].[cCore_LanguageSupported] ([iLanguageId])
GO
ALTER TABLE [dbo].[mSec_Users] CHECK CONSTRAINT [fk_iLanguage_mSec_Users]
GO
ALTER TABLE [dbo].[mSec_Users]  WITH CHECK ADD  CONSTRAINT [fk_iPwdPolicyId_mSec_Users] FOREIGN KEY([iPwdPolicyId])
REFERENCES [dbo].[mSec_PasswordPolicy] ([iPwdPolicyId])
GO
ALTER TABLE [dbo].[mSec_Users] CHECK CONSTRAINT [fk_iPwdPolicyId_mSec_Users]
GO
ALTER TABLE [dbo].[mSec_Users]  WITH CHECK ADD  CONSTRAINT [fk_iTimeZone_mSec_Users] FOREIGN KEY([iTimeZone])
REFERENCES [dbo].[mCore_TimeZone] ([iTimeZoneId])
GO
ALTER TABLE [dbo].[mSec_Users] CHECK CONSTRAINT [fk_iTimeZone_mSec_Users]
GO
