USE [Focus8040]
GO
/****** Object:  Table [dbo].[tCore_Data3340_0]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCore_Data3340_0](
	[iBodyId] [int] NOT NULL,
	[Description] [nvarchar](60) NULL,
	[sRemarks] [nvarchar](100) NULL,
	[EDD] [int] NULL,
	[sBRSUser] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
