USE [Focus8040]
GO
/****** Object:  Table [dbo].[mCore_SalesRefernce]    Script Date: 19-04-2023 9:27:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mCore_SalesRefernce](
	[iMasterId] [int] IDENTITY(0,1) NOT NULL,
	[sName] [nvarchar](100) NOT NULL,
	[sCode] [nvarchar](100) NULL,
	[bGroup] [bit] NULL,
	[iStatus] [tinyint] NULL,
	[bDoNotRestrict] [bit] NULL,
	[iCreatedBy] [int] NULL,
	[iModifiedBy] [int] NULL,
	[iCreatedDate] [bigint] NULL,
	[iModifiedDate] [bigint] NULL,
	[iLocationId] [int] NULL,
	[bEditedFrom] [bit] NULL,
	[iClosingDate] [int] NULL,
	[bAllowOtherCompaniesToViewRecord] [bit] NULL,
	[iAuthStatus] [tinyint] NULL,
	[iEditingLocation] [int] NULL,
	[iSyncReceivedDate] [bigint] NULL,
 CONSTRAINT [pk_mCore_SalesRefernce] PRIMARY KEY CLUSTERED
(
	[iMasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[mCore_SalesRefernce] ADD  DEFAULT ((0)) FOR [iAuthStatus]
GO
ALTER TABLE [dbo].[mCore_SalesRefernce]  WITH CHECK ADD  CONSTRAINT [fk_iCreatedBy_mCore_SalesRefernce] FOREIGN KEY([iCreatedBy])
REFERENCES [dbo].[mSec_Users] ([iUserId])
GO
ALTER TABLE [dbo].[mCore_SalesRefernce] CHECK CONSTRAINT [fk_iCreatedBy_mCore_SalesRefernce]
GO
ALTER TABLE [dbo].[mCore_SalesRefernce]  WITH CHECK ADD  CONSTRAINT [fk_iModifiedBy_mCore_SalesRefernce] FOREIGN KEY([iModifiedBy])
REFERENCES [dbo].[mSec_Users] ([iUserId])
GO
ALTER TABLE [dbo].[mCore_SalesRefernce] CHECK CONSTRAINT [fk_iModifiedBy_mCore_SalesRefernce]
GO
