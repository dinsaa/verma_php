<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Throwable;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $data = User::query()
            ->filter($request->input())
            ->latest()
            ->paginate(10);

        return view('user', ['data' => $data]);
    }

    public function create()
    {
        $title = 'Add User';
        $type = 'add';
        $branch = MasterBranch::query()->orderBy('inv_prefix')->get()->toArray();

        return view('user.modal', ['title' => $title, 'type' => $type, 'branch' => $branch])->render();
    }

    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => ['required'],
                'email' => ['required'],
                'password' => ['required', 'confirmed'],
                'designation' => ['required'],
                'voucher_series' => ['required']
            ]);

            $checkExist = User::whereEmail($validated['email'])->exists();
            throw_if($checkExist, 'User already exists with this Username');

            $validated['password'] = Hash::make($validated['password']);
            $user = User::create($validated);

            return $this->sendSuccess($user);
        } catch (Throwable $th) {
            return $this->sendError($th);
        }
    }

    public function edit(Request $request)
    {
        $title = 'Edit User';
        $type = 'edit';
        $data = User::query()->where('id', $request->id)->first()->toArray();
        $branch = MasterBranch::query()->orderBy('inv_prefix')->get()->toArray();

        return view('user.modal', ['title' => $title, 'type' => $type, 'branch' => $branch, 'data' => $data])->render();
    }

    public function update(Request $request)
    {
        try {
            $validated = $request->validate([
                'id' => ['required'],
                'name' => ['required'],
                'email' => ['required'],
                'password' => ['sometimes', 'confirmed'],
                'designation' => ['required'],
                'voucher_series' => ['sometimes']
            ]);

            $checkExist = User::whereEmail($validated['email'])->where('id', '!=', $validated['id'])->exists();
            throw_if($checkExist, 'Another User already exists with this Username');

            $checkUpdate = User::whereId($validated['id'])->first();
            throw_if(!$checkUpdate, 'User does not exists');
            throw_if($checkUpdate->status == 0, 'User is not in active state');

            $validated = array_filter($validated);
            if (isset($validated['password'])) {
                $validated['password'] = Hash::make($validated['password']);
            }
            $user = User::query()->where('id', $validated['id'])->first();
            $user->update($validated);

            return $this->sendSuccess($user);
        } catch (Throwable $th) {
            return $this->sendError($th);
        }
    }

    public function destroy(Request $request)
    {
        try {
            $validated = $request->validate([
                'id' => ['required'],
            ]);

            $user = User::query()->where('id', $validated['id'])->first();
            $user->update(['status' => !$user->status]);

            return $this->sendSuccess(null);
        } catch (Throwable $th) {
            return $this->sendError($th);
        }
    }

    private function sendSuccess($data)
    {
        return response()->json([
            'status' => true,
            'message' => 'Success',
            'data' => $data
        ]);
    }

    private function sendError($th)
    {
        return response()->json([
            'status' => false,
            'message' => ($th instanceof ValidationException)
                ? $th->validator->errors()->first()
                : $th->getMessage()
        ]);
    }
}
