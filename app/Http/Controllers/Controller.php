<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function efrisCall($content, $interfaceCode)
    {
        date_default_timezone_set("Africa/Kampala");

        $request = [
            "data" => [
                "content" => is_null($content) ? "" : base64_encode(json_encode($content)),
                "signature" => "",
                "dataDescription" => [
                    "codeType" => "0",
                    "encryptCode" => "1",
                    "zipCode" => "0",
                ],
            ],
            "globalInfo" => [
                "appId" => "AP04",
                "version" => "1.1.20191201",
                "dataExchangeId" => "9230489223014123",
                "interfaceCode" => $interfaceCode,
                "requestCode" => "TP",
                "requestTime" => date('Y-m-d H:i:s'),
                "responseCode" => "TA",
                "userName" => "admin",
                "deviceMAC" => "FFFFFFFFFFFF",
                "deviceNo" => env('DEVICE_NUMBER'),
                "tin" => env('TIN_NUMBER'),
                "brn" => "",
                "taxpayerID" => env('TAXPAYER_ID'),
                "longitude" => "116.397128",
                "latitude" => "39.916527",
                "extendField" => [
                    "responseDateFormat" => "dd/MM/yyyy",
                    "responseTimeFormat" => "dd/MM/yyyy HH:mm:ss",
                    "referenceNo" => "",
                ],
            ],
            "returnStateInfo" => [
                "returnCode" => "",
                "returnMessage" => "",
            ],
        ];

        $ch = curl_init(env('ENABLER_IP_2') . "/efristcs/ws/tcsapp/getInformation");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($request)),
        ]);

        return json_decode(curl_exec($ch), true);
    }

    public function efrisDecode($data)
    {
        if (isset($data['data']['content']) && !empty($data['data']['content'])) {
            $content = json_decode(base64_decode($data['data']['content']), true);
            if (isset($data['globalInfo']['extendField']['referenceNo'])) {
                $content['referenceNo'] = $data['globalInfo']['extendField']['referenceNo'];
            }

            return [
                'status' => true,
                'message' => $data['returnStateInfo']['returnMessage'],
                'data' => $content,
            ];
        }

        return [
            'status' => false,
            'message' => $data['returnStateInfo']['returnMessage'],
            'data' => [],
        ];
    }
}
