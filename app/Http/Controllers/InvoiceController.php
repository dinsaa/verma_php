<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\MasterItem;
use App\Models\SyncCron;
use App\Models\SyncStatus;
use App\Models\Transaction;
use App\Models\InvoiceDetails;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Throwable;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        $data = SyncStatus::query()->filter($request->input())->latest()->paginate(10);
        return view('invoice', ['data' => $data]);
    }

    public function upload(Request $request)
    {
        set_time_limit(0);

        try {
            throw_if(Cache::get('UPLOADING', 0) == 1, 'Upload Skipped');

            Cache::put('UPLOADING', 1, 120);

            try {
                dump('Upload Started');

                $invoices = $this->getInvoice($request);

                foreach ($invoices as $key => $invoice) {
                    if ($key < env('INVOICE_LIMIT', 5)) {
                        dump("Sync for iHeaderId " . $invoice["sellerDetails"]["REFERENCE"]);
                        dump($invoice);

                        $stockAdjusted = $this->setStockAdjustment($invoice);

                        if ($stockAdjusted) {
                            $res = Http::withHeaders([
                                'X-Request-For' => 'Invoice'
                            ])->post(env('API_URL') . '/verma', [
                                'json_data' => [$invoice]
                            ]);

                            $resArray = $res->json('data.0');
                            dump($resArray);

                            if ($request->has('vch_no')) {
                                SyncStatus::query()
                                    ->where('req_key', $invoice["sellerDetails"]["REFERENCE"])
                                    ->update(['status' => 3]);
                            }

                            SyncStatus::create([
                                'req_type' => 'Invoice',
                                'req_key' => $invoice["sellerDetails"]["REFERENCE"],
                                'req_body' => json_encode($invoice),
                                'res_body' => json_encode($resArray),
                                'status' => (isset($resArray['qr_code']) && $resArray['qr_code'] != '') ? 1 : 2,
                            ]);

                            if (isset($resArray['qr_code']) && $resArray['qr_code'] != '') {
                                SyncCron::create([
                                    'vch_dt' => strtotime($invoice["basicInformation"]["issuedDate"] !== false)
                                        ? date('Y-m-d', strtotime($invoice["basicInformation"]["issuedDate"]))
                                        : ((strtotime(env('SYNC_DATE')) !== false)
                                            ? date('Y-m-d', strtotime(env('SYNC_DATE')))
                                            : date('Y-m-d')),
                                    'vch_no' => $invoice["sellerDetails"]["referenceNo"],
                                    'status' => 1
                                ]);

                                $focusArray = Http::post(env('FOCUS_URL'), [
                                    'Invoice_No' => $invoice['sellerDetails']['referenceNo'],
                                    'FDN' => $resArray['invoiceNo'],
                                    'Antifake_Code' => $resArray['antifakeCode'],
                                    'QR_Code' => $resArray['qr_code']
                                ])->json();
                                dump($focusArray);

                                try {
                                    if ($invoice['buyerDetails']['buyerTin'] != '') {
                                        $res = Http::withHeaders([
                                            'X-Request-For' => 'Check_TIN'
                                        ])->post(env('API_URL') . '/verma', [
                                            'tin_number' => $invoice['buyerDetails']['buyerTin']
                                        ])->json();
                                        $taxpayer = $res['data']['error_code'] == 200 ? $res['data']['taxpayer'] : null;
                                    } else {
                                        $taxpayer = null;
                                    }

                                    QrCode::size(90)->generate($resArray['qr_code'], public_path() . '/img/' . $invoice["sellerDetails"]["REFERENCE"] . '.jpg');

                                    Pdf::setOptions([
                                        'isHtml5ParserEnabled' => true,
                                        'isRemoteEnabled' => true,
                                        'chroot' => public_path()
                                    ])->loadView($this->getInvoiceName($invoice['sellerDetails']['referenceNo']), [
                                        'data' => $invoice,
                                        'result' => $resArray,
                                        'taxpayer' => $taxpayer,
                                        'qrcode' => public_path() . '/img/' . $invoice["sellerDetails"]["REFERENCE"] . '.jpg'
                                    ])->save(Storage::path('public') . '/' . $invoice["sellerDetails"]["REFERENCE"] . '.pdf');

//                            Mail::raw('Invoice Upload Test Mail', function ($message) use ($resArray, $invoice) {
//                                $message->from('admin@cabbagesoft.com', 'Cabbagesoft Admin');
//                                $message->to('info@cabbagesoft.com');
//                                $message->subject('Invoice Uploaded');
//                                $message->attach($resArray['pdf']);
//                                $message->attach(Storage::path('public') . '/' . $invoice["sellerDetails"]["REFERENCE"] . '.pdf');
//                            });
                                } catch (Throwable $th) {
//                                Log::error($th);
                                    dump($th->getMessage());
                                }
                            } else {
                                SyncCron::create([
                                    'vch_dt' => strtotime($invoice["basicInformation"]["issuedDate"] !== false)
                                        ? date('Y-m-d', strtotime($invoice["basicInformation"]["issuedDate"]))
                                        : ((strtotime(env('SYNC_DATE')) !== false)
                                            ? date('Y-m-d', strtotime(env('SYNC_DATE')))
                                            : date('Y-m-d')),
                                    'vch_no' => $invoice["sellerDetails"]["referenceNo"],
                                    'status' => 2
                                ]);

                                $focusArray = Http::post(env('FOCUS_URL'), [
                                    'Invoice_No' => $invoice['sellerDetails']['referenceNo'],
                                    'FDN' => '',
                                    'Antifake_Code' => '',
                                    'QR_Code' => ''
                                ])->json();
                                dump($focusArray);
                            }
                        }
                    }
                }

                dump('Upload Completed');
            } catch (Throwable $th) {
                Log::error($th);
                dump($th->getMessage());
                dump('Upload Failed');
            }

            Cache::put('UPLOADING', 0, 120);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            dump($th->getMessage());
        }
    }

    private function getInvoice($request)
    {
        $sDate = (strtotime(env('SYNC_DATE')) !== false)
            ? date('Y-m-d', strtotime(env('SYNC_DATE')))
            : date('Y-m-d');
        $limit = env('QUERY_LIMIT', 100);
        $sql1 = "SELECT d.[iTransactionId]
				,Convert(date, convert(varchar, (iDate % 256)) + '/' +
                     convert(varchar, (iDate / 256 % 256)) + '/' +
                     convert(varchar, (iDate / 65536)), 103) as dDate
				,h.[sVoucherNo],h.[iVoucherType],d.[iHeaderId],d.[iBodyId]
				,u.[sUserName],d.[iBookNo],cs.[sName]
				,i.[iProduct],i.[iUnit],i.[fQuantity],i.[mRate],d.[mAmount2]
				,i.[mGross],d.[mAmount1],d.[mAmount3],d.[mOriginalAmount]
				,s.[mInput0],s.[mVal0],s.[mInput1],s.[mVal1]
				,s.[mInput2],s.[mVal2],s.[mInput3],s.[mVal3]
				,s.[mInput4],s.[mVal4],s.[mInput5],s.[mVal5]
				,s.[mInput6],s.[mVal6],s.[mInput7],s.[mVal7]
				,s.[mInput8],s.[mVal8],s.[mInput9],s.[mVal9]
				,CASE h.iVoucherType
					WHEN 3332 THEN D3332.[Description]
					WHEN 3333 THEN D3333.[Description]
					WHEN 3334 THEN D3334.[Description]
					WHEN 3335 THEN D3335.[Description]
					WHEN 3337 THEN D3337.[Description]
					WHEN 3338 THEN D3338.[Description]
					WHEN 3339 THEN D3339.[Description]
					WHEN 3340 THEN D3340.[Description]
				END as Description
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[sNarration]
					WHEN 3333 THEN H3333.[sNarration]
					WHEN 3334 THEN H3334.[sNarration]
					WHEN 3335 THEN H3335.[sNarration]
					WHEN 3337 THEN H3337.[sNarration]
					WHEN 3338 THEN H3338.[sNarration]
					WHEN 3339 THEN H3339.[sNarration]
					WHEN 3340 THEN H3340.[sNarration]
				END as sNarration
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Delivery_Terms]
					WHEN 3333 THEN H3333.[Delivery_Terms]
					WHEN 3334 THEN H3334.[Delivery_Terms]
					WHEN 3335 THEN H3335.[Delivery_Terms]
					WHEN 3337 THEN H3337.[Delivery_Terms]
					WHEN 3338 THEN H3338.[Delivery_Terms]
					WHEN 3339 THEN H3339.[Delivery_Terms]
				END as Delivery_Terms
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Delivery_At]
					WHEN 3333 THEN H3333.[Delivery_At]
					WHEN 3334 THEN H3334.[Delivery_At]
					WHEN 3335 THEN H3335.[Delivery_At]
					WHEN 3337 THEN H3337.[Delivery_At]
					WHEN 3338 THEN H3338.[Delivery_At]
					WHEN 3339 THEN H3339.[Delivery_At]
				END as Delivery_At
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Payment_Terms]
					WHEN 3333 THEN H3333.[Payment_Terms]
					WHEN 3334 THEN H3334.[Payment_Terms]
					WHEN 3335 THEN H3335.[Payment_Terms]
					WHEN 3337 THEN H3337.[Payment_Terms]
					WHEN 3338 THEN H3338.[Payment_Terms]
					WHEN 3339 THEN H3339.[Payment_Terms]
				END as Payment_Terms
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[PO_No]
					WHEN 3333 THEN H3333.[PO_No]
					WHEN 3334 THEN H3334.[PO_No]
					WHEN 3335 THEN H3335.[PO_No]
					WHEN 3337 THEN H3337.[PO_No]
					WHEN 3338 THEN H3338.[PO_No]
					WHEN 3339 THEN H3339.[PO_No]
				END as PO_No
				,CASE h.iVoucherType
					WHEN 3332 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3332.[PO_Date]), 105)
					WHEN 3333 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3333.[PO_Date]), 105)
					WHEN 3334 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3334.[PO_Date]), 105)
					WHEN 3335 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3335.[PO_Date]), 105)
					WHEN 3337 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3337.[PO_Date]), 105)
					WHEN 3338 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3338.[PO_Date]), 105)
					WHEN 3339 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H3339.[PO_Date]), 105)
				END as PO_Date
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Receiver_Name]
					WHEN 3333 THEN H3333.[Receiver_Name]
					WHEN 3334 THEN H3334.[Receiver_Name]
					WHEN 3335 THEN H3335.[Receiver_Name]
					WHEN 3337 THEN H3337.[Receiver_Name]
					WHEN 3338 THEN H3338.[Receiver_Name]
					WHEN 3339 THEN H3339.[Receiver_Name]
				END as Receiver_Name
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Receiver_Mobile_No]
					WHEN 3333 THEN H3333.[Receiver_Mobile_No]
					WHEN 3334 THEN H3334.[Receiver_Mobile_No]
					WHEN 3335 THEN H3335.[Receiver_Mobile_No]
					WHEN 3337 THEN H3337.[Receiver_Mobile_No]
					WHEN 3338 THEN H3338.[Receiver_Mobile_No]
					WHEN 3339 THEN H3339.[Receiver_Mobile_No]
				END as Receiver_Mobile_No
				,CASE h.iVoucherType
					WHEN 3332 THEN H3332.[Available_Points]
					WHEN 3333 THEN H3333.[Available_Points]
					WHEN 3334 THEN H3334.[Available_Points]
					WHEN 3335 THEN H3335.[Available_Points]
					WHEN 3337 THEN H3337.[Available_Points]
					WHEN 3339 THEN H3339.[Available_Points]
				END as Available_Points
				,f.[mInput1],f.[mVal1],
				cc.[sName] as CostCenter,
				CASE h.iVoucherType
					WHEN 3332 THEN H5635.[PO_No]
					WHEN 3333 THEN H5637.[PO_No]
					WHEN 3335 THEN H5639.[PO_No]
					WHEN 3337 THEN H5643.[PO_No]
				END as SalesOrderNo,
				vm.[sName] as VermaMember,sm.[sName] as SalesMan,
				sf.[sName] as SalesRefernce,mc.[sName] as MechanicName,
				st.[sCode] as ServiceType,
				CASE h.iVoucherType WHEN 3337 THEN H3337.[Time_In]
				END as TimeIn,
				CASE h.iVoucherType WHEN 3337 THEN CONVERT(VARCHAR(10), [dbo].[IntToDate](H5643.[Est_Delivery_Date]), 105)
				END as EstDeliveryDate,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[Est_Delivery_Time]
				END as EstDeliveryTime,
				v.[sName] as ChassisNo,v.[sCode] as RegistrationNo,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[Sold_To]
				END as Soldto,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[Current_KM]
				END as CurrentKm,
				CASE h.iVoucherType WHEN 3337 THEN H5643.[PO_No]
				END as JobCardNo,
				CONVERT(VARCHAR(10), [dbo].[IntToDate](iDate), 105) as Vdate
			FROM [Focus8040].[dbo].[tCore_Data_0] as d
				LEFT JOIN [Focus8040].[dbo].[tCore_Header_0] as h  on h.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3332_0] as H3332 on H3332.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3333_0] as H3333 on H3333.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3334_0] as H3334 on H3334.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3335_0] as H3335 on H3335.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3337_0] as H3337 on H3337.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3338_0] as H3338 on H3338.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3339_0] as H3339 on H3339.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData3340_0] as H3340 on H3340.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Indta_0] as i  on i.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[vCore_Account] as cs  ON cs.iMasterId = d.iBookNo
				LEFT JOIN [Focus8040].[dbo].[mSec_Users] as u ON u.iUserId=h.iUserId
				LEFT JOIN [Focus8040].[dbo].[tCore_IndtaBodyScreenData_0] as s ON s.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3332_0] as D3332 ON D3332.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3333_0] as D3333 ON D3333.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3334_0] as D3334 ON D3334.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3335_0] as D3335 ON D3335.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3337_0] as D3337 ON D3337.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3338_0] as D3338 ON D3338.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3339_0] as D3339 ON D3339.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data3340_0] as D3340 ON D3340.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[tCore_IndtaFooterScreenData_0] as f ON f.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_Data_Tags_0] as t  ON t.[iBodyId] = d.[iBodyId]
				LEFT JOIN [Focus8040].[dbo].[mCore_CostCenter] as cc on cc.[iMasterId] = t.[iTag5]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5635_0] as H5635 on H5635.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5637_0] as H5637 on H5637.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5639_0] as H5639 on H5639.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[tCore_HeaderData5643_0] as H5643 on H5643.[iHeaderId] = d.[iHeaderId]
				LEFT JOIN [Focus8040].[dbo].[mCore_VermaMember] as vm on vm.[iMasterId]=t.[iTag3019]
				LEFT JOIN [Focus8040].[dbo].[mCore_SalesMan] as sm on sm.[iMasterId]=t.[iTag3007]
				LEFT JOIN [Focus8040].[dbo].[mCore_SalesRefernce] as sf on sf.[iMasterId]=t.[iTag3021]
				LEFT JOIN [Focus8040].[dbo].[mCore_Mechanic] as mc on mc.[iMasterId]=t.[iTag3025]
				LEFT JOIN [Focus8040].[dbo].[mCore_ServiceType] as st on st.[iMasterId]=t.[iTag3026]
				LEFT JOIN [Focus8040].[dbo].[mCore_Vehicle] as v on v.[iMasterId] = t.[iTag3002]
			where i.[iBodyId] = d.[iBodyId]
				and h.iVoucherType in (3332,3333,3334,3335,3337,3339,3340)";
        if ($request->has('vch_no')) {
            $sql1 .= " and h.sVoucherNo = '" . $request->query('vch_no') . "'";
        } else {
            $existVoucher = SyncCron::query()
                ->where('vch_dt', $sDate)
                ->pluck('vch_no')
                ->implode("', '");
            if (!empty($existVoucher)) {
                $sql1 .= " and h.sVoucherNo not in ('" . $existVoucher . "')";
            }
            $sql1 .= " and Convert(date, convert(varchar, (iDate % 256)) + '/' + convert(varchar, (iDate / 256 % 256)) + '/' +  convert(varchar, (iDate / 65536)), 103)='" . $sDate . "'";
        }
        $sql1 .= " order by cs.sName desc";
        $sql1 .= " OFFSET 0 ROWS FETCH FIRST " . $limit . " ROWS ONLY";

        Transaction::truncate();
        InvoiceDetails::truncate();

        $result1 = DB::select($sql1);
        if ($request->has('vch_no')) {
            $existVoucher = SyncCron::query()
                ->where('vch_no', $request->has('vch_no'))
                ->where('status', '1')
                ->first();
            if (isset($existVoucher))
                $result1 = [];
        }

        $uniqueHeaderId = [];
        foreach ($result1 as $result) {
            if (!in_array($result->iHeaderId, $uniqueHeaderId)) {
                $uniqueHeaderId[] = $result->iHeaderId;
            }
        }
        for ($i = 0; $i < count($result1); $i++) {
            $data = array_values((array)$result1[$i]);
            $ID = $data[0];
            $DATE = str_replace("-", "", $data[1]);
            $VOUCHERNUMBER = $data[2];
            $VOUCHERTYPENAME = $data[3];
            $REFERENCE = $data[4];
            $GUID = $data[5];
            $ENTEREDBY = $data[6];
            $PARTYNAME = $data[7];
            $PARTYLEDGERNAME = $data[8];
            $STOCKITEMNAME = $data[9];
            $UNIT = $data[10];

            $QTY = round(($data[11]), 2);
            if ($QTY < 0) {
                $QTY = $QTY * -1;
            } elseif ($QTY == 0) {
                $QTY = 1;
            }
            $RATEVALUE = $data[12];
            if ($RATEVALUE == 0) {
                $RATEVALUE = $data[15];
            }
            $RATEVALUE = round($RATEVALUE, 2);
            $AMOUNT = $data[14];
            if ($AMOUNT == 0) {
                $AMOUNT = $data[15];
            } elseif ($AMOUNT < 0) {
                $AMOUNT = $AMOUNT * -1;
            }
            $AMOUNT = round($AMOUNT, 2);
            $DiscountPer = 0;
            $Discount = 0;
            if ($data[18] > 0 && $data[18] <= 100) {
                $DiscountPer = $data[18];
                $Discount = $AMOUNT * ($DiscountPer / 100);
            } else if ($data[18] > 100) {
                $Discount = $data[18];
                if ($AMOUNT == 0) {
                    $DiscountPer = 100.00;
                } else {
                    $DiscountPer = round(($Discount / $AMOUNT) * 100, 2);
                }
            }
            $VATTax = $data[23];
            $VAT = round(floatval($VATTax), 2);
            if ($VAT < 0) {
                $VAT = $VAT * -1;
            }
            $Sales = $data[14];
            $NetSales = $data[21];
            $RegistrationFee = $data[25];
            $NPTransferFee = $data[27];
            $StampDuty = $data[29];
            $LoyalityAddition = $data[31];
            $RiderAdvTax = floatval($data[37]);

            $Description = $data[38];
            $NARRATION = $data[39];
            $Delivery_Terms = $data[40];
            $Delivery_At = $data[41];
            $Payment_Terms = $data[42];
            $PO_No = $data[43];
            $PO_Date = $data[44];
            $Receiver_Name = $data[45];
            $Receiver_Mobile_No = $data[46];
            $Available_Points = $data[47];
            $CostCenter = $data[48];
            $SalesOrderNo = $data[49];
            $VermaMember = $data[50];
            $SalesMan = $data[51];
            $SalesRefernce = $data[52];
            $MechanicName = $data[53];
            $ServiceType = $data[54];
            $TimeIn = $data[55];
            $EstDeliveryDate = str_replace("-", "", $data[56]);
            $EstDeliveryTime = $data[57];
            $ChassisNo = $data[58];
            $RegistrationNo = $data[59];
            $Soldto = $data[60];
            $CurrentKm = $data[61];
            $JobCardNo = $data[62];

            if ((count($result1) < $limit) || (count($uniqueHeaderId) > 1 && $uniqueHeaderId[count($uniqueHeaderId) - 1] != $REFERENCE)) {
                Transaction::insert([
                    'ID' => $ID,
                    'DATE' => $DATE,
                    'VOUCHERNUMBER' => $VOUCHERNUMBER,
                    'VOUCHERTYPENAME' => $VOUCHERTYPENAME,
                    'REFERENCE' => $REFERENCE,
                    'ENTEREDBY' => $ENTEREDBY,
                    'PARTYNAME' => $PARTYNAME,
                    'PARTYLEDGERNAME' => $PARTYLEDGERNAME,
                    'STOCKITEMNAME' => $STOCKITEMNAME,
                    'RATEVALUE' => $RATEVALUE,
                    'QTY' => $QTY,
                    'UNIT' => $UNIT,
                    'VAT' => $VAT,
                    'AMOUNT' => $AMOUNT,
                    'Sales' => $RegistrationFee,
                    'SalesTyres' => $NPTransferFee,
                    'SalesExempted' => $StampDuty,
                    'SalesZeroRate' => floatval($LoyalityAddition),
                    'SalesExport' => $RiderAdvTax,
                    'DISCOUNT' => $Discount,
                    'ADDLCOSTPERC' => $DiscountPer,
                    'GUID' => $GUID,
                    'PRICELEVEL' => $Description,
                    'NARRATION' => $NARRATION,
                    'BASICORDERREF' => $Delivery_Terms,
                    'MFDON' => $Delivery_At,
                    'BASICORDERTERMS' => $Payment_Terms,
                    'BASICPURCHASEORDERNO' => $PO_No,
                    'BASICORDERDATE' => $PO_Date,
                    'EXPIRYPERIOD' => $Receiver_Name,
                    'GODOWNNAME' => $Receiver_Mobile_No,
                    'BATCHNAME' => $Available_Points,
                ]);

                InvoiceDetails::insert([
                    'ID' => $ID,
                    'CostCenter' => $CostCenter,
                    'SalesOrderNo' => $SalesOrderNo,
                    'VermaMember' => $VermaMember,
                    'SalesMan' => $SalesMan,
                    'SalesRefernce' => $SalesRefernce,
                    'MechanicName' => $MechanicName,
                    'ServiceType' => $ServiceType,
                    'TimeIn' => intval($TimeIn),
                    'EstDeliveryDate' => $EstDeliveryDate,
                    'EstDeliveryTime' => intval($EstDeliveryTime),
                    'ChassisNo' => $ChassisNo,
                    'RegistrationNo' => $RegistrationNo,
                    'Soldto' => intval($Soldto),
                    'CurrentKm' => intval($CurrentKm),
                    'JobCardNo' => $JobCardNo,
                ]);
            }
        }

        $sql2 = "Select DISTINCT t.VOUCHERNUMBER,t.DATE,t.VOUCHERTYPENAME,t.REFERENCE,t.ENTEREDBY,
				t.PARTYNAME,t.PARTYLEDGERNAME,p.VATTINNUMBER,p.Address1,
				p.Address2,p.Address3,p.Address4,p.LEDGERMOBILE,p.LEDGERPHONE,p.LEDGEREMAIL,
				e.invoiceno,
                (Select i3.STOCKITEMNAME from transactions as t2
                 	left join master_items as i3 on i3.id=t2.STOCKITEMNAME
                 		where t2.VOUCHERNUMBER=t.VOUCHERNUMBER
                 		and (i3.ura_commodityCategoryId=''
                        or i3.ura_commodityCategoryId is null) limit 1)
						as STOCKNAME,
				(Select count(id) from transactions as t0
                 		where t0.VOUCHERNUMBER=t.VOUCHERNUMBER
                 		and (select count(id) from master_items as i
						where i.id=t0.STOCKITEMNAME and (i.ura_commodityCategoryId=''
							or i.ura_commodityCategoryId is null)) > 0)
						as chkCommodityCode,
				(Select count(id) from transactions as t00
                 		where t00.VOUCHERNUMBER=t.VOUCHERNUMBER
                 		and (select count(id) from master_items as i2
						where i2.id=t00.STOCKITEMNAME and
								(i2.ura_goodsCode='' or i2.ura_goodsCode is null)) > 0)
						as chkGoodsCode,
                 (Select sum(amount) from transactions as t1
						where t1.VOUCHERNUMBER=t.VOUCHERNUMBER ) as TAmount,
                  (Select sum(vat) from transactions as t1
						where t1.VOUCHERNUMBER=t.VOUCHERNUMBER ) as TVat
				FROM transactions as t
                left join master_parties as p on p.ID=t.PARTYNAME
				left join efris_invoices as e on e.VOUCHERNUMBER=t.VOUCHERNUMBER";
        if (!$request->has('vch_no')) {
            $sql2 .= " where t.date='" . str_replace("-", "", $sDate) . "'";
        }
        $sql2 .= " ORDER BY DATE,t.VOUCHERNUMBER";

        $result2 = DB::connection('mysql')->select($sql2);

        if (count($result2) > 0) {
            for ($i = 0; $i < count($result2); $i++) {
                $bgcolor = "";
                $row = (array)$result2[$i];
                $invoiceno = $row["invoiceno"];
                $VOUCHERNUMBER = $row["VOUCHERNUMBER"];
                $chkCommodityCode = $row["chkCommodityCode"];
                $chkGoodsCode = $row["chkGoodsCode"];
                $TAmount = $row["TAmount"];
                if ($chkCommodityCode > 0) {
                    $bgcolor = "style='background: sienna;'";
                }
                if ($chkGoodsCode > 0) {
                    $bgcolor = "style='background: darkgoldenrod;'";
                }
                if ($invoiceno != "") {
                    $bgcolor = "style='background: darkgreen;'";
                }
                if (count(explode('/', $VOUCHERNUMBER)) != 3) {
                    $bgcolor = "style='background: teal;'";
                }
                if ($bgcolor == "" && $TAmount != 0) {
                    $sql3 = "Select t.ID,p.Trans,t.DATE,t.VOUCHERNUMBER,t.VOUCHERTYPENAME,
						t.REFERENCE,t.ENTEREDBY,t.PARTYNAME,t.PARTYLEDGERNAME,
						t.STOCKITEMNAME as StockCode,t.RATEVALUE,t.DISCOUNT,t.ADDLCOSTPERC,t.QTY,
						t.VAT,t.AMOUNT,t.SalesExempted,t.SalesZeroRate, t.Sales,t.SalesTyres, t.SalesExport,
						t.GUID,
						i.STOCKITEMNAME,i.STOCKITEMUOM,i.ura_goodsCode,i.ura_commodityCategoryId,i.ura_measureUnit,
						p.VATTINNUMBER,p.Address1,p.Address2,p.Address3,p.Address4,
						p.LEDGERMOBILE,p.LEDGERPHONE,p.LEDGEREMAIL,
						e.invoiceno
						FROM transactions as t
						left join master_items as i on i.id=t.STOCKITEMNAME
						left join master_parties as p on p.ID=t.PARTYNAME
						left join efris_invoices as e on e.VOUCHERNUMBER=t.VOUCHERNUMBER
						where t.VOUCHERNUMBER='" . $VOUCHERNUMBER . "'
						ORDER BY DATE, t.VOUCHERNUMBER";

                    $result3 = DB::connection('mysql')->select($sql3);
                    if (count($result3) > 0) {
                        $ps = -1;
                        $nrow = array();
                        for ($j = 0; $j < count($result3); $j++) {
                            $row1 = (array)$result3[$j];
                            $ps = $ps + 1;
                            $nrow[$ps] = $row1;
                        }
                        $extra_data = collect($result1)->where('sVoucherNo', $VOUCHERNUMBER)->first();
                        $param[] = $this->getParam($nrow, (array)$extra_data);
                    }
                }
            }
        }

        return $param ?? [];
    }

    private function getParam($lrow, $extra_data)
    {
        $param = array();
        $ncnt = -1;
        $TnetAmount = 0;
        $TVat = 0;
        $docty = "";
        $StampDuty = 0;
        $LoyalityAddition = 0;
        $RegistrationFee = 0;
        $NPTransferFee = 0;
        $RiderAdvTax = 0;
        $tcnt = 0;
        $taxDetails_netAmount_ex = 0;
        $taxDetails_grossAmount_ex = 0;
        $taxDetails_netAmount_std = 0;
        $taxDetails_taxAmount_std = 0;
        $taxDetails_grossAmount_std = 0;
        $TOTAL_DISCOUNT = 0;
        foreach ($lrow as $k => $row) {
            if ($k == 0) {
                $param["sellerDetails"]["REFERENCE"] = $row["REFERENCE"];
                $param["sellerDetails"]["DATE"] = $row["DATE"];
                $param["sellerDetails"]["tin"] = "1000041335";
                $param["sellerDetails"]["ninBrn"] = "";
                $param["sellerDetails"]["legalName"] = "Verma Co Ltd";
                $param["sellerDetails"]["businessName"] = "Verma Co Ltd";
                $param["sellerDetails"]["address"] = "Plot -46, Mirembe Business Center, Lugogo Bypass P.O. Box 33733, Kampala-Uganda";
                $param["sellerDetails"]["mobilePhone"] = "";
                $param["sellerDetails"]["linePhone"] = "+256 414 340 7771";
                $param["sellerDetails"]["emailAddress"] = "info@bajajverma.com";
                $param["sellerDetails"]["placeOfBusiness"] = "http://bajajverma.com";
                $param["sellerDetails"]["referenceNo"] = $row["VOUCHERNUMBER"];
                $adoc = explode("/", $row["VOUCHERNUMBER"]);
                $docty = $adoc[0];
                $doclc = substr($adoc[1], 0, 3);
                $param["sellerDetails"]["branchCode"] = $this->getBranchID($docty . "/" . $doclc, "Code");
                $param["sellerDetails"]["branchId"] = $this->getBranchID($docty . "/" . $doclc);

                $param["basicInformation"]["invoiceNo"] = $row["REFERENCE"];
                $param["basicInformation"]["antifakeCode"] = "";
                $param["basicInformation"]["deviceNo"] = "";
                $yr2 = substr($row["DATE"], 0, 4);
                $mth2 = substr($row["DATE"], 4, 2);
                $dt2 = substr($row["DATE"], 6, 2);
                $param["basicInformation"]["issuedDate"] = $yr2 . "-" . $mth2 . "-" . $dt2 . " 00:00:00";
                if ($row["ENTEREDBY"] == "") {
                    $param["basicInformation"]["operator"] = "Admin - sasbuz.com";
                } else {
                    $param["basicInformation"]["operator"] = $row["ENTEREDBY"] . " - sasbuz.com";
                }
                $param["basicInformation"]["currency"] = "UGX";
                $param["basicInformation"]["oriInvoiceI"] = "";
                $param["basicInformation"]["invoiceType"] = "1";
                $param["basicInformation"]["invoiceKind"] = "1";
                $param["basicInformation"]["dataSource"] = "103";
                if ($docty == "CSIB" || $docty == "SINV") {
                    $param["basicInformation"]["invoiceIndustryCode"] = "108";
                    $param["basicInformation"]["isBatch"] = "0";
                } else {
                    $param["basicInformation"]["invoiceIndustryCode"] = "";
                    $param["basicInformation"]["isBatch"] = "0";
                }
                $param["basicInformation"]["seller_ref"] = $row["VOUCHERNUMBER"];
                $param["basicInformation"]["tally_gdwnId"] = !empty($param['sellerDetails']['branchCode']) ? (int)$param['sellerDetails']['branchCode'] : "";

                if (strlen($row["VATTINNUMBER"]) >= 10 && strlen($row["VATTINNUMBER"]) < 20) {
                    $param["buyerDetails"]["buyerTin"] = $row["VATTINNUMBER"];
                } else {
                    $param["buyerDetails"]["buyerTin"] = "";
                }
                $param["buyerDetails"]["buyerNinBrn"] = "";
                $param["buyerDetails"]["buyerPassportNum"] = "";
                $param["buyerDetails"]["PARTYNAME"] = $row["PARTYNAME"];
                $param["buyerDetails"]["buyerLegalName"] = $row["PARTYLEDGERNAME"];
                $param["buyerDetails"]["buyerBusinessName"] = $row["PARTYLEDGERNAME"];
                $Address = $row["Address1"];
                if ($row["Address2"] != "" && $row["Address2"] != "0") {
                    if ($Address != "") {
                        $Address = ",";
                    }
                    $Address .= $row["Address2"];
                }
                if ($row["Address3"] != "") {
                    if ($Address != "") {
                        $Address = ",";
                    }
                    $Address .= $row["Address2"];
                }
                if ($row["Address4"] != "") {
                    if ($Address != "") {
                        $Address = ",";
                    }
                    $Address .= $row["Address4"];
                }
                $param["buyerDetails"]["buyerAddress"] = $Address;
                $param["buyerDetails"]["buyerEmail"] = $row["LEDGEREMAIL"];
                $param["buyerDetails"]["buyerMobilePhone"] = $row["LEDGERMOBILE"];
                $param["buyerDetails"]["buyerLinePhone"] = $row["LEDGERPHONE"];
                $param["buyerDetails"]["buyerPlaceOfBusi"] = "";
                $Trans = $row["Trans"];
                if ($Trans == 2 || $Trans == 3 || $Trans == 7 || $Trans == 8 || $Trans == 9) {
                    $param["buyerDetails"]["buyerType"] = "0";
                } else {
                    $param["buyerDetails"]["buyerType"] = "1";
                }
                $param["buyerDetails"]["buyerCitizenship"] = "";
                $param["buyerDetails"]["buyerSector"] = "";
                $param["buyerDetails"]["buyerReferenceNo"] = "";
            }

            if ($docty == "CSIB" || $docty == "SINV") {
                $tcnt = $row["QTY"];
                $StampDuty = $StampDuty + $row["SalesExempted"];
                $LoyalityAddition = $LoyalityAddition + $row["SalesZeroRate"];
                $RegistrationFee = $RegistrationFee + $row["Sales"];
                $NPTransferFee = $NPTransferFee + $row["SalesTyres"];
                $RiderAdvTax = $RiderAdvTax + $row["SalesExport"];
            }

            $maxCnt = 100;
            $DISCOUNT = 0;
            if ($ncnt <= $maxCnt && $row["AMOUNT"] != 0) {
                $ncnt = $ncnt + 1;
                $AMOUNT = $row["AMOUNT"];
                $VAT = $row["VAT"];
                $RATEVALUE = $row["RATEVALUE"];
                if ($row["DISCOUNT"] != 100) {
                    $DISCOUNT = $row["DISCOUNT"];
                    $TOTAL_DISCOUNT = $TOTAL_DISCOUNT + $row["DISCOUNT"];
                }
                $qty = $row["QTY"];
                if ($qty < 0) {
                    $qty = $qty * -1;
                }
                if ($row["RATEVALUE"] == 0) {
                    $row["RATEVALUE"] = $AMOUNT;
                }
                $RATEQty = $row["RATEVALUE"] * $qty;
                if ($row["STOCKITEMNAME"] == "Exempted Sales (Helmet)") {
                    $VAT = 0;
                } else {
                    $VAT = round(($RATEQty - $DISCOUNT) * .18, 2);
                    $RATEVALUE = round(($row["RATEVALUE"] + ($row["RATEVALUE"] * .18)), 2);
                }
                if ($DISCOUNT != 0) {
                    if ($row["STOCKITEMNAME"] == "Exempted Sales (Helmet)") {
                        $AMOUNT = round($RATEQty - $DISCOUNT, 2);
                    } else {
                        $AMOUNT = round($RATEQty - $DISCOUNT + (($RATEQty - $DISCOUNT) * .18), 2);
                    }
                    $RATEVALUE = round(($AMOUNT / $qty), 2);
                    $AMOUNT = round(($RATEVALUE * $qty), 2);
                } else {
                    $AMOUNT = round((($RATEVALUE * $qty)), 2);
                }
                if ($row["STOCKITEMNAME"] == "Exempted Sales (Helmet)") {
                    $VAT = 0;
                } else {
                    $VAT = round($AMOUNT - ($AMOUNT / 1.18), 2);
                }
                $param["goodsDetails"][$ncnt]["item"] = $row["STOCKITEMNAME"];
                $param["goodsDetails"][$ncnt]["itemCode"] = $row["ura_goodsCode"];
                $param["goodsDetails"][$ncnt]["qty"] = $qty;
                $param["goodsDetails"][$ncnt]["unitOfMeasure"] = $row["ura_measureUnit"];
                $param["goodsDetails"][$ncnt]["STOCKITEMUOM"] = $row["STOCKITEMUOM"] ?? 'PCS';
                if ($RATEVALUE == 0) {
                    $param["goodsDetails"][$ncnt]["unitPrice"] = number_format(1, 4, '.', '');
                    $param["goodsDetails"][$ncnt]["total"] = number_format($qty, 2, '.', '');
                    $TnetAmount = $TnetAmount + $qty;
                } else {
                    $param["goodsDetails"][$ncnt]["unitPrice"] = number_format($RATEVALUE, 4, ".", "");
                    $param["goodsDetails"][$ncnt]["total"] = number_format($AMOUNT, 2, '.', '');
                    $TnetAmount = $TnetAmount + $AMOUNT;
                }
                $TVat = $TVat + $VAT;
                $param["goodsDetails"][$ncnt]["taxRate"] = "";
                if ($VAT == 0) {
                    $param["goodsDetails"][$ncnt]["tax"] = "0";
                } else {
                    $param["goodsDetails"][$ncnt]["tax"] = $VAT;
                }
                $discountFlag = "2";
                if ($discountFlag == "2") {
                    $param["goodsDetails"][$ncnt]["discountTotal"] = "";
                } else {
                    $param["goodsDetails"][$ncnt]["discountTotal"] = $DISCOUNT * -1;
                }
                $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0;
                $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
                $param["goodsDetails"][$ncnt]["discountFlag"] = $discountFlag;
                $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
                $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
                $param["goodsDetails"][$ncnt]["categoryId"] = "";
                $param["goodsDetails"][$ncnt]["categoryName"] = "";
                $param["goodsDetails"][$ncnt]["goodsCategoryId"] = $row["ura_commodityCategoryId"];
                $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
                $param["goodsDetails"][$ncnt]["exciseRate"] = "";
                $param["goodsDetails"][$ncnt]["exciseRule"] = "";
                $param["goodsDetails"][$ncnt]["exciseTax"] = "";
                $param["goodsDetails"][$ncnt]["pack"] = "";
                $param["goodsDetails"][$ncnt]["stick"] = "";
                $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
                $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
                $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
                if ($VAT == 0) {
                    // $param["goodsDetails"][$ncnt]["taxRate"] = "-";
                    $param["goodsDetails"][$ncnt]["taxRate"] = "$";
                    $taxDetails_netAmount_ex = $taxDetails_netAmount_ex + $AMOUNT;
                    $taxDetails_taxAmount_ex = 0;
                    $taxDetails_grossAmount_ex = $taxDetails_grossAmount_ex + $AMOUNT;
                } else {
                    // $param["goodsDetails"][$ncnt]["taxRate"] = "0.18";
                    $param["goodsDetails"][$ncnt]["taxRate"] = "18";
                    $taxDetails_netAmount_std = $taxDetails_netAmount_std + ($AMOUNT - $VAT);
                    $taxDetails_taxAmount_std = $taxDetails_taxAmount_std + $VAT;
                    $taxDetails_grossAmount_std = $taxDetails_grossAmount_std + $AMOUNT;
                }
            }
        }

        $ExemptCharges = 0;
        if ($docty == "CSIB" || $docty == "SINV") {
            $ncnt2 = $ncnt;
            $ExemptCharges = $RegistrationFee + $NPTransferFee + $RiderAdvTax + $StampDuty;
            if ($ExemptCharges > 0) {
                $taxDetails_netAmount_ex = $taxDetails_netAmount_ex + $ExemptCharges;
                $taxDetails_taxAmount_ex = 0;
                $taxDetails_grossAmount_ex = $taxDetails_grossAmount_ex + $ExemptCharges;
            }
            /*$ncnt = $ncnt + 1;
            $param["goodsDetails"][$ncnt]["item"] = "Loyality Addition";
            $param["goodsDetails"][$ncnt]["itemCode"] = "LoyalityAddition";
            $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
            $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
            $param["goodsDetails"][$ncnt]["STOCKITEMUOM"] = $row["STOCKITEMUOM"] ?? 'PCS';
            $param["goodsDetails"][$ncnt]["unitPrice"] = $LoyalityAddition * -1;
            $param["goodsDetails"][$ncnt]["total"] = $LoyalityAddition * -1;
            $param["goodsDetails"][$ncnt]["taxRate"] = "";
            $param["goodsDetails"][$ncnt]["tax"] = "0";
            $param["goodsDetails"][$ncnt]["discountTotal"] = "";
            $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0;
            $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
            $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
            $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
            $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
            $param["goodsDetails"][$ncnt]["categoryId"] = "";
            $param["goodsDetails"][$ncnt]["categoryName"] = "";
            $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010101";
            $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
            $param["goodsDetails"][$ncnt]["exciseRate"] = "";
            $param["goodsDetails"][$ncnt]["exciseRule"] = "";
            $param["goodsDetails"][$ncnt]["exciseTax"] = "";
            $param["goodsDetails"][$ncnt]["pack"] = "";
            $param["goodsDetails"][$ncnt]["stick"] = "";
            $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
            $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
            $param["goodsDetails"][$ncnt]["exciseRateName"] = "";*/

            if ($RegistrationFee > 0) {
                $ncnt = $ncnt + 1;
                $param["goodsDetails"][$ncnt]["item"] = "Registration Fees";
                $param["goodsDetails"][$ncnt]["itemCode"] = "RegistrationFees";
                $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
                $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
                $param["goodsDetails"][$ncnt]["STOCKITEMUOM"] = $row["STOCKITEMUOM"] ?? 'PCS';
                $param["goodsDetails"][$ncnt]["unitPrice"] = number_format($RegistrationFee / $tcnt, 4, '.', '');
                $param["goodsDetails"][$ncnt]["total"] = number_format($RegistrationFee, 2, '.', '');
                $param["goodsDetails"][$ncnt]["taxRate"] = "0";
                $param["goodsDetails"][$ncnt]["tax"] = "0";
                $param["goodsDetails"][$ncnt]["discountTotal"] = "";
                $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0;
                $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
                $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
                $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
                $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
                $param["goodsDetails"][$ncnt]["categoryId"] = "";
                $param["goodsDetails"][$ncnt]["categoryName"] = "";
                $param["goodsDetails"][$ncnt]["vatApplicableFlag"] = "0";
                $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010102";
                $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
                $param["goodsDetails"][$ncnt]["exciseRate"] = "";
                $param["goodsDetails"][$ncnt]["exciseRule"] = "";
                $param["goodsDetails"][$ncnt]["exciseTax"] = "";
                $param["goodsDetails"][$ncnt]["pack"] = "";
                $param["goodsDetails"][$ncnt]["stick"] = "";
                $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
                $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
                $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
            }
            if ($NPTransferFee > 0) {
                $ncnt = $ncnt + 1;
                $param["goodsDetails"][$ncnt]["item"] = "NP Transfer Fees";
                $param["goodsDetails"][$ncnt]["itemCode"] = "NP TFR Fees";
                $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
                $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
                $param["goodsDetails"][$ncnt]["STOCKITEMUOM"] = $row["STOCKITEMUOM"] ?? 'PCS';
                $param["goodsDetails"][$ncnt]["unitPrice"] = number_format($NPTransferFee / $tcnt, 4, '.', '');
                $param["goodsDetails"][$ncnt]["total"] = number_format($NPTransferFee, 2, '.', '');
                $param["goodsDetails"][$ncnt]["taxRate"] = "0";
                $param["goodsDetails"][$ncnt]["tax"] = "0";
                $param["goodsDetails"][$ncnt]["discountTotal"] = "";
                $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0;
                $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
                $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
                $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
                $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
                $param["goodsDetails"][$ncnt]["categoryId"] = "";
                $param["goodsDetails"][$ncnt]["categoryName"] = "";
                $param["goodsDetails"][$ncnt]["vatApplicableFlag"] = "0";
                $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010102";
                $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
                $param["goodsDetails"][$ncnt]["exciseRate"] = "";
                $param["goodsDetails"][$ncnt]["exciseRule"] = "";
                $param["goodsDetails"][$ncnt]["exciseTax"] = "";
                $param["goodsDetails"][$ncnt]["pack"] = "";
                $param["goodsDetails"][$ncnt]["stick"] = "";
                $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
                $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
                $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
            }
            if ($RiderAdvTax > 0) {
                $ncnt2 = $ncnt2 + 1;
                $ncnt = $ncnt + 1;
                $param["goodsDetails"][$ncnt]["item"] = "Rider Advance Tax";
                $param["goodsDetails"][$ncnt]["itemCode"] = "RiderAdvTax";
                $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
                $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
                $param["goodsDetails"][$ncnt]["STOCKITEMUOM"] = $row["STOCKITEMUOM"] ?? 'PCS';
                $param["goodsDetails"][$ncnt]["unitPrice"] = number_format($RiderAdvTax / $tcnt, 4, '.', '');
                $param["goodsDetails"][$ncnt]["total"] = number_format($RiderAdvTax, 2, '.', '');
                $param["goodsDetails"][$ncnt]["taxRate"] = "0";
                $param["goodsDetails"][$ncnt]["tax"] = "0";
                $param["goodsDetails"][$ncnt]["discountTotal"] = "";
                $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0;
                $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
                $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
                $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
                $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
                $param["goodsDetails"][$ncnt]["categoryId"] = "";
                $param["goodsDetails"][$ncnt]["categoryName"] = "";
                $param["goodsDetails"][$ncnt]["vatApplicableFlag"] = "0";
                $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010102";
                $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
                $param["goodsDetails"][$ncnt]["exciseRate"] = "";
                $param["goodsDetails"][$ncnt]["exciseRule"] = "";
                $param["goodsDetails"][$ncnt]["exciseTax"] = "";
                $param["goodsDetails"][$ncnt]["pack"] = "";
                $param["goodsDetails"][$ncnt]["stick"] = "";
                $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
                $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
                $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
            }
            if ($StampDuty > 0) {
                $ncnt = $ncnt + 1;
                $param["goodsDetails"][$ncnt]["item"] = "Stamp Duty";
                $param["goodsDetails"][$ncnt]["itemCode"] = "StampDuty";
                $param["goodsDetails"][$ncnt]["qty"] = $tcnt;
                $param["goodsDetails"][$ncnt]["unitOfMeasure"] = "PP";
                $param["goodsDetails"][$ncnt]["STOCKITEMUOM"] = $row["STOCKITEMUOM"] ?? 'PCS';
                $param["goodsDetails"][$ncnt]["unitPrice"] = number_format($StampDuty / $tcnt, 4, '.', '');
                $param["goodsDetails"][$ncnt]["total"] = number_format($StampDuty, 2, '.', '');
                $param["goodsDetails"][$ncnt]["taxRate"] = "0";
                $param["goodsDetails"][$ncnt]["tax"] = "0";
                $param["goodsDetails"][$ncnt]["discountTotal"] = "";
                $param["goodsDetails"][$ncnt]["discountTaxRate"] = 0;
                $param["goodsDetails"][$ncnt]["orderNumber"] = $ncnt;
                $param["goodsDetails"][$ncnt]["discountFlag"] = "2";
                $param["goodsDetails"][$ncnt]["deemedFlag"] = "2";
                $param["goodsDetails"][$ncnt]["exciseFlag"] = "2";
                $param["goodsDetails"][$ncnt]["categoryId"] = "";
                $param["goodsDetails"][$ncnt]["categoryName"] = "";
                $param["goodsDetails"][$ncnt]["vatApplicableFlag"] = "0";
                $param["goodsDetails"][$ncnt]["goodsCategoryId"] = "96010102";
                $param["goodsDetails"][$ncnt]["goodsCategoryName"] = "";
                $param["goodsDetails"][$ncnt]["exciseRate"] = "";
                $param["goodsDetails"][$ncnt]["exciseRule"] = "";
                $param["goodsDetails"][$ncnt]["exciseTax"] = "";
                $param["goodsDetails"][$ncnt]["pack"] = "";
                $param["goodsDetails"][$ncnt]["stick"] = "";
                $param["goodsDetails"][$ncnt]["exciseUnit"] = "";
                $param["goodsDetails"][$ncnt]["exciseCurrency"] = "";
                $param["goodsDetails"][$ncnt]["exciseRateName"] = "";
            }
        }
        $ncnt2 = -1;
        if ($taxDetails_grossAmount_ex != 0) {
            $ncnt2 = $ncnt2 + 1;
            // $param["taxDetails"][$ncnt2]["taxCategory"] = "B: Exempted";
            $param["taxDetails"][$ncnt2]["taxCategory"] = "F: VAT OUT OF SCOPE";
            $param["taxDetails"][$ncnt2]["taxCategoryCode"] = "11";
            $param["taxDetails"][$ncnt2]["netAmount"] = $taxDetails_netAmount_ex;
            // $param["taxDetails"][$ncnt2]["taxRate"] = "-";
            $param["taxDetails"][$ncnt2]["taxRate"] = "0";
            $param["taxDetails"][$ncnt2]["taxAmount"] = "0";
            $param["taxDetails"][$ncnt2]["grossAmount"] = $taxDetails_grossAmount_ex;
            $param["taxDetails"][$ncnt2]["taxRateName"] = "0%";
            $param["taxDetails"][$ncnt2]["exciseUnit"] = "";
            $param["taxDetails"][$ncnt2]["exciseCurrency"] = "";
        }

        if ($taxDetails_grossAmount_std != 0) {
            $ncnt2 = $ncnt2 + 1;
            // $param["taxDetails"][$ncnt2]["taxCategory"] = "A: Standard";
            $param["taxDetails"][$ncnt2]["taxCategory"] = "01";
            $param["taxDetails"][$ncnt2]["taxCategoryCode"] = "01";
            $param["taxDetails"][$ncnt2]["netAmount"] = $taxDetails_netAmount_std;
            // $param["taxDetails"][$ncnt2]["taxRate"] = "0.18";
            $param["taxDetails"][$ncnt2]["taxRate"] = "18";
            $param["taxDetails"][$ncnt2]["taxAmount"] = $taxDetails_taxAmount_std;
            $param["taxDetails"][$ncnt2]["grossAmount"] = $taxDetails_grossAmount_std;
            $param["taxDetails"][$ncnt2]["taxRateName"] = "18%";
            $param["taxDetails"][$ncnt2]["exciseUnit"] = "";
            $param["taxDetails"][$ncnt2]["exciseCurrency"] = "";
        }

        if ($TnetAmount != 0) {
            $param["summary"]["netAmount"] = $TnetAmount - $TVat + $ExemptCharges;
            $param["summary"]["taxAmount"] = $TVat;
            $param["summary"]["grossAmount"] = $TnetAmount + $ExemptCharges;
            $param["summary"]["itemCount"] = $ncnt + 1;
            $param["summary"]["modeCode"] = "0";
            $param["summary"]["remarks"] = "";
            $param["summary"]["qrCode"] = "";
            $param["payWay"]["paymentMode"] = "101";
            $param["payWay"]["paymentAmount"] = $TnetAmount + $ExemptCharges;
            $param["payWay"]["orderNumber"] = "a";
            $param["extend"]["reason"] = "";
            $param["extend"]["reasonCode"] = "";
        }

        $param['extra'] = $extra_data;
        $param['extra']["discount"] = $TOTAL_DISCOUNT;

        return $param;
    }

    private function getBranchID($docty, $type = "")
    {
        $branch = MasterBranch::query()->where('inv_prefix', $docty)->first();

        if (!isset($branch)) {
            return '';
        }

        if ($type == 'Code') {
            return $branch->branch_code;
        } else {
            return $branch->branch_id;
        }
    }

    private function getInvoiceName($referenceNo)
    {
        $prefix = explode('/', $referenceNo);
        $prefix = $prefix[0];

        if ($prefix == 'SINV') {
            return 'templates.pdf.sinv';
        } elseif ($prefix == 'CSIB') {
            return 'templates.pdf.csib';
        } elseif ($prefix == 'SIS') {
            return 'templates.pdf.sis';
        } elseif ($prefix == 'CS') {
            return 'templates.pdf.cs';
        } elseif ($prefix == 'JBI') {
            return 'templates.pdf.jbi';
        }
    }

    private function setStockAdjustment($invoice)
    {
        try {
            if (env('STOCK_ADJUST') == null || env('STOCK_ADJUST') == 0) {
                return true;
            }

            $goodsDetails = $invoice['goodsDetails'];

            $goodsStockIn = [
                'operationType' => "101",
                'stockInType' => "103",
                'branchId' => $invoice['sellerDetails']['branchId']
            ];

            $goodsStockInItem = [];

            foreach ($goodsDetails as $goodsDetail) {
                if (isset($goodsDetail['itemCode']) && !empty($goodsDetail['itemCode'])) {
                    if (MasterItem::query()
                        ->where('ura_goodsCode', $goodsDetail['itemCode'])
                        ->where('status', '1')
                        ->exists()) {
                        $goodsStockInItem[] = [
                            'goodsCode' => $goodsDetail['itemCode'],
                            'quantity' => $goodsDetail['qty'],
                            'unitPrice' => $goodsDetail['unitPrice']
                        ];
                    }
                }
            }

            if (count($goodsStockInItem) > 0) {
                $this->efrisCall([
                    'goodsStockIn' => $goodsStockIn,
                    'goodsStockInItem' => $goodsStockInItem
                ], 'T131');
            }

            return true;
        } catch (Throwable $th) {
            Log::error($th);
            dump($th->getMessage());

            return false;
        }
    }
}
