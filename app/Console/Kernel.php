<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('import:party --force')->everyMinute()
            ->name('import_party')->withoutOverlapping()
            ->environments(['production']);
        $schedule->command('import:stock')->everyMinute()
            ->name('import_stock')->withoutOverlapping()
            ->environments(['production']);
        $schedule->command('import:stock-upload')->everyMinute()
            ->name('import_stock_upload')->withoutOverlapping()
            ->environments(['production']);
        $schedule->call('App\Http\Controllers\InvoiceController@upload')->everyMinute()
            ->name('invoice_upload')->withoutOverlapping()
            ->environments(['production']);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
