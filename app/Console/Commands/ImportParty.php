<?php

namespace App\Console\Commands;

use App\Models\MasterParty;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportParty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:party {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Party from Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Party Import Started');

        $sql = "SELECT [iMasterId],[sName],[sCode],[iAccountTypeName],[iParentId]
                    ,[iAccountTypeId],[AccountCategoryName],[TownName],[RegionName],[SalesManName]
                    ,[sAddress],[sPin],[iCityName],[DistrictName]
                    ,[sTelNo],[sFaxNo],[ContactPerson],[MobileNo],[sEMail],[sFinanceEmail],[Website]
                    ,[fCreditLimit],[TINNO],[iCreditDays],[iPaymentTermsName],[iDisplayCreditTotal],[AccountCategory]
                FROM [Focus8040].[dbo].[vcCore_Account]
                WHERE [iAccountTypeName] NOT IN ('Liabilities','Bank', 'Income', 'Expenses','Assets', 'Vendor','Purchases','Employee','Other expenses', 'Inventory','Fixed Assets','Control','Assets/Liabilities','Cash')
                    AND [iAccountTypeName] IN ('Sales','Customer/Vendor','Customer')";
        if (!$this->option('force')) {
            $sql .= " AND [iModifiedDate] > '" . Carbon::now()->subMinutes(2)->format('Y-m-d H:i:s') . "'";
        }

        $result = DB::select($sql);

        $this->info(count($result) . ' Party Fetched from Database');

        if ($this->option('force')) {
            MasterParty::truncate();
            $this->info('Master Party Truncated');
        }

        $insertArray = [];
        for ($i = 0; $i < count($result); $i++) {
            $data = array_values((array)$result[$i]);
            $partyArray = [
                'ID' => $data[0],
                'VATTINNUMBER' => $data[22],
                'Address1' => $data[10],
                'Address2' => $data[11],
                'Address3' => $data[12],
                'Address4' => $data[13],
                'LEDGERPHONE' => $data[14],
                'LEDGERFAX' => $data[15],
                'LEDGERCONTACT' => $data[16],
                'LEDGERMOBILE' => $data[17],
                'LEDGEREMAIL' => $data[18],
                'Trans' => $data[26]
            ];
            if ($this->option('force')) {
                $insertArray[] = $partyArray;
            } else {
                $party = MasterParty::where('ID', $partyArray['ID'])->first();
                if ($party) {
                    $party->update($partyArray);
                } else {
                    $insertArray[] = $partyArray;
                }
            }
        }

        foreach (array_chunk($insertArray, ceil(999 / 13)) as $chunk) {
            MasterParty::insert($chunk);
        }

        $this->info('Party Import Completed');

        return 0;
    }
}
