<?php

namespace App\Console\Commands;

use App\Models\MasterItem;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:stock {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Stock from Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Stock Import Started');

        $sql = "SELECT p.[iMasterId],p.[sName],p.[sCode],p.[iProductType],
				u.[iDefaultBaseUnit],u1.[sName] as sUomName,u1.[iNoOfDecimals],
				(SELECT top 1 iif(sp.[iProductId]!=20,(sp.[fVal0]+(sp.[fVal0]*.18)),sp.[fVal0])
					FROM [Focus8040].[dbo].[mCore_SellingPriceBookDetails] as sp
					where sp.[iProductId]=p.[iMasterId] and sp.[bMarkDeleted]=0
					order by sp.[iProductId], sp.[iSequenceNumber] desc ) as UnitPrice,
					ISNULL((SELECT sum(fQuantity)
				  FROM	[Focus8040].[dbo].[tCore_Indta_0] as ib2
						left join [Focus8040].[dbo].[tCore_Data_0] as db2 on ib2.iBodyId=db2.iBodyId
						left join [Focus8040].[dbo].[tCore_Header_0] as hb2 on db2.iHeaderId=hb2.iHeaderId
				  where hb2.[iDate] =132385823 and hb2.iVoucherType=512 and
				   ib2.iProduct=p.[iMasterId]),0) as StkBal,
				mup.[Commoditycode]
			FROM [Focus8040].[dbo].[mCore_Product] as p
				left join [Focus8040].[dbo].[muCore_Product_Units] as u
					on u.[iMasterId] = p.[iMasterId]
				left join [Focus8040].[dbo].[mCore_Units] as u1
					on u.[iDefaultBaseUnit] = u1.[iMasterId]
				left join [Focus8040].[dbo].[muCore_Product] as mup
					on mup.[iMasterId] = p.[iMasterId]";
        if (!$this->option('force')) {
            $sql .= " WHERE datediff(second, '1970-01-01', [Focus8040].[dbo].[fCore_IntToDateTime](p.[iModifiedDate])) > '" . Carbon::now()->subMinutes(2)->getTimestamp() . "'";
        }

        $result = DB::select($sql);

        $this->info(count($result) . ' Stock Fetched from Database');

        if ($this->option('force')) {
            MasterItem::truncate();
            $this->info('Master Stock Truncated');
        }

        $insertArray = [];
        $uomArray = [
            'PCS' => 'PP',
            'LTR' => '102',
        ];
        for ($i = 0; $i < count($result); $i++) {
            $data = array_values((array)$result[$i]);
            $stockArray = [
                'id' => $data[0],
                'STOCKITEMNAME' => $data[1],
                'ura_goodsCode' => $data[2],
                'STOCKITEMUOM' => $data[5],
                'ura_commodityCategoryId' => $data[9],
                'ura_measureUnit' => $uomArray[$data[5]] ?? null,
            ];

//            Only for Test Environment
//            if ($stockArray['ura_commodityCategoryId'] == '46181705') {
//                $stockArray['ura_commodityCategoryId'] = '60101001';
//            }

            if (in_array($stockArray['ura_goodsCode'], ["00VCL139", "00VCL129", "01170110", "01170111", "01100610"])) {
                if (in_array($stockArray['ura_commodityCategoryId'], ["46181706", "25172502", "31163207", "31171504", "01100610"])) {
                    $stockArray['ura_commodityCategoryId'] = "25175102";
                }
            }

            if ($this->option('force')) {
                $insertArray[] = $stockArray;
            } else {
                $stock = MasterItem::where('id', $stockArray['id'])->first();
                if ($stock) {
                    $stockArray['status'] = 0;
                    $stock->update($stockArray);
                } else {
                    $insertArray[] = $stockArray;
                }
            }
        }

        foreach (array_chunk($insertArray, ceil(999 / 7)) as $chunk) {
            MasterItem::insert($chunk);
        }

        /*
        SELECT mi.ura_goodsCode, mp.goods_code, mi.ura_commodityCategoryId, mp.commodityCategoryId, mi.ura_measureUnit, mp.measureUnit
        FROM db_bajaj.master_items mi, db_ura.mst_product mp
        WHERE mp.goods_code = mi.ura_goodsCode;

        UPDATE db_bajaj.master_items mi
        SET mi.ura_commodityCategoryId = (SELECT mp.commodityCategoryId FROM db_ura.mst_product mp WHERE mp.goods_code = mi.ura_goodsCode),
        mi.ura_measureUnit = (SELECT mp.measureUnit FROM db_ura.mst_product mp WHERE mp.goods_code = mi.ura_goodsCode);

        UPDATE master_items
        SET `status` = 1
        WHERE ura_commodityCategoryId is not null OR ura_commodityCategoryId != '';
        */

        $this->info('Stock Import Completed');

        return 0;
    }
}
