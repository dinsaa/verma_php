<?php

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use App\Models\MasterItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ImportStockUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:stock-upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload Stock from Database to EFRIS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Stock Upload Started');

        $stocks = MasterItem::query()
            ->where('status', '0') /* Pending */
            ->where(function ($query) {
                $query->where('ura_commodityCategoryId', '!=', '');
                $query->whereNotNull('ura_commodityCategoryId');
            })
            ->where(function ($query) {
                $query->where('ura_goodsCode', '!=', '');
                $query->whereNotNull('ura_goodsCode');
            })
            ->get();

        $this->info($stocks->count() . ' Stock Fetched from Database');

        $controller = new Controller();

        foreach ($stocks as $stock) {
            $response = $controller->efrisCall([
                "goodsCode" => $stock->ura_goodsCode
            ], 'T127');

            $responseContent = $controller->efrisDecode($response);

            if ($responseContent['status']) {
                if (isset($responseContent['data']['records'])
                    && count($responseContent['data']['records']) == 1) {
                    $stock->update(['status' => 1]);
                } else {
                    $stockArray = [
                        'goodsName' => $stock->STOCKITEMNAME,
                        'goodsCode' => $stock->ura_goodsCode,
                        'measureUnit' => $stock->ura_measureUnit,
                        'unitPrice' => "1",
                        'currency' => "101",
                        'commodityCategoryId' => $stock->ura_commodityCategoryId,
                        'description' => "",
                        'stockPrewarning' => "0",
                        'haveExciseTax' => "102",
                        'havePieceUnit' => "102",
                        'pieceMeasureUnit' => "",
                        'pieceUnitPrice' => "",
                        'packageScaledValue' => "",
                        'pieceScaledValue' => "",
                        'exciseDutyCode' => "",
                        'isExempt' => "102",
                        'isZeroRate' => "102",
                        'taxRate' => "18",
                        'commodityType' => "VAT",
                        'taxType' => "STANDARD"
                    ];

                    $res = Http::withHeaders([
                        'X-Request-For' => 'Product'
                    ])->post(env('API_URL') . '/verma', [
                        'json_data' => [$stockArray]
                    ]);

                    if ($res->json('data.0.error_code') === "") {
                        $stock->update(['status' => 1]); /* Success */
                    } else {
                        $stock->update(['status' => 2]); /* Failed */
                    }
                }
            }
        }

        $this->info('Stock Upload Completed');

        return 0;
    }
}
