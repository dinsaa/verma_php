<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'designation',
        'voucher_series',
        'status'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'voucher_series' => 'array',
    ];

    protected $connection = "mysql";

    public function scopeFilter($query, $data)
    {
        if (isset($data['username']) && !empty(['username'])) {
            $query->where('name', 'like', "%{$data['username']}%");
            $query->orWhere('designation', 'like', "%{$data['username']}%");
        }

        if (isset($data['status']) && !empty(['status'])) {
            if ($data['status'] == 'active') $query->where('status', '1');
            if ($data['status'] == 'inactive') $query->where('status', '0');
        }
    }
}
