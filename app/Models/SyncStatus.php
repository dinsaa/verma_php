<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SyncStatus extends Model
{
    use HasFactory;

    protected $connection = "mysql";

    protected $guarded = [];

    public function scopeFilter($query, $data)
    {
        if (isset($data['customer_name']) && !empty(['customer_name']))
            $query->where('req_body->buyerDetails->buyerLegalName', 'like', "%{$data['customer_name']}%");

        if (auth()->user()->role_id != 1 && isset(auth()->user()->voucher_series)) {
            $query->where(function ($q1) use ($data) {
                foreach (auth()->user()->voucher_series as $key => $series) {
                    if ($key === array_key_first(auth()->user()->voucher_series)) {
                        $q1->where('req_body->sellerDetails->referenceNo', 'like', "%{$series}%");
                    } else {
                        $q1->orWhere('req_body->sellerDetails->referenceNo', 'like', "%{$series}%");
                    }
                }
            });
        }

        if (isset($data['unique_no']) && !empty(['unique_no'])) {
            $query->where(function ($q1) use ($data) {
                $q1->where('req_body->sellerDetails->referenceNo', 'like', "%{$data['unique_no']}%");
                $q1->orWhere('res_body->invoiceNo', 'like', "%{$data['unique_no']}%");
            });
        }

        if (isset($data['from_date']) && !empty(['from_date']))
            $query->whereDate('req_body->basicInformation->issuedDate', '>=', $data['from_date']);
        if (isset($data['to_date']) && !empty(['to_date']))
            $query->whereDate('req_body->basicInformation->issuedDate', '<=', $data['to_date']);

        if (isset($data['status']) && !empty(['status'])) {
            if ($data['status'] == 'success') $query->where('status', '1');
            if ($data['status'] == 'failed') $query->where('status', '2');
        } else {
            $query->where('status', '!=', '3');
        }
    }
}
