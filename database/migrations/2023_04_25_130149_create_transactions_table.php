<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->string('ID')->nullable();
            $table->string('DATE')->nullable();
            $table->string('VOUCHERNUMBER')->nullable();
            $table->string('VOUCHERTYPENAME')->nullable();
            $table->string('REFERENCE')->nullable();
            $table->string('ENTEREDBY')->nullable();
            $table->string('PARTYNAME')->nullable();
            $table->string('PARTYLEDGERNAME')->nullable();
            $table->string('STOCKITEMNAME')->nullable();
            $table->string('RATEVALUE')->nullable();
            $table->string('QTY')->nullable();
            $table->string('UNIT')->nullable();
            $table->string('VAT')->nullable();
            $table->string('AMOUNT')->nullable();
            $table->string('Sales')->nullable();
            $table->string('SalesTyres')->nullable();
            $table->string('SalesExempted')->nullable();
            $table->string('SalesZeroRate')->nullable();
            $table->string('SalesExport')->nullable();
            $table->string('DISCOUNT')->nullable();
            $table->string('ADDLCOSTPERC')->nullable();
            $table->string('GUID')->nullable();
            $table->string('PRICELEVEL')->nullable();
            $table->string('NARRATION')->nullable();
            $table->string('BASICORDERREF')->nullable();
            $table->string('MFDON')->nullable();
            $table->string('BASICORDERTERMS')->nullable();
            $table->string('BASICPURCHASEORDERNO')->nullable();
            $table->string('BASICORDERDATE')->nullable();
            $table->string('EXPIRYPERIOD')->nullable();
            $table->string('GODOWNNAME')->nullable();
            $table->string('BATCHNAME')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
