<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyncStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('req_type')->nullable();
            $table->string('req_key')->nullable();
            $table->text('req_body')->nullable();
            $table->text('res_body')->nullable();
            $table->integer('status')->nullable(); // 1 = Success, 2 = Failed, 3 = Reupload
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_statuses');
    }
}
