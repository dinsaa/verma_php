<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->string('ID')->nullable();
            $table->string('CostCenter')->nullable();
            $table->string('SalesOrderNo')->nullable();
            $table->string('VermaMember')->nullable();
            $table->string('SalesMan')->nullable();
            $table->string('SalesRefernce')->nullable();
            $table->string('MechanicName')->nullable();
            $table->string('ServiceType')->nullable();
            $table->string('TimeIn')->nullable();
            $table->string('EstDeliveryDate')->nullable();
            $table->string('EstDeliveryTime')->nullable();
            $table->string('ChassisNo')->nullable();
            $table->string('RegistrationNo')->nullable();
            $table->string('Soldto')->nullable();
            $table->string('CurrentKm')->nullable();
            $table->string('JobCardNo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
