<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterPartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_parties', function (Blueprint $table) {
            $table->string('ID')->nullable();
            $table->string('VATTINNUMBER')->nullable();
            $table->string('Address1')->nullable();
            $table->string('Address2')->nullable();
            $table->string('Address3')->nullable();
            $table->string('Address4')->nullable();
            $table->string('LEDGERCONTACT')->nullable();
            $table->string('LEDGERPHONE')->nullable();
            $table->string('LEDGERFAX')->nullable();
            $table->string('LEDGERMOBILE')->nullable();
            $table->string('LEDGEREMAIL')->nullable();
            $table->string('Trans')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_parties');
    }
}
