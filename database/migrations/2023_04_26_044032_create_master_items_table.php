<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_items', function (Blueprint $table) {
            $table->string('id')->nullable();
            $table->string('STOCKITEMNAME')->nullable();
            $table->string('STOCKITEMUOM')->nullable();
            $table->string('ura_commodityCategoryId')->nullable();
            $table->string('ura_goodsCode')->nullable();
            $table->string('ura_measureUnit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_items');
    }
}
