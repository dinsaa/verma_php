<?php

namespace Database\Seeders;

use App\Models\MasterBranch;
use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branchList = [
//            ['inv_prefix' => '', 'branch_code' => '00', 'branch_name' => 'VERMA CO. LIMITED', 'branch_id' => 'E002345'],
            ['inv_prefix' => 'CS/BMB', 'branch_code' => '01', 'branch_name' => 'Bombo Road', 'branch_id' => '404724736259113052'],
            ['inv_prefix' => 'SINV/BMB', 'branch_code' => '01', 'branch_name' => 'Bombo Road', 'branch_id' => '404724736259113052'],
            ['inv_prefix' => 'CSIB/BMB', 'branch_code' => '01', 'branch_name' => 'Bombo Road', 'branch_id' => '404724736259113052'],
            ['inv_prefix' => 'CS/LUG', 'branch_code' => '02', 'branch_name' => 'Lugogo-SHW', 'branch_id' => '404724737024336704'],
            ['inv_prefix' => 'CS/NDB', 'branch_code' => '03', 'branch_name' => 'Ndeeba STR-2', 'branch_id' => '404724737796228606'],
            ['inv_prefix' => 'SINV/GUL', 'branch_code' => '04', 'branch_name' => 'Gulu', 'branch_id' => '404724738683503208'],
            ['inv_prefix' => 'CSIB/GUL', 'branch_code' => '04', 'branch_name' => 'Gulu', 'branch_id' => '404724738683503208'],
            ['inv_prefix' => 'SCI/GUL', 'branch_code' => '04', 'branch_name' => 'Gulu', 'branch_id' => '404724738683503208'],
            ['inv_prefix' => 'SIS/GUL', 'branch_code' => '04', 'branch_name' => 'Gulu', 'branch_id' => '404724738683503208'],
            ['inv_prefix' => 'SINV/MSK', 'branch_code' => '05', 'branch_name' => 'Masaka', 'branch_id' => '404724739880075701'],
            ['inv_prefix' => 'CSIB/MSK', 'branch_code' => '05', 'branch_name' => 'Masaka', 'branch_id' => '404724739880075701'],
            ['inv_prefix' => 'SINV/NDB', 'branch_code' => '06', 'branch_name' => 'Ndeeba Branch', 'branch_id' => '404724740493471603'],
            ['inv_prefix' => 'CSIB/NDB', 'branch_code' => '06', 'branch_name' => 'Ndeeba Branch', 'branch_id' => '404724740493471603'],
            ['inv_prefix' => 'SINV/MBR', 'branch_code' => '07', 'branch_name' => 'Mbarara', 'branch_id' => '404724741329492105'],
            ['inv_prefix' => 'CSIB/MBR', 'branch_code' => '07', 'branch_name' => 'Mbarara', 'branch_id' => '404724741329492105'],
            ['inv_prefix' => 'SCI/MBR', 'branch_code' => '07', 'branch_name' => 'Mbarara', 'branch_id' => '404724741329492105'],
            ['inv_prefix' => 'SIS/MBR', 'branch_code' => '07', 'branch_name' => 'Mbarara', 'branch_id' => '404724741329492105'],
            ['inv_prefix' => 'SINV/MBL', 'branch_code' => '08', 'branch_name' => 'Mbale', 'branch_id' => '404724742247143207'],
            ['inv_prefix' => 'CSIB/MBL', 'branch_code' => '08', 'branch_name' => 'Mbale', 'branch_id' => '404724742247143207'],
            ['inv_prefix' => 'SINV/JNJ', 'branch_code' => '09', 'branch_name' => 'Jinja', 'branch_id' => '404724743318115459'],
            ['inv_prefix' => 'CSIB/JNJ', 'branch_code' => '09', 'branch_name' => 'Jinja', 'branch_id' => '404724743318115459'],
            ['inv_prefix' => 'JBI/WSP', 'branch_code' => '10', 'branch_name' => 'Lugogo-WSP', 'branch_id' => '404724744327400952'],
            ['inv_prefix' => 'SINV/LUG', 'branch_code' => '11', 'branch_name' => 'Lugogo', 'branch_id' => '404724745083932254'],
            ['inv_prefix' => 'CSIB/LUG', 'branch_code' => '11', 'branch_name' => 'Lugogo', 'branch_id' => '404724745083932254'],
            ['inv_prefix' => 'SCI/LUG', 'branch_code' => '11', 'branch_name' => 'Lugogo', 'branch_id' => '404724745083932254'],
            ['inv_prefix' => 'SIS/LUG', 'branch_code' => '11', 'branch_name' => 'Lugogo', 'branch_id' => '404724745083932254'],
            ['inv_prefix' => 'SINV/NDW', 'branch_code' => '12', 'branch_name' => 'Ndeeba WH', 'branch_id' => '404724745833492756'],
            ['inv_prefix' => 'CSIB/NDW', 'branch_code' => '12', 'branch_name' => 'Ndeeba WH', 'branch_id' => '404724745833492756'],
            ['inv_prefix' => 'SINV/FPL', 'branch_code' => '13', 'branch_name' => 'Fort Portal', 'branch_id' => '404724746840610458'],
            ['inv_prefix' => 'CSIB/FPL', 'branch_code' => '13', 'branch_name' => 'Fort Portal', 'branch_id' => '404724746840610458'],
            ['inv_prefix' => 'SCI/FPL', 'branch_code' => '13', 'branch_name' => 'Fort Portal', 'branch_id' => '404724746840610458'],
            ['inv_prefix' => 'SIS/FPL', 'branch_code' => '13', 'branch_name' => 'Fort Portal', 'branch_id' => '404724746840610458'],
            ['inv_prefix' => 'SINV/LRA', 'branch_code' => '14', 'branch_name' => 'Lira', 'branch_id' => '404724748112561851'],
            ['inv_prefix' => 'CSIB/LRA', 'branch_code' => '14', 'branch_name' => 'Lira', 'branch_id' => '404724748112561851'],
            ['inv_prefix' => 'SINV/HMA', 'branch_code' => '15', 'branch_name' => 'Hoima', 'branch_id' => '404724748593400053'],
            ['inv_prefix' => 'CSIB/HMA', 'branch_code' => '15', 'branch_name' => 'Hoima', 'branch_id' => '404724748593400053'],
            ['inv_prefix' => 'SINV/IGN', 'branch_code' => '16', 'branch_name' => 'Iganga', 'branch_id' => '404724749374648055'],
            ['inv_prefix' => 'CSIB/IGN', 'branch_code' => '16', 'branch_name' => 'Iganga', 'branch_id' => '404724749374648055'],
            ['inv_prefix' => 'RS/RTS', 'branch_code' => '17', 'branch_name' => 'Route Sales Van', 'branch_id' => '414391414125100586'],
            ['inv_prefix' => 'RS/RSP', 'branch_code' => '18', 'branch_name' => 'Route Sales Pickup', 'branch_id' => '472661049463530024'],
//            ['inv_prefix' => '', 'branch_code' => '19', 'branch_name' => 'Route Sales - Sowedi Mudehere', 'branch_id' => '100917809431150352'],
            ['inv_prefix' => 'JBI/MSK', 'branch_code' => '20', 'branch_name' => 'Masaka-WSP', 'branch_id' => '325227000921036177'],
        ];

        foreach ($branchList as $branch) {
            MasterBranch::create([
                'inv_prefix' => $branch['inv_prefix'],
                'branch_code' => $branch['branch_code'],
                'branch_name' => $branch['branch_name'],
                'branch_id' => $branch['branch_id'],
            ]);
        }
    }
}
